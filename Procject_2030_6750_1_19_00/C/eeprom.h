/*========================================================
*@File name:eeprom.h
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#ifndef	_EEPROM_H__
#define _EEPROM_H__

#include "BA45F6750.h"
#include "common.h"


#define	Cal_State_A_B			0x00	//0 calibration state address backup
										//1
#define	ZERO_PPM_OFFSET			0x02	//2
										//3
#define Cal_Scale_LSB			0x04	//4 scale
#define Cal_Scale_MSB			0x05	//5
#define OPA_OFFSET				0x06	//6
#define PCB_OFFSET_Add			0x07	//7
#define Life_Day_LSB			0x08	//8
#define Life_Day_MSB			0x09	//9
										//10 0x0A
#define CLEAR_AIR_MAX_Add		0x0B	//11 0x0B	
										//12 0x0C
										//13 0x0D
										//14 0x0E
										//15 0x0F	
#define EEP_Checksum_A_H		0x10	//16 0x10	check sum address
#define EEP_Checksum_A_L		0x11	//17 0x11
										//18 0x12
										//19 0x13
										//20 0x14		
#define	PTT_FLAG_Address		0x15	//21 0x15		PTT	happend 1, no , 0
#define	D_wPastSum_L_Address	0x16	//22 0x16		w1AlarmPastSum_LSB
#define	D_wPastSum_H_Address	0x17	//23 0x17		w1AlarmPastSum_HSB
#define	D_InterimSum_L_Address  0x18	//24 0x18		w1AlarmInterimSum_LSB
#define	D_InterimSum_H_Address  0x19	//25 0x19		w1AlarmInterimSum_HSB
#define	D_AlarmSum_L_Address	0x1A	//26 0x1A		wAlarmSum_LSB
#define	D_AlarmSum_H_Address	0x1B	//27 0x1B		wAlarmSum_HSB
										//28 0x1C
										//29 0x1D										
#define Low_BAT_Threshold		0x1E	//30 0x1E
#define PTT_SHORT_ADD			0x1F	//31 0x1F
										//32 0x20
										//33 0x21
										//34 0x22
										//35 0x23
										//36 0x24
										//37 0x25
										//38 0x26
										//39 0x27
#define	C_EEP_ROMChecksum_L 	0x28	//40 0x28   ROM checksum L
#define	C_EEP_ROMChecksum_H		0x29	//41 0x29	ROM checksum H
										//42 0x2A			
										//43 0x2B				
										//44 0x2C
#define PTT_Day_L_Address		0x2D	//45 0x2D		
#define PTT_Day_H_Address		0x2E	//46 0x2E		
#define PTT_Count_Address		0x2F	//47 0x2F       PTT count save
#define RESET_POWER_EVENT		0x30	//48 0x30       power event day LSB
										//49 0x31       power event day HSB
										//50 0x32		power event count
#define LOW_BAT_EVENT			0x33	//51 0x33		low battery envent day LSB		
										//52 0x34		low battery envent day HSB
										//53 0x35		low battery envent count
#define CO_FAULT_EVENT			0x36	//54 0x36		co fault event day LSB
										//55 0x37		co fault event day HSB
										//56 0x38		co fault event count
										//57 0x39		co alarm event day LSB
#define CO_ALARM_EVENT			0x3A	//58 0x3A		co alarm event day LSB 
										//59 0x3B		co alarm event day HSB 
										//60 0x3C		co alarm event count
#define MEMORY_FAULT_EVENT		0x3D	//61 0x3D		memory fault event day LSB
										//62 0x3E		memory fault event day HSB
										//63 0x3F		memory fault event count
										
#define EEP_Checksum_Address	0x7F	//127 0x7F	eeprom checksum
#define	BACK_EEP_Csum_Address   0xFF	//256		backup checksum
		
	
#define HISTORY_EVENT			1

unsigned char S_RD_EE(unsigned char RD_EE_addr);
void S_WR_EE(unsigned char WR_EE_addr,unsigned char WR_EE_data);
void Save_CheckSum(u16 sum);


#endif
