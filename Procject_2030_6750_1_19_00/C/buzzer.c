/*========================================================
*@File name:buzzer.c
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#include "buzzer.h"
#include "sys_init.h"
#include "co.h"
#include "uart.h"

/**************************************
*@neam:horn init
*@brief:horn init
*@param[in]:
*@retval: 
*@notes:
**************************************/
#if F_PWM
void S_BUZZER_INIT(void)
{
	
	//8M/16=500KHZ	
	_ptmc0	=0b00100000;	
	_ptmc1	=0b10101000;
	
	//frequency and duty
	_ptmrpl = (C_BUZZ_FREQUENCY&0xFF);
	_ptmrph = ((C_BUZZ_FREQUENCY>>8)&0xFF);
	_ptmal  = (C_BUZZ_DUTY&0xFF);
	_ptmah  = ((C_BUZZ_DUTY>>8)&0xFF);
	
	
//	config gpio PA4 	
//	_pas11	= 1;					
//	_pas10	= 1;
//	config gpio PA3
	_pas07  = 0;
	_pas06  = 1;

	_pton = 0;
}
#endif

#if 0
void S_BUZZER_INIT(void)
{
	static u8 duty = 0;
	
	//8M/16=500KHZ	
	//_ptmc0	=0b00000000;
	_ptmc0	=0b00100000;	
	_ptmc1	=0b10101000;
	
	//频率占空比赋值
	_ptmrpl = (100&0xFF);
	_ptmrph = ((100>>8)&0xFF);
	_ptmal  = (duty&0xFF);
	_ptmah  = ((duty>>8)&0xFF);
	

	duty ++;
	if(duty >= 100) duty = 0;
	
	S_UART_SEND_Str("duty:");
	S_UART_SEND_ASCII_Dec((duty&0xff00)>>8,duty&0x00ff);
	S_UART_SEND_BYTE('\r');
	S_UART_SEND_BYTE('\n');
		
//	配置复用引脚PA4 	
//	_pas11	= 1;					
//	_pas10	= 1;
//	配置复用引脚PA3
//	_pas07  = 0;
//	_pas06  = 1;

//	配置复用引脚PA5
	_pas13  = 0;
	_pas12  = 1;

	_pton = 0;
}
#endif

/**************************************
*@neam:S_BUZZER_CTRL
*@brief:蜂鸣器驱动 
*@param[in]:
*@retval: 
*@notes:
**************************************/
#if 1
void S_BUZZER_CTRL(void)
{
	#if 1
//	static	u16	R_BUZZER_CNT;
//	static	u16	R_BUZZER_SERIES;
	static  u8 FLAG_Delay = 0;
	
	//enable horn
	if(F_BUZZER_EN == 1)
	{
//		if((F_TEST == 1) && (F_CO_ALARM == 1))
//		{
//			_papu3 = 0;
//		}
//		else
//		{
//			_papu3 = 1;	
//		}
		u8 R_BUZZ_MODE_FG = 0;
		if(R_BUZZER_STATE == 0x01)	R_BUZZ_MODE_FG = 3;
		if(R_BUZZER_STATE == 0x02)	R_BUZZ_MODE_FG = 2;
		if(R_BUZZER_STATE == 0x04)	R_BUZZ_MODE_FG = 1;
		
		switch(R_BUZZ_MODE_FG)
		{
			//open horn
			case 3:			
					if(!F_TEST)
					{
						P_BUZZ_EN = 0;
					}
					
					if((F_TEST == 0) && (F_CO_ALARM == 0) && (F_BUZZER_ONEPERIOD == 1))//||(F_BAT_ISLOW == 1)
					{
						FLAG_Delay++;
						if(FLAG_Delay>=8)
						{
							_pa3 = 1;	
							R_BUZZER_CNT++;
							if(R_BUZZER_CNT >= R_BUZZON_CNTSET)	
							{
								F_BUZZ_SHIFT=1;
								R_BUZZER_CNT = 0;
								FLAG_Delay = 0;
							}
						}
					}
					else
					{
						_pa3 = 1;	
						R_BUZZER_CNT++;
						if(R_BUZZER_CNT >= R_BUZZON_CNTSET)	
						{
							F_BUZZ_SHIFT=1;
							R_BUZZER_CNT = 0;
						}	
					}
		
			break;
			//===================================================
			//horn close
			case 2:
				#if F_PWM
					_pton=0;
				#else
					_pa3 = 0;
				#endif			
				R_BUZZER_CNT++;				
				if(R_BUZZER_CNT >= R_BUZZOFF_CNTSET)	
				{	
					F_BUZZ_SHIFT=1;
					R_BUZZER_CNT = 0;
				}
					
			break;
			//===================================================
			//cycle process
			case 1:
				//once process
//				F_BUZZER_RESET = 1;
//				R_BUZZER_SERIES++;
				if(F_BUZZER_ONCE)						
				{
					F_BUZZER_RESET = 1;
					R_BUZZER_SERIES++;
					if(R_BUZZER_SERIES >= R_BUZZ_SERIESSET)	
						{
							F_BUZZER_RESET=0;
							F_BUZZER_STOP=1;
						}
				}
				//cycle process
				else									
				{
					F_BUZZER_RESET = 1;
					R_BUZZER_SERIES++;
					if(R_BUZZER_SERIES>=R_BUZZ_SERIESSET)	
						{
							F_BUZZER_RESET = 0;
							R_BUZZER_SERIES = 0;
							F_BUZZER_ONEPERIOD = 1;		//one cycle done
							F_BUZZ_SHIFT = 1;
							#if F_PWM
							
							#else
								//if((!F_TEST) || (!F_CO_ALARM))P_BUZZ_EN = 1;
								if((!F_TEST) && (!F_CO_ALARM))P_BUZZ_EN = 1;
								/*_pton = 0;*/
							#endif
						}				
				}			
			break;
			//===================================================
			//wait process
			case 0:
				#if F_PWM
					//_pton=0;
				#else
					_pa3 = 0;
				#endif	
				F_BUZZER_RESET=0;
				R_BUZZER_CNT++;
				if(R_BUZZER_CNT>=R_BUZZWAIT_CNTSET)		F_BUZZER_RESET=1;
				
			break;
			//===================================================
			//
			default:
				//_pton = 0;
				//_pa3 = 0;
			break;
			
		}
//===================================================		
		//reset horn process		
		if(F_BUZZER_RESET)							
		{
			F_BUZZER_RESET = 0;
			R_BUZZER_CNT = 0;
			R_BUZZER_STATE = 0;
			R_BUZZER_STATE = 0x01;
		}
		
		//states process
		if(F_BUZZ_SHIFT)							
		{
			F_BUZZ_SHIFT = 0;
			R_BUZZER_CNT = 0;
			R_BUZZER_STATE <<= 1;	
		}
		//horn stop process
		if(F_BUZZER_STOP)							
		{
			F_BUZZER_STOP = 0;
			#if F_PWM
				_pton=0;
			#else
				_pa3 = 0;
			#endif	
			F_BUZZER_EN = 0;
			R_BUZZER_STATE = 0;
			R_BUZZER_CNT = 0;
		}
	}
	else
	{
		R_BUZZER_CNT = 0;
		R_BUZZER_SERIES = 0;
	}
	#endif
}
#endif


/**************************************
*@neam:S_BUZZER_ON
*@brief:控制蜂鸣器按指定参数响 
*@param[in]:V1，ON时长
	      	V2，OFF时长
			V3，一个周响的次数
			V4，WAIT时长
			V5,	=1 响一次，=0循环鸣叫
*@retval: 
*@notes: 假设S_BUZZER_CTRL调用周期为8ms，V1=10，V2=15，V3=2，V4=50
		 ON=80ms，OFF=120ms，WAIT=400ms
		 |ON|OFF|ON|OFF|WAIT|ON|OFF|ON|OFF|WAIT|
**************************************/

#if 1
void S_BUZZER_ON(u16	R_BUZZ_ON, u16 R_BUZZ_OFF, u16 R_BUZZ_CNT,u16 R_BUZZ_WAIT,u16 R_BUZZ_ONCE)
{
//	_pa3 = 0;
//	if(F_TEST == 1)
//	{
//		//S_Delay_ms(5);
//		unsigned char i;
//		for (i = 0; i <7; i++)
//		{
//			//_pa3 = 1;
//			P_BUZZ_EN = 0;
//			GCC_DELAY(20);
//			//_pa3 = 0;
//			P_BUZZ_EN = 1;
//			S_Delay_us(i);
//		}
//		S_Delay_ms(1);
//	} 
	P_BUZZ_EN = 0;
//	if(F_CO_ALARM == 1)
//	{
//		S_Delay_ms(100);
//	}
	if((F_TEST == 1) || (F_CO_ALARM == 1))//||(F_BAT_ISLOW == 1)
	{
		S_Delay_ms(100);
	}
	R_BUZZON_CNTSET  = R_BUZZ_ON;
	R_BUZZOFF_CNTSET = R_BUZZ_OFF;
	R_BUZZ_SERIESSET = R_BUZZ_CNT;
	if(R_BUZZ_ONCE == 0)
	{
		R_BUZZWAIT_CNTSET = R_BUZZ_WAIT;
		F_BUZZER_ONCE = 0;
	}
	else
	{
		F_BUZZER_ONCE = 1;
	}
	F_BUZZER_EN = 1;
	R_BUZZER_STATE = 0x01;
	
	R_BUZZER_CNT = 0;
	R_BUZZER_SERIES = 0;
}
#endif

#if 0
#define BUZ_STATE_ON 0
#define BUZ_STATE_OFF 1
#define BUZ_STATE_CHIP 2
#define BUZ_STATE_WAIT 3
#define BUZ_STATE_STOP 4 
//R_BUZZER_STATE= BUZ_STATE_ON //结合总体功能决定放置位置

void S_BUZZER_ON(u8	R_BUZZ_ON, u8 R_BUZZ_OFF, u8 R_BUZZ_CNT,u8 R_BUZZ_WAIT,u8 R_BUZZ_ONCE)
{
	static	u8	R_BUZZER_CNT;
	static	u8	R_BUZZER_SERIES;
	P_BUZZ_EN = 0;
	if(F_TEST == 1)
	{
		S_Delay_ms(5);
	}
	R_BUZZON_CNTSET  = R_BUZZ_ON;
	R_BUZZOFF_CNTSET = R_BUZZ_OFF;
	R_BUZZ_SERIESSET = R_BUZZ_CNT;
	R_BUZZWAIT_CNTSET = R_BUZZ_WAIT;
		switch(R_BUZZER_STATE)
		{
			//蜂鸣器打开
			case BUZ_STATE_ON:
				//_pton = 1;
				_pa3 = 1;	
				//P_BUZZ_EN = 0;
				R_BUZZER_CNT++;
				if(R_BUZZER_CNT >= R_BUZZON_CNTSET)	
				{
					//F_BUZZ_SHIFT=1;
					//_pton = 0;
					//_pa3 = 0;
					R_BUZZER_CNT = 0;
			        R_BUZZER_STATE++;
				}		
			break;
			//===================================================
			//蜂鸣器关闭
			case BUZ_STATE_OFF:
				//_pton=0;
				_pa3 = 0;
				//P_BUZZ_EN = 1;			
				R_BUZZER_CNT++;				
				if(R_BUZZER_CNT >= R_BUZZOFF_CNTSET)
				{
					R_BUZZER_CNT = 0;
			        R_BUZZER_STATE++;
				}		//F_BUZZ_SHIFT=1;
					
			break;
			//===================================================
			//周期循环处理
			case BUZ_STATE_CHIP:
				//单次处理
				
								//	F_BUZZER_RESET = 1;
					R_BUZZER_SERIES++;
					if(R_BUZZER_SERIES>=R_BUZZ_SERIESSET)	//不能放到括号里面加
						{
						    if(F_BUZZER_ONCE)
							{    
								_pa3 = 0;
								R_BUZZER_STATE=BUZ_STATE_STOP;
								 
								//F_BUZZER_RESET=0;
							    //F_BUZZER_STOP=1;
							}
							else
							{
 								//F_BUZZER_RESET = 0;
								R_BUZZER_SERIES = 0;
								F_BUZZER_ONEPERIOD = 1;
								R_BUZZER_CNT = 0;
			                    R_BUZZER_STATE++;		//完成一个短周期
								//F_BUZZ_SHIFT = 1;
  							}
						   
						}						
			break;
			//===================================================
			//等待处理
			case BUZ_STATE_WAIT:
			//	_pton=0;
				_pa3 = 0;
				//P_BUZZ_EN = 1;
				//F_BUZZER_RESET=0;
				R_BUZZER_CNT++;
				if(R_BUZZER_CNT>=R_BUZZWAIT_CNTSET)
				{
					R_BUZZER_CNT = 0;
					R_BUZZER_STATE = BUZ_STATE_ON;

				}		//F_BUZZER_RESET=1;
				
			break;
			//===================================================
			//
			case BUZ_STATE_STOP: 
				R_BUZZER_CNT = 0;
		    	R_BUZZER_SERIES = 0;
			                     
				//_pton = 0;
				//_pa3 = 0;
			break;
			
		}
}
#endif

/**************************************
*@neam:Horn_OFF
*@brief:蜂鸣器关闭 
*@param[in]:
*@retval: 
*@notes:
**************************************/
void Horn_OFF(void)
{
	if(!F_CO_ALARM)P_BUZZ_EN = 1;
	#if F_PWM
		_pton = 0;
	#else
		_pa3 = 0;
	#endif
	F_BUZZER_EN = 0;
	R_BUZZER_STATE = 0;
}

#if 0
/**************************************
*@neam:buzzer chirp once
*@brief: 
*@param[in]:
*@retval: 
*@notes: v1 大循环周期， V2  每次响等待时间， V3 响的次数  V4 中间等待时间 V5 中间等待几次停止
**************************************/

void Chirp_Once(u16 Chirp_Time,u16 Wait_Time,u8 Chirp_Count, u16 Mid_Wait,u8 Stop_Count)
{
	static u16 Chirp_Wait = 0,Mid_Wait_Time = 0;
	static u8 Buzzer_Chirp_Count = 0;
	static u16 Buzzer_Chirp_Time = 0;
	static u8 R_BUZZER_FG = 0,Stop_Cnt = 0;
	
	Buzzer_Chirp_Time ++;
	if(Buzzer_Chirp_Time >= Chirp_Time)
	{
		switch (R_BUZZER_FG)
		{
			case 0:
				P_BUZZ_EN = 0;
				_pa3 = 1;
				//开等待时间
				Chirp_Wait ++;
				if(Chirp_Wait >= Wait_Time)
				{
					Chirp_Wait = 0;
					R_BUZZER_FG = 1;
				}
			break;	
			
			case 1:
				//关闭蜂鸣器
				P_BUZZ_EN = 1;
				_pa3 = 0;
				//关闭等待时间
				Chirp_Wait++;
				if(Chirp_Wait >= Wait_Time)
				{
					Chirp_Wait = 0;
					Buzzer_Chirp_Count ++;
					if(Buzzer_Chirp_Count >= Chirp_Count)
					{
						Buzzer_Chirp_Count = 0;
						if(Mid_Wait > 0)R_BUZZER_FG = 2;	
					}
					else
					{
						R_BUZZER_FG = 0;	
					}
				}
			break;
			
			case 2:
				Mid_Wait_Time ++;
				if(Mid_Wait_Time >=Mid_Wait) 
				{
					Mid_Wait_Time = 0;
					Stop_Cnt++;
					if(Stop_Cnt >= Stop_Count)
					{
						Buzzer_Chirp_Time = 0;
						R_BUZZER_FG = 3;
						Stop_Cnt = 0;	
					}
					else
					R_BUZZER_FG = 0;
				}
			break;
			default:
			break;	
		}
	}
}
#endif