/*========================================================
*@File name:adc.c
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#include "adc.h"
#include "common.h"
#include "co.h"
#include "sys_function.h"
#include "eeprom.h"

/*u8 Read_ADC_OPAOUT_Value[2] = {0};*/
u16 average_counts = 0;
u16 wPrev_RawCOCount = 0;
u16	wRawCOCounts = 0;


/**************************************
*@neam:S_ADC_SET_CLKSRC
*@brief:设置ADC时钟源 
*@param[in]:
*@retval: 
*@notes:库中需要使用的函数，不要更改，将时钟源赋值给_acc,
**************************************/
#if 0
//void S_ADC_SET_CLKSRC(void)
//{
//	u8 R_ADC_TMP0=_acc;
//	_sadc1	&=0b11111000;
//	_sadc1	|=R_ADC_TMP0;
//	
//}
#endif

void ADC_CH_SELECT(u8 CH)
{
	u8	R_ADC_TMP1=CH;
	_sadc1	&=0b00011111;
	_sadc0	&=0b11110000;
	if(R_ADC_TMP1 == C_ADC_CH_AN6)
	{
		_pas17	=1;							//Pa7复用AN6
		_pas16	=0;
		_sadc0	|=0b00000110;
	}
	else
	{	_sadc0	|= 0b00001111;				//内部通道
		if(R_ADC_TMP1==C_ADC_CH_OPA)
		{
			_sadc1	|= 0b01100000;			//内部通道-OPA输出
			
		}
		if(R_ADC_TMP1==C_ADC_CH_TEMP)
		{
			_sadc1	|= 0b01000000;			//内部通道-温度传感器
		}
		if(R_ADC_TMP1==C_ADC_CH_VBG)
		{
			_sadc1	|= 0b00100000;			//内部通道-VBG
		}
	}
	
}

/**************************************
*@neam:READ_ADC_OPAOUT
*@brief:read ADC
*@param[in]:V_OPPAPV_OPOUT/ADC_BI
*@retval: ADC Value		 Reference voltage:VLDO 2.2
*@notes:
**************************************/
u16 Read_ADC_OPAOUT(u8 ADC_BIT)
{
	u16 u16_OPA_Value = 0;	
		
#if 0
	S_UART_SEND_BYTE('\n');
	S_UART_SEND_Str("_adrfs:");
	S_UART_SEND_ASCII_Dec(_adrfs);
	S_UART_SEND_BYTE('\r');
#endif
	_adcen	=1;
	
	GCC_DELAY(12);
	_start	=0;
	_start	=1;
	_start	=0;
	GCC_DELAY(12);
	while(_adbz);
	
//	Read_ADC_OPAOUT_Value[1] = _sadoh;
//	Read_ADC_OPAOUT_Value[0] = _sadol;
		
	_adcen	=0;
	
	if(ADC_BIT)
	{
		u16_OPA_Value = (_sadoh <<8)| _sadol;	
	}
	else
	{
		u16_OPA_Value = (_sadoh<<4) | (_sadol>>4);
	}
	return u16_OPA_Value;
}


/**************************************
*@neam:READ_CO_Value
*@brief:read ADC 
*@param[in]:
*@retval: V_VOPINP = V_OPPAP  R1=680K R2=0  
*@notes:  Max V_OPOUT=2905
**************************************/
u16 Read_CO_Value(void)
{	
	u16 CO_ADC = 0;
	
	_adrfs = 1;
	_regsw = 1;
	_savrs0 = 0;
	_savrs1 = 0;
	//_acc = 0x02;
	ADC_CH_SELECT(C_ADC_CH_OPA);
	
//	_sadc1 =0x65;
//	_sadc0 =0x1f;
	
	CO_ADC = Read_ADC_OPAOUT(ADC_12BIT);

	return CO_ADC;
}

void CO_Sample(void)
{
	
	//if(!F_TEST)
	{
		static u8 Falut_Sample_Count = 0;
		//wPrev_RawCOCount = wRawCOCounts;
		
		if(F_CO_FAULT_FLAG)
		{
			Falut_Sample_Count ++;
			if(Falut_Sample_Count >= 5)					//The first 5 seconds are not sample
			{
				//_pb2 = 1;
				wRawCOCounts = Read_CO_Value();
				//_pb2 = 0;
		
				if(Falut_Sample_Count >=10)	//60		
				{
					Falut_Sample_Count = 0;	
				}
			}
			else
			{
				//nothing to do?	
			}
		}
		else
		{
			//_pb2 = 1;
			wRawCOCounts = Read_CO_Value();
			//_pb2 = 0;	
		}
		
		//_pb2 = 1;
	//	Temp = Read_CO_Value();
	//	//_pb2 = 0;
	//	
		
		//average_counts = (average_counts*3 + wRawCOCounts)/4;
		
		average_counts = (wPrev_RawCOCount*7 + wRawCOCounts)/8;
		
//		wPrev_RawCOCount = wPrev_RawCOCount + wPrev_RawCOCount + wPrev_RawCOCount + 
//							wPrev_RawCOCount + wPrev_RawCOCount + wPrev_RawCOCount + wPrev_RawCOCount;
//		average_counts = wPrev_RawCOCount + wRawCOCounts;
//		average_counts = average_counts/8;

		wPrev_RawCOCount = average_counts;
	}
	
	//==================================================
	//druing ppt  short, the OPA always 0 or 1,then can calibration OPA again
	static u8 ptt_short_sensor = 0;
	
	if((average_counts == 0) || (average_counts == 1))
	{
		ptt_short_sensor ++;
		if(ptt_short_sensor >= 10)
		{
			ptt_short_sensor = 0;
			_ops2 = 1;
			Opamp_Calibration();
			_ops2 = 0;
		}
	}
	
}