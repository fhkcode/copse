/*========================================================
*@File name:sys init.h
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#ifndef	_A_SYS_INIT_H__
#define _A_SYS_INIT_H__

#include "BA45F6750.h"
#include "common.h"

extern u8 opvos_recovery; 
extern u16 DAY;

#define Mode_Serial_Output		1

extern volatile _byte_type 	F_MEMORY_FLAG;
#define	F_EEPROM_FALULT		F_MEMORY_FLAG.bits.bit0
#define	F_RAM_FAULT		F_MEMORY_FLAG.bits.bit1
#define	F_ROM_FAULT		F_MEMORY_FLAG.bits.bit1


unsigned int S_READ_ROM_data(unsigned int R_addr);
void S_EEPROM_SUMCHECK(unsigned char R_CHECA_FG);

void Disable_LVD(void);
void S_SYSTEM_INIT(void);
void S_Delay_ms(unsigned short cnt);
void S_ROM_SUMCHECK(void);
void Power_On_Load_Data(void);
void DATA_Backup(void);
void Data_Recovery(void);
void EOL_Check(void);

#endif

