//*************************************************************************************************************
//2021-2-20: create CO 2030-DCR project FHK 
//************************************************************************************************************ 
#include "BA45F6750.h"
#include "sys_init.h"
#include "sys_function.h"
#include "adc.h"
#include "buzzer.h"
#include "key.h"
#include "led.h"
#include "eeprom.h"
#include "key.h"
#include "co.h"
#include "uart.h"
#include "timer.h"

unsigned char R_UART_WAIT_CNT;
void LifespCheck(void);
void EOL_Check(void);
unsigned int Main_Average_Array(unsigned int * array, u8 array_size);
void IDE_Jude(void);
void Horn_Test(void);
u16 Key_Count = 0;
u8 BAT_Check_Count = 0;
unsigned int CO_Compensation_Array[8] ;

/**************************************
*@neam:main
*@brief:main  Principal function
*@param[in]:
*@retval: 
*@notes:
**************************************/
void main()
{
	//if((_to == 0) && (_pdf == 0))
	//{ 
		//System clock, IO and ram initialization
		S_SYSTEM_INIT();
		//
		//S_STM_INIT();	
		//Buzzer initialization	  
		#if F_PWM 		
		S_BUZZER_INIT();
		#endif
		
		//KEY initialization	
		//S_KEY_INIT();
			
		//ADC initialization
		//_ADC_SET_CLKSRC(C_ADC_CLK_FSYS_DIV64)		//C_ADC_CLK_FSYS_DIV32 根据datasheet adc 采样频率125为64分频
		_sadc1	&=0b11111000;
		_sadc1	|=0b00000110;
		
		
		//Set LDO --2.2V
		_regc|=0B00000010;
			
		//UART initialization
		S_UART_INIT();
		//S_UART_SEND_HEAR();
		  	
		//CO initialization
		CO_Init();	

		//timer 8ms
		//_pscr = 0b00000010;
		_psc0r = 0b00000010;
	
		//Read battery voltage 
		S_BAT_LOWCHECK();
		
	
		//wait opa stable mode
		F_POWERON_MODE = 1;
		
		
		//Global interrupt
		_emi=1;
		//

		//===================================================
		//calibration clock
		S_Delay_ms(250);
		S_Clock_CALC_Enable();
		S_Delay_ms(250);	
		S_Clock_CALC_Enable();
		S_Delay_ms(250);	
		_tb0e = 1;	
		//===================================================
		//if init over time maybe over feed watchdog time
		//GCC_CLRWDT();
		//power on read data from eeprom 
		Power_On_Load_Data();
		//COALM_Init();		//can remove, init variable set to 0 is OK
	//}
	
	while(1)
	{
		//===================================================
		//Horn test, if short hardware
		Horn_Test();
		
		//feed watchdog
		GCC_CLRWDT();
		//tb1 8ms
		_tb1c  = 0b10000000;	
		//tb0 1.024s
		_tb0c  = 0b10000111;	
		
		//===================================================
		//8ms cycle
		if(F_TIMER == 1)				
		{
			F_TIMER = 0;
			
			//===================================================
			//button scan
			Button_Scan();

			//===================================================
 			//uart process	
 			S_UART_PROCESS();
	
			//===================================================
			//system mode deal
			//S_SYSMODE_DEAL();
			
			//===================================================
			//LED driver

			S_LED_FLASH_CTRL();

			//===================================================
			//horn driver
			S_BUZZER_CTRL();
			
		}
		//===================================================
		//1s cycle
		if(F_OneSec == 1)
		{
			F_OneSec = 0;
			F_ONESEC_MODE=1;
			{
				//===================================================
				//uart over time count
				if(R_UART_WAIT_CNT != 0)
				{
					R_UART_WAIT_CNT--;
				}
				
				//===================================================
				//stable count
				if(F_POWERON_MODE==1 && R_START_CNT!=0) R_START_CNT--;
				if(R_START_CNT == 0)
				{
					F_POWERON_MODE = 0;

				}
				//===================================================
				//low battery check	
				Battery_test();
				
				//===================================================
				//saple co adc				
				CO_Sample();
				
				//===================================================
				//Calculate PPM
				if(FLAG_RECOVERY_CORR_ACTIVE)
				{
					Calculate_PPM(Recovery_Corr_Fac + average_counts) ;
				}
				else
				{
					Calculate_PPM(average_counts);	
				}
				
				
				//===================================================
				//High PPM recover
				Recovery_Process();
					
				//===================================================		
				//co sensor check 					
				CO_ERR_CHECK();
				
				//===================================================
				//CO calibration cal PPM    check alarm								
				Calibration_Task();
				
				//===================================================
				//short senser process	
				Senser_Short_Process();
				
				//===================================================
				//life check 	
				LifespCheck();	
				
				//===================================================
				//sys mode process
				//S_SYSMODE_DEAL();	
			}
			//===================================================
			//sys mode process
			S_SYSMODE_DEAL();	
			
			//===================================================
			//uart output data
			S_UART_DATA_Output();	
			
			
			//===================================================
			//sleep jude
			IDE_Jude();	
		}		
	}
}


/**********************************************************
*Function: TB0 interrupt
*Description:   1s
*Return: 	None
***********************************************************/
DEFINE_ISR(TB0_ISR,0x1C)
{
	F_OneSec = 1;
	S_Clock_CALC();	
}

/**********************************************************
*Function: TB1 interrupt
*Description:   8ms/125ms
*Return: 	None
***********************************************************/
DEFINE_ISR(TB1_ISR,0x30)
{
	F_TIMER = 1;	
	S_TB1_INTERRUPT();
}

/**********************************************************
*Function: UART interrupt
*Description:   interrupt 
*Return: 	None
***********************************************************/
DEFINE_ISR(L_UART_INT,0x0C)
{
	//_pb2 = 0;
	S_UART_INTERRUPT();
	R_UART_WAIT_CNT = 10;	//32
	F_UART_RX_RUNING = 1;
	//_pb2 = 1;
}

/**********************************************************
*Function: clock calibration interrupt
*Description:  clock calibration interrupt 
*Return: 	None
***********************************************************/
DEFINE_ISR(L_STM_A_INT,0x2C)
{
	//S_STM_INTERRUPT();
	if( F_Clock_CALC_FG == 1)	
	{
		R_Clock_count ++;
	}
}

/**********************************************************
*Function: LVR interrupt
*Description:  <2.1V is LVR, <2.2V is LVD 
*Return: 	None
***********************************************************/
DEFINE_ISR(L_LVR_INT,0x10)
{
	if (_lvf == 1)
     {
        _lvf = 0;
        _lvrf = 1;
        if(_lvdo == 1)
        {
        	_wdtc = 0;	// Watchdog reset.
			while(1);	
        }
     }
}
/**********************************************************
*Function:LifespCheck
*Description:  updata 1 day 
*Return: 	None
***********************************************************/
void LifespCheck(void)
{
	if(FLAG_HISTORY_UPDATA == 1)
	{
		//save day
		S_WR_EE(Life_Day_LSB,DAY&0x00ff);
		S_WR_EE(Life_Day_MSB,(DAY&0xff00)>>8);	
		
		//eeprom cycle to check
		//cycle time 1 day
		S_EEPROM_SUMCHECK(0);		//first save new check sum
		S_EEPROM_SUMCHECK(1);		//1 is check eeprom sum

		EOL_Check();
		
		//if LOW Battery Hush，then hush 24Hour，24hour later，FLAG_FAULT_HUSH clear，
		//horn will on，if over 7 day，will  Battery fatal
		if(F_BAT_ISLOW)		//FLAG_FAULT_HUSH && 
		{
			static u8 Low_Bat_Fatal_Count = 0;
			//FLAG_FAULT_HUSH = 0;	
			Low_Bat_Fatal_Count ++;
			if(Low_Bat_Fatal_Count >= 7)
			{
				Low_Bat_Fatal_Count = 0;
				F_LOW_BAT_FATAL = 1;	
				F_LB_FATAL_M_INI = 0;
			}
		}
		
		//clear PTT FLAG
		if(S_RD_EE(PTT_FLAG_Address) == 1)
		{
			S_WR_EE(PTT_FLAG_Address,0x00);		
		}
		
		FLAG_HISTORY_UPDATA = 0;
	}
	
	//======================================================
	//0PPM OFFSET Compensation
	if(DAY >= 1440)
	{
		static u8 Offset_Compensation_Count = 0;
		static u8 Compensation_Ptr = 0;
		static unsigned int CO_PrevCompensationAverage = 0;
		Offset_Compensation_Count ++;
		if(Offset_Compensation_Count >= 30)
		{
			Offset_Compensation_Count = 0;
			CO_Compensation_Array[Compensation_Ptr]	 = wRawCOCounts;
			if (++Compensation_Ptr >= 8)
			Compensation_Ptr = 0 ;
			
			unsigned int CO_Average = Main_Average_Array(CO_Compensation_Array,8);
		
			if(CO_Average > OPPM_OFFSET)
			{
				CO_Compensation = 0;
			}
			else
			{
				if(CO_Average > OPPM_OFFSET/2)	
				{
					//compensation fault	
				}
				
				if (CO_Average > CO_PrevCompensationAverage)
				{
					if ( (CO_Average - CO_PrevCompensationAverage) > 6)
					{
						CO_Compensation = OPPM_OFFSET - CO_Average ; 
					}
				}
				else
				{
					if ( (CO_PrevCompensationAverage - CO_Average) > 6)
					{
						CO_Compensation = OPPM_OFFSET - CO_Average ; 
					}
				}
			}
			
			CO_PrevCompensationAverage = CO_Average ;
		}		
	}
}

/**********************************************************
*Function:Main_Average_Array
*Description: Returns the average value of an array give the 	
*Return: 	None
***********************************************************/
unsigned int Main_Average_Array(unsigned int * array, u8 array_size)
{
	unsigned long sum = 0 ;
	unsigned long average ;
	unsigned int i ;
	
	// Sum the array.				
	for (i=0 ; i<array_size ; i++)
	{
		sum += *(array + i) ;	
	}
	
	// Scale, sum up and divide to get average.  Only right shift by one
	// so that the LSB can be checked for rounding.
	sum = sum<<2 ;
	sum /= array_size ;
	average = sum>>1 ;
	
	// If the LSB is 1 (which is 1/2), add one to the result.  This causes
	// round up if the result is over .5.
	if (average & 0x01)
	{
		// Result was odd.  Right shift and add 1.
		return ((unsigned int)((average>>1) + 1)) ;
	}
	else
	{
		return ((unsigned int)(average>>1)) ;
	}
}

/**********************************************************
*Function:EOL_Check
*Description:  >= 3750 is EOL , >=3757 is EOL Fatal
array and the array size	
*Return: 	None
***********************************************************/
void EOL_Check(void)
{
	if(DAY >= 3750)		//3650 + 30  day
	{
		if((FLAG_EOL_HUSH == 0) && (F_LIFE_REACH == 0))
		{
			F_LIFE_REACH = 1;
			F_EOL_M_INI = 0;	
		}

			
		if(DAY >= 3757)
		{
			if(FLAG_END_FATAL == 0)
			{
				FLAG_END_FATAL = 1;
				F_EOL_M_INI = 0;
				F_LIFE_REACH = 0;
				FLAG_EOL_HUSH = 0;
				
				#if Mode_Serial_Output
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("EOL Fatal");
				S_UART_SEND_BYTE('\r');	
				#endif
			}
		}
		else
		{
			#if Mode_Serial_Output
			S_UART_SEND_BYTE('\n');
			S_UART_SEND_Str("EOL");
			S_UART_SEND_BYTE('\r');	
			#endif	
		}
	}
	else
	{
		F_LIFE_REACH = 0;
		FLAG_END_FATAL = 0;	
	}		
}

/**********************************************************
*Function:Sleep mode jude
*Description:  	
*Return: 	None
***********************************************************/
void IDE_Jude(void)
{
	F_SYS_WMODE = 0;
	//timer TB1 must run idle 1 mode 
	
	if(F_CO_ERR  == 1)			F_SYS_WMODE = 1;
	if(F_TEST == 1)				F_SYS_WMODE = 1;
	if(F_LED_FLASH_EN == 1)		F_SYS_WMODE = 1;
	if(F_BUZZER_EN == 1) 		F_SYS_WMODE = 1;
	if(F_LIFE_REACH == 1)		F_SYS_WMODE = 1;
	if(F_UART_RX_RUNING == 1)	F_SYS_WMODE = 1;
	if(Key_Count != 0)			F_SYS_WMODE = 1;
	if(F_CO_ALARM == 1) 		F_SYS_WMODE = 1;
	if(FLAG_PTT_WAIT ==1 )		F_SYS_WMODE = 1;
				
	if(F_SYS_WMODE == 0)
	{
		//low power
		_tb1on = 0;
		_tb0f = 0;
		_fhiden = 0;
		_fsiden = 1;
		GCC_HALT();			
	}
	else
	{
		//idle 1
		_tb1f = 0;
		_fhiden = 1;
		_fsiden = 1;
		GCC_HALT();		
	}
}


/**********************************************************
*Function:horn test
*Description:  	
*Return: 	None
***********************************************************/
void Horn_Test(void)
{
	//while(F_BUZZER_KEEP_ON)
	
	if(Sys_Cal_State == 5)
	{
		while(F_BUZZER_KEEP_ON)
		{
			_pa5 = 0;
			_pa3 = 1;
			GCC_CLRWDT();	
		}	
	}	
}