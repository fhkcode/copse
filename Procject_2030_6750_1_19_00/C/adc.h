/*========================================================
*@File name:adc.h
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#ifndef	_ADC_H__
#define _ADC_H__

#include "common.h"
#include "uart.h"
#include "sys_init.h"
#include "BA45F6750.h"

#define	ADC_8BIT	0		//8bit
#define	ADC_12BIT	1		//12bit

#define	_ADC_SET_CLKSRC(a)    {_acc=a;S_ADC_SET_CLKSRC();}
//时钟源参数选择
#define	C_ADC_CLK_FSYS					0b00000000	//A/D CLK = Fsys
#define	C_ADC_CLK_FSYS_DIV2				0b00000001	//A/D CLK = Fsys/2
#define	C_ADC_CLK_FSYS_DIV4				0b00000010	//A/D CLK = Fsys/4
#define	C_ADC_CLK_FSYS_DIV8				0b00000011	//A/D CLK = Fsys/8
#define	C_ADC_CLK_FSYS_DIV16			0b00000100	//A/D CLK = Fsys/16
#define	C_ADC_CLK_FSYS_DIV32			0b00000101	//A/D CLK = Fsys/32
#define	C_ADC_CLK_FSYS_DIV64			0b00000110	//A/D CLK = Fsys/64
#define	C_ADC_CLK_FSYS_DIV128			0b00000111	//A/D CLK = Fsys/128
//-------------------------------------------------------------------------
#define	C_ADC_CH_AN6			0x01
#define	C_ADC_CH_OPA			0x02
#define	C_ADC_CH_TEMP			0x04
#define	C_ADC_CH_VBG			0x08

/*extern u8 Read_ADC_OPAOUT_Value[2];*/

//-------------------------------------------------------------------------
extern u16 average_counts;
extern u16 wPrev_RawCOCount;
extern u16 wRawCOCounts;

//void S_ADC_SET_CLKSRC(void);
void ADC_CH_SELECT(u8 CH);

u16 Read_ADC_OPAOUT(u8 ADC_BIT);
u16 Read_CO_Value(void);
void CO_Sample(void);

#endif


