/*========================================================
*@File name:sys function.h
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#ifndef	__SYS_FUNCTION_H__
#define __SYS_FUNCTION_H__

#include "common.h"
#include "BA45F6750.h"

//=====================================
#define  _DEBUG				0
//=====================================
#define	 C_SOFT_TIMER_CNT	125

//=====================================
//年份基数记录地址(MCU EEPROM)
//=====================================
#define	C_BASEYEAR_ADDR		0x48
//寿命记录地址(MCU EEPROM)	04BH ~ 04EH
#define	 C_LIFESPWAN_ADDR	0x4B
//=====================================
//寿命设定值
//=====================================
#define C_LIFESPWAN_YEAR	0x03	//3年
#define	C_LIFESPWAN_MONTH	10		//10个月
//寿命到达闪灯周期，单位s
#define	C_LIFESW_LED_PERIOD	10
//;故障检测周期，单位s
#define	C_ERR_CHK_PERIOD	10
//低压检测设置
#define	C_BAT_LOW_SET		117		//2.63V		2.7V 114  
#define	C_BAT_LOW_CNTSET	3
//电压检测周期
#define	C_BATVOL_CHK_PERIOD	15
//自动标定限制
#define	C_AUTOBD_LIMIT		450
#define	C_AUTOBD_LIMITA		(C_AUTOBD_LIMIT>>8)
#define	C_AUTOBD_LIMITB		(C_AUTOBD_LIMIT)&0XFF
//=====================================
//报警记录总数
#define	C_ALARMLOG_NUMADR			0x70
//报警恢复记录总数
#define	C_ALARESLOG_NUMADR			0x71
//故障记录总数
#define	C_ERRLOG_NUMADR				0x72
//故障恢复总数
#define	C_ERRRESLOG_NUMADR			0x73
//掉电记录总数
#define	C_PWRDOWNLOG_NUMADR			0x74
//上电记录总数
#define	C_PWRUPLOG_NUMADR			0x75
//传感器失效记录总数
#define	C_SENSORLOG_NUMADR			0x76
//校验码
#define	C_LOGNUM_CHKADR				0x77

//=====================================
//各项记录总数限制
//=====================================
#define	C_ALARMLOG_LIMIT			200
#define	C_ALARMRESLOG_LIMIT			200
#define	C_ERRLOG_LIMIT				100
#define	C_ERRRESLOG_LIMIT			100
#define	C_PWRDOWNLOG_LIMIT			50
#define	C_PWRUPLOG_LIMIT			50
#define	C_SENSORLOG_LIMIT			1


//预热相关宏定义
#define	C_GAS_PWON_RECORD_CNT		8
//故障周期
#define	C_ERR_PROM_PERIOD			30
//闪灯周期
#define	C_NOR_LED_PERIOD			40
//预热时间
#define	C_START_CNT_SET				120
//掉电检测周期
#define	C_PWRDOWN_PERIOD			40
//单点报警浓度设定(PPM)
#define	C_CO_ALARM_VAL				180
#define	C_CO_ALARM_CAL				40

#define	 P_RS_TX	_pa2
#define	 PC_RS_TX	_pac2
#define	 PU_RS_TX	_papu2

#define	 P_V_EN		_pa7
//=================================
//开关量
//=================================


//#define  _EEPROM_READ(a)	    {_acc=a; S_EEPROM_READ();}
//#define  _EEPROM_WRITE(a,b)		{R_EEPROM_DATA=b; _acc=a; S_EEPROM_WRITE();}
//#define	_RS232_IO_ENABLE		{PC_RS_TX=0;P_RS_TX=1;}
//#define	_RS232_IO_DISABLE		{PC_RS_TX=1;PU_RS_TX=1;}


#define	DATA_MSB(a)	 (u8)(((u16)a&0xff00)>>8)	

#define	_LED_R_OFF	{P_LED_R=1;}
#define	_LED_G_OFF	{P_LED_G=1;}
#define	_LED_Y_OFF	{P_LED_Y=1;}
#define	_LED_R_ON	{P_LED_R=0;}
#define	_LED_G_ON	{P_LED_G=0;}
#define	_LED_Y_ON	{P_LED_Y=0;}
//#define	_OPEN_BUZZ	{_pton=1;}
//#define	_OFF_BUZZ	{_pton=0;}

extern volatile _byte_type	F_RUN_FLAG;
#define		F_CO_ALARM_MEMEORY		F_RUN_FLAG.bits.bit0
#define		F_END_LIFE				F_RUN_FLAG.bits.bit1
#define		F_CO_ALARM_CONSERVE		F_RUN_FLAG.bits.bit2
#define		F_LOW_BAT_FATAL			F_RUN_FLAG.bits.bit3
#define 	F_BAT_TEST_MONITOR		F_RUN_FLAG.bits.bit4
#define		F_MEMORY_FAULT			F_RUN_FLAG.bits.bit5
#define		F_PTT_FAULT				F_RUN_FLAG.bits.bit6
#define 	F_STUCK_FALUT			F_RUN_FLAG.bits.bit7

extern volatile _byte_type			R_SYS_FG2;
#define 	FLAG_PWR_12_Min			R_SYS_FG2.bits.bit0
#define 	FLAG_HISTORY_UPDATA		R_SYS_FG2.bits.bit1
//#define		FLAG_ALARM_MEMORY_C		R_SYS_FG2.bits.bit2
#define 	FLAG_EOL_HUSH			R_SYS_FG2.bits.bit3	
#define 	FLAG_END_FATAL			R_SYS_FG2.bits.bit4
#define 	FLAG_PTT_CONDITION		R_SYS_FG2.bits.bit5
#define 	FLAG_PTT_FAULT_HUSH		R_SYS_FG2.bits.bit6
#define		FLAG_LOW_BAT_HUSH		R_SYS_FG2.bits.bit7

extern volatile _byte_type			R_SYS_FG3;
#define		FLAG_MEMORY_FALUT_HUSH	R_SYS_FG3.bits.bit0
#define 	FLAG_FAULT_HUSH			R_SYS_FG3.bits.bit1
#define 	FLAG_PTT_WAIT			R_SYS_FG3.bits.bit2
//#define		FLAG_EOL_HUSH			R_SYS_FG3.bits.bit3
#define		FLAG_SHORT_MEMORY		R_SYS_FG3.bits.bit3
#define		FLAG_ALARM_MEMORY_HUSH	R_SYS_FG3.bits.bit4
#define		FLAG_ALARM_MEMORY_SHORT	R_SYS_FG3.bits.bit5	
#define		FLAG_CO_ERR_ENABLE		R_SYS_FG3.bits.bit6	
#define		FLAG_RECOVERY_CORR_ACTIVE	R_SYS_FG3.bits.bit7

extern u8 R_MODE_FG;
extern u16 Time_Rate;
//void S_SysTimeTask(void);
void SYSMODE_DEAL(void);
void S_SYSMODE_DEAL(void);
void S_BAT_LOWCHECK(void);
void S_UART_PROCESS(void);
void S_UART_DATA_Output(void);
//void S_UART_DATA_CO_Ooutput(void);
//void S_UART_SEND_HEAR(void);
void Read_EEPROM(void);

#if 1
void Battery_test(void);
#endif

#endif


