/*========================================================
*@File name:sys init.c
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#include "sys_init.h"
#include "common.h"
#include "co.h"
#include "eeprom.h"
#include "sys_function.h"
#include "key.h"
#include "led.h"
#include "uart.h"
#include "buzzer.h"
#include "adc.h"

u8 opvos_recovery = 0; 
u16 DAY = 0;

unsigned char R_TMP0 = 0;
unsigned int R_ROM_Cheaksum = 0;
volatile _byte_type 	F_MEMORY_FLAG;

/**************************************
*@neam:Disable_LVD
*@brief:Disable_LVD 
*@param[in]:
*@retval: 
*@notes:
**************************************/
void Disable_LVD(void)
{
	_vbgen = 0;		//disable bandgap 
	_lvden = 0;		//disable lvd 
	_lve   = 0;		//disable lvd interrupt	
}

/*unsigned int R_CACL_tmp = 0;*/
/**************************************
*@neam:S_RAM_INIT
*@brief:RAM初始化  
*@param[in]:
*@retval: 
*@notes:
**************************************/
static void S_RAM_INIT(void)
{
	u8 i;
	_mp1h = 0;
	_mp1l = 0x80;
	while((_mp1h < 8))			//2 bank RAM , if is 1,only clear 1 BANK RAM
	//do
	{
		for(i = 0x00;i < 128;i++)
		//for(i =128; i > 0; i --)
		{
			_iar1 = 0;
			_mp1l++;
		}
		_mp1l = 0x80;
		_mp1h++;
	}
	//while(_mp1h ==3);
	_mp1h = 0;
}
/**************************************
*@neam:S_SYSTEM_INIT
*@brief:初始化系统设置  
*@param[in]:
*@retval: 
*@notes:
**************************************/
void S_SYSTEM_INIT(void)
{
	
	_scc	=0b00000001;			//初始化系统时钟源
	_hircc	=0b00001001;			//时钟系统时钟为8M
	_wdtc	=0b01010101;			//初始化WDT溢出周期为2s
	
	while(!_hircf);					//等待HITC稳定
	//===================================================
	#if 1
//	_vlvd0 = 0;
//	_vlvd1 = 1;
//	_vlvd2 = 0;		//set lvr voltage is 2.4V
//	_vbgen = 1;		//enable bandgap 
//	_lvden = 1;		//enable lvr 
//	_emi   = 1;		//Enable global interrupt
//	_lvf   = 0;		//clear lvf interrupt
//	_lve   = 1;	
//	_lvden = 1;	
//	
//	_wrf = 0;
//	_emi   = 0;		//disable global interrupt
	
	_emi   = 1;		//Enable global interrupt
	_lvf   = 0;		//clear lvf interrupt
	_lve   = 1;	
	_lvden = 1;	
	
	_lvdc = 0b00011001;
	//_lvdc = 00011010;		// Bit 2~0  VLVD2~VLVD0:  001: 000: 2.0V， 2.2V， 010: 2.4V， 011: 2.7V， 100: 3.0V
	_lvrf = 0;
	#endif
	
	S_Delay_ms(12);			// ptt last chirp 30 ms
	//===================================================	
	//初始化PORTA
	//PA3 PA4初始化调换
	_pa		=0b00000010;
	_papu	=0b00010010;
	_pac	=0b00010010;
	_pawu	=0b00010000;
	
//	_pa		=0b00000010;
//	_papu	=0b00001010;
//	_pac	=0b00001010;
//	_pawu	=0b00001000;

	#if F_PWM
		P_BUZZ_EN = 1;
		_pton = 0;
		F_BUZZER_EN = 0;
		R_BUZZER_STATE = 0;
	#endif
	//===================================================	
	//初始化PORTB
	_pb		=0b00000000;
	_pbpu	=0b00000000;
	_pbc	=0b00000000;	
	//===================================================
	//初始化PORTC
	_pc		=0b11111111;
	//_pcpu	=0b00000000;
	_pcpu	=0b11111111;
	_pcc	=0b00000000;
	//===================================================
	//初始化PORTD
//	_pd		=0b11111111;
//	//_pcpu	=0b00000000;
//	_pdpu	=0b11111111;
//	_pdc	=0b00000000;
//	//===================================================
//	//初始化PORTE
//	_pe		=0b11111111;
//	//_pcpu	=0b00000000;
//	_pepu	=0b11111111;
//	_pec	=0b00000000;
	//===================================================
	_pdc=0x00;
	_pdpu=0x00;
	_pd=0x00;
	_pec=0x00;
	_pepu=0x00;
	_pe=0x00;
	//===================================================
	//===================================================
	//BAKK初始化
	S_RAM_INIT();
	//===================================================

	#if 1
			//save day
	S_WR_EE(RESET_POWER_EVENT,DAY&0x00ff);
	S_WR_EE(RESET_POWER_EVENT+0x01,(DAY&0xff00)>>8);	
		
	u8 temp_reset_power_event = 0;
	temp_reset_power_event = S_RD_EE(RESET_POWER_EVENT+0x02);
	if(temp_reset_power_event >= 255) temp_reset_power_event = 0;
	temp_reset_power_event = temp_reset_power_event +1;
	//temp_reset_power_event +=1;
	S_WR_EE(RESET_POWER_EVENT+0x02,temp_reset_power_event);
	//S_WR_EE(RESET_POWER_EVENT,S_RD_EE(RESET_POWER_EVENT) >= 255 ? 0:S_RD_EE(RESET_POWER_EVENT)+1);
	#endif

	
	//key init
	//co save
	#if Old_Key
	R_KEY_RAM=0;   //to test delete
	if(1 == P_TEST_KEY)
	{
		R_KEY_RAM|=0x01;
	}
	#endif

	// STM timer init
	//===================================================
	_stmc0 = 0b00100000;	//fH/16
	_stmc1 = 0b11000001;	//定时/计数模式，A匹配
	
	//(1/fstm)*x
	//fstm = 8000000/16=500000
	//1/500000*500= 1ms
	_stmal = 500 & 0x0ff;
	_stmah = 500 >> 8;
	
	//STM中断使能
	_stmae = 1;
	//===================================================	
}

/**************************************
*@neam:S_Delay_ms
*@brief:
*@param[in]:cnt
*@retval: 
*@notes:
**************************************/
void S_Delay_ms(unsigned short cnt)
{
	//unsigned char i;
	unsigned short i;
	for (i = 0; i < cnt; i++)
	//for (i = cnt; i >0 ; i--)
	{
		GCC_CLRWDT();
		GCC_DELAY(2000);
	}	
}



/**************************************
*@neam:PWON_RECORD_DATA
*@brief:PWON_RECORD_DATA
*@param[in]:PWON_RECORD_DATA
*@retval: PWON_RECORD_DATA
*@notes:
**************************************/
void Power_On_Load_Data(void)
{
	#if New_Led
	P_LED_R = 1;
	P_LED_Y	= 1;
	P_LED_G = 1;
	#endif
	
	Sys_Cal_State=S_RD_EE(0x00);				//read calibration states
	Scale = (u16)(S_RD_EE(Cal_Scale_MSB) << 8) | S_RD_EE(Cal_Scale_LSB);	//read slope
	OPPM_OFFSET = S_RD_EE(ZERO_PPM_OFFSET);		//读取OPPM offset   Cal_0PPM_Value
	DAY = (u16)(S_RD_EE(Life_Day_MSB) << 8) | S_RD_EE(Life_Day_LSB);	//read data
	PCB_OFFSET = S_RD_EE(PCB_OFFSET_Add);		//read PCB  offset
	Time_Rate = 1;								//set time rate is 1
	//if((_to == 0) && (_pdf == 0))				//reset is power on
	if(_wrf == 0)
	{
		//clear alarm count
		COALM_Init();
		#if 0
		S_UART_SEND_BYTE('\n');
		S_UART_SEND_Str("power on:");
		S_UART_SEND_BYTE('\r');	
		#endif
	}
	else
	{
		_wrf = 0;
		//ptt reset on
		if(S_RD_EE(PTT_FLAG_Address) == 1)
		{
			w1AlarmPastSum = (u16)(S_RD_EE(D_wPastSum_H_Address) << 8) | S_RD_EE(D_wPastSum_L_Address);
			w1AlarmInterimSum = (u16)(S_RD_EE(D_InterimSum_H_Address) << 8) | S_RD_EE(D_InterimSum_L_Address);
			wAlarmSum = (u16)(S_RD_EE(D_AlarmSum_H_Address) << 8) | S_RD_EE(D_AlarmSum_L_Address);
	
			#if 0
			S_UART_SEND_BYTE('\n');
			S_UART_SEND_Str("PTT Recovery:");
			S_UART_SEND_BYTE('\r');	
			#endif
		}
	}
	
	if(Sys_Cal_State == 5)		//校准完成
	{
		R_START_CNT = 0;
		F_POWERON_MODE = 0;
		S_ROM_SUMCHECK();
		S_EEPROM_SUMCHECK(0);		//cal eeprom check sum to save eeprom
	}
	else
	{
		F_CO_BD_EN = 1;				//标定模式
		R_START_CNT = C_START_CNT_SET;
		
		
		//if enable 10k to sensor, should with pcb calibartion circut and 0PPM
		#if F_10K_ENBALE
		if((Sys_Cal_State == 0) || (Sys_Cal_State == 1))		
		{
			_ops6 = 1;	//10K
		}
		#endif
		
		#if F_10K_DISABLE
			R_START_CNT = 0;		//preheat tiem is 0
		#endif
	}
	

	_pa5 = 1;
	_papu5 = 1;
	_pac5 = 1;
	_pawu5 = 1;
	
	if(0==_pa5)
	{
		F_BUZZER_KEEP_ON = 1;
		if(Sys_Cal_State !=5)
		{
			Sys_Cal_State = 4;
		}
	}
	
	_pa5 = 0;
	_papu5 = 0;
	_pac5 = 0;
	_pawu5 = 0;
	
	//PCB calibration need wait 50s
	if(Sys_Cal_State == 0)
	{
		//_opc = 0b01000001;
		R_START_CNT = 0;	//50
		if(1==P_TEST_KEY)
		{
			FLAG_PCB_CAL_FLAG = 1;	
			FLAG_PCB_CAL_FAULT = 1; 		//PCB校准失败
		}
	}
	
	F_CO_CAL_FAULT = 1;

	//close lvd
	Disable_LVD();
	
	//check EOL Flag
	EOL_Check();
}

/**************************************
*@neam:DATA_BACPUP
*@brief:DATA_BACPUP
*@param[in]:DATA_BACPUP
*@retval: DATA_BACPUP
*@notes:
**************************************/
void DATA_Backup(void)
{
	u8 i;
	
	for(i = 0; i < 0x7F; i++)
	{
		//Back up the data read from the front to the data backup area behind
		S_WR_EE(0x80+i,S_RD_EE(i));
	}
}

/**************************************
*@neam:recover database
*@brief:
*@param[in]:
*@retval:
*@notes:
**************************************/
void Data_Recovery(void)
{
	u8 i;
	
	for(i = 0x80; i < 0xFF ; i --)
	{
		S_WR_EE(i - 0x80,S_RD_EE(i));	
	}	
}

/**************************************
*@neam:S_EEPROM_SUMCHECK
*@brief:EEPROM 计算校验和
*@param[in]:R_CHECA_FG 0为计算保存校验和，1为判断校验和
*@retval: 
*@notes:
**************************************/
void S_EEPROM_SUMCHECK(unsigned char R_CHECA_FG)
{
	unsigned char i;
	unsigned char R_checksum = 0;
	//u16 R_checksum = 0;
	
	//0x0D =13 128 = 0x80
	for(i=0; i < 127; i ++)
	//for(i=13; i >0 ; i--)
	{
		//R_cheaksum  = R_cheaksum + S_RD_EE(i);
		R_checksum  = R_checksum + S_RD_EE(i);
	}	
	
	//-----------------------------------------------
	//R_CHECA_FG greater than 0 means verify that the saved calculation sum is correct
	if(R_CHECA_FG == 1)
	{
		//if( R_cheaksum == S_RD_EE(EEP_Cheaksum_Address))
		//u16 Checksum_Temp = 0;
		unsigned char Checksum_Temp = 0;
		//Checksum_Temp = (u16)(S_RD_EE(EEP_Checksum_A_H) << 8) | S_RD_EE(EEP_Checksum_A_L);
		Checksum_Temp = S_RD_EE(EEP_Checksum_Address);
		if(R_checksum == Checksum_Temp)
		{
			F_EEPROM_FALULT = 0;
			//F_MEMORY_FAULT = 0;
		}
		else
		{
			Data_Recovery();
			for(i=0; i < 127; i ++)
			{
				R_checksum  = R_checksum + S_RD_EE(i);
			}
			
			if(R_checksum != Checksum_Temp)
			{
				F_EEPROM_FALULT = 1;
				
				#if Mode_Serial_Output
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("Memory Fault");
				S_UART_SEND_BYTE('\r');	
				#endif
				
				//save day
				S_WR_EE(MEMORY_FAULT_EVENT,DAY&0x00ff);
				S_WR_EE(MEMORY_FAULT_EVENT+0x01,(DAY&0xff00)>>8);	
				
				u8 temp_memory_fault_count = 0;
				temp_memory_fault_count = S_RD_EE(MEMORY_FAULT_EVENT+0x02);
				if(temp_memory_fault_count >= 255) temp_memory_fault_count = 0;
				temp_memory_fault_count = temp_memory_fault_count + 1;
				S_WR_EE(MEMORY_FAULT_EVENT+0x02,temp_memory_fault_count);
			}
			else
			{
				F_EEPROM_FALULT = 0;
			}
		}
	}
	//R_CHECA_FG is 0 to save the calculated value
	else
	{
		S_WR_EE(EEP_Checksum_Address,R_checksum);
		S_WR_EE(BACK_EEP_Csum_Address,R_checksum);	
//		S_WR_EE(EEP_Checksum_A_L,R_checksum&0x00ff);
//		S_WR_EE(EEP_Checksum_A_H,(R_checksum&0xff00)>>8);
	}
}
/**************************************
*@neam:S_Drift_Compensation
*@brief:读ROM地址数据
*@param[in]:R_addr(查询地址位置)(Query address location)
*@retval: i
*@notes:
**************************************/
unsigned int S_READ_ROM_data(unsigned int R_addr)
{
	unsigned int i = 0;
	_tbhp = ((R_addr)>>8)&0xFF;
	_tblp = (R_addr)&0xFF;

	asm("TABRD _R_TMP0");
	i = _tblh;
	i = i<< 8 | R_TMP0;
	//return i;
	return _tblh + R_TMP0;
}
/**************************************
*@neam:S_ROM_SUMCHECK
*@brief:ROM校验
*@param[in]:R_addrStart，R_addrEnd
*@retval: 
*@notes:计算为数据的高8位+低8位 结果保存为16位数据
		Calculate as the high 8 bits + low 8 bits of the data and save the result as 16 bits data
**************************************/
void S_ROM_SUMCHECK(void)
{
	//unsigned int R_cnt = 0;
	unsigned int i;
	unsigned int R_CACL_tmp = 0;
	//R_cnt = R_addrStart;
	
	//-----------------------------------------------
	//计算和
	//Calculate and
	//for(i = 0; i< (R_addrEnd - R_addrStart) + 1; i++)
	for(i = 0; i < (u16)0x1FFE +1; i++)	//0x0FFF
	{
		R_ROM_Cheaksum += S_READ_ROM_data(i);
		//R_ROM_Cheaksum = R_ROM_Cheaksum + (((S_READ_ROM_data(R_cnt))>>8)&0xFF) + ((S_READ_ROM_data(R_cnt))&0xFF);
		//R_cnt ++;
	}
	//-----------------------------------------------
	//读EEPROM数据
	//Read EEPROM data
	R_CACL_tmp = (u16)S_RD_EE(C_EEP_ROMChecksum_H)<< 8 | S_RD_EE(C_EEP_ROMChecksum_L);
	
	//-----------------------------------------------
	//计算为数据的高8位+低8位 结果保存为16位数据
	//Calculate as the high 8 bits + low 8 bits of the data and save the result as 16 bits data
	if(R_CACL_tmp == R_ROM_Cheaksum)
	{
		//F_ROM_FAULT = 0;
		F_MEMORY_FAULT = 0;
	}
	else
	{
		//F_ROM_FAULT = 1;
		F_MEMORY_FAULT = 1;
		F_MEMORY_FAULT_M_INI = 0;
		#if Mode_Serial_Output
		S_UART_SEND_BYTE('\n');
		S_UART_SEND_Str("Memory Fault");
		S_UART_SEND_BYTE('\r');	
		#endif
	}
	
	#if 0
	S_UART_SEND_BYTE('\n');
	S_UART_SEND_Str("F_MEMORY_FAULT:");
	S_UART_SEND_ASCII_Dec((F_MEMORY_FAULT&0xff00)>>8,F_MEMORY_FAULT&0x00ff);
	S_UART_SEND_BYTE('\r');	
	#endif
}