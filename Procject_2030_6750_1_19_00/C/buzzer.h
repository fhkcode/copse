/*========================================================
*@File name:buzzer.h
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#ifndef	_BUZZER_H__
#define _BUZZER_H__

#include "BA45F6750.h"
#include "common.h"

#define		F_PWM		0

#define		P_BUZZ_EN	_pa5

#if F_PWM
//PWM频率	8000000/16/155=3.2kHz
#define	C_BUZZ_FREQUENCY		77		//155
//PWM占空比	60/155
#define	C_BUZZ_DUTY				38	//60
#endif

#define	C_BUZZ_FREQUENCY		155		//155
//PWM占空比	60/155
#define	C_BUZZ_DUTY				145	//60


	
/*void S_BUZZER_INIT(void);*/
void S_BUZZER_CTRL(void);
void S_BUZZER_ON(u16 R_BUZZ_ON, u16 R_BUZZ_OFF, u16 R_BUZZ_CNT,u16 R_BUZZ_WAIT,u16 R_BUZZ_ONCE);
void Horn_OFF(void);

#if 0
void Chirp_Once(u16 Chirp_Time,u16 Wait_Time,u8 Chirp_Count, u16 Mid_Wait,u8 Stop_Count);
#endif

#endif



