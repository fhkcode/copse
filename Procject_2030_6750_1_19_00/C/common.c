/*========================================================
*@File name:common.c
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#include "common.h"

//ADC define

u8 R_ADC_DATA[2];		

//horn define
volatile _byte_type	R_BUZZ_FG;
u16	R_BUZZER_STATE;
u16	R_BUZZON_CNTSET;
u16	R_BUZZOFF_CNTSET;
u16	R_BUZZWAIT_CNTSET;
u16	R_BUZZ_SERIESSET;

u16	R_BUZZER_CNT;
u16	R_BUZZER_SERIES;


//system define
//u8	R_SOFT_TIMER_CNT;
u8	R_START_CNT;
//u8	R_PWRDOWN_CNT;
volatile _byte_type	R_SYS_FG;
volatile _byte_type R_INI_FG;
volatile _byte_type R_INI_FG1;
volatile _byte_type	R_SYS_FG1;
volatile _byte_type R_SYS_WORK_FG;

//led define
volatile _byte_type	R_LED_FG;
volatile _byte_type	R_LED_FG1;
u8	R_RED_SET_ON;
u8	R_RED_SET_OFF;

//LED define
u8	R_LED_STATE;
u8	R_LEDON_CNTSET;
u8	R_LEDOFF_CNTSET;
u16	R_LEDWAIT_CNTSET;
u8	R_LED_SERIESSET;


//EEPROM define
//u8	R_EEPROM_DATA;

//uart define
//u8	R_RS_CHEAKSUM;

//u8	R_UTX_BUFFER[16];
//u8	R_SFUART_RXCNT;
//u8	R_UART_RUNCNT;

//key define
volatile u8	R_KEY_RAM;
volatile u8	R_KEY_OLD;
volatile u8	R_KEY_CHANGE;
volatile u8	R_KEY_DEBOUNCE;
volatile _byte_type	R_KEY_FG;	

//low battery flag
volatile bit F_BAT_ISLOW;
u8 R_BAT_ADVAL;

_byte_type	R_UART_FG;
_byte_type  R_UART_CMD_FG;

_byte_type  R_Clock_CALC_FG;

