/*========================================================
*@File name:uart.c
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#include "uart.h"

unsigned char R_UART_BUFF[C_UART_RX_BUFFSIZE];
unsigned char R_UART_BUFFSIZE;


/**********************************************************
*Function:S_UART_SEND_BYTE
*Description: uart send byte	
*Return: 	None
***********************************************************/

void S_UART_INIT(void)
{
	_pac0 = 1;
	GCC_DELAY(2000);
	if(_pa0 == 1)
	{
		_papu0  = 0;
		_pac0 = 1;
		//RX config -PA0
		_pas01 = 1;
		_pas00 = 0;
		
		_ifs01 = 0;
		_ifs00 = 0;
		
		//TX config -PA2
		_pas05 = 1;
		_pas04 = 0;
		
		//config uart batde 8000000/(16*(25+1))=19230(19200)	//4000000/(16*(12+1))=19230(19200)
		
		_ubrg = 25;
		_simc0 = 0b11110000;
		_uucr1 = 0b10000000;
		_uucr2 = 0b11100100;
		
		//config urat interrupt enable
		//_usime = 1;
		_usi0e = 1;

		
		//enable uart wake up
		_uwake = 1;	
			
		S_UART_SEND_Str(">2030-DCR Ver 1.19.0\r\0");
	}
}

/*********************************************************
*Function:	S_UART_SEND_BYTE
*Description: uart send byte	
*Return: 	None
**********************************************************/
void S_UART_SEND_BYTE(unsigned char data)
{
	#if 0
	unsigned int overtime = 0;
	_utxr_rxr = data;
	while(_utidle == 0)
	{
		overtime++;
		if(overtime >= 2000)
		{
			break;	
		}
	}
	#endif
	
	#if 1
	/* Initialization */
	_utx8 = 0;

	/* waitting UART transmitter free */
//	while(!_utxif)		
//	{
//		_nop();
//	}	
	GCC_DELAY(1000);	//wait 0.5ms				
	 //end 	
	/************************************/
	
	//Write data to UART transmitter 
	_utxr_rxr = data;
	 //Write end 

	/************************************/

	 //Waitting UART transmit data finished
//	while(!_utidle)
//	{
//		_nop();
//	}
	GCC_DELAY(1000);	//wait 0.5ms
	/* transmit finished */	
	#endif
}

/**************************************************************************************************************************
													S_UART_SEND_Str
*Description: uart send string	
*Return: 	None
***************************************************************************************************************************/
void S_UART_SEND_Str(const char *data)
{
	while (*data != '\0')
	{
		S_UART_SEND_BYTE(*data);
		data++;
	}
}

/**************************************************************************************************************************
													_itoa
*Description:	itoa function is to convert numbers to a ascii characters in decimal or hexadecimal.
*Arguments:	number	the value to be converted.
			radix	10 for decimal, 16 for hexdecimal.
*Return: 	None
***************************************************************************************************************************/
//void _itoa(unsigned int number, unsigned char radix)
//{
//	// unsigned int raw;
//	// unsigned char R_tho,R_hun,R_ten;
//	
//	//raw = ((unsigned int) data_H << 8) | data_L;
//	if(F_DEBUG_OUTPUT)
//	{
//		const unsigned char *ptalpha = "0123456789ABCDEF";
//		unsigned char str[4];
//		char i = 0;
//
//		do 
//		{
//			str[i] = ptalpha[number % radix];
//			number /= radix;
//			i++;
//		}while((number / radix !=0)||(number % radix !=0));
//
//		if((radix == 16) && (i == 1))
//		{
//			S_UART_SEND_BYTE('0');
//		}
//		while(i != 0)
//		{
//			i--;
//			S_UART_SEND_BYTE(str[i]);
//		}
//	}
//}

/**********************************************************
*Function:S_UART_SEND_ASCII_Dec
*Description: uart send ascii dec	
*Return: 	None
***********************************************************/
void S_UART_SEND_ASCII_Dec(unsigned char data_H,unsigned char data_L)
{
	unsigned int R_ind;
	unsigned char R_tho,R_hun,R_ten;
	
	R_ind = ((unsigned int) data_H << 8) | data_L;
	R_tho = R_ind / 1000;
	R_ind %= 1000;
		
	R_hun = R_ind / 100;
	R_ind %= 100;
	
	R_ten = R_ind / 10;
	R_ind %= 10;
	
	//千位
	if(R_tho > 0)
	{
		S_UART_SEND_BYTE(R_tho + '0');
		S_UART_SEND_BYTE(R_hun + '0');
		S_UART_SEND_BYTE(R_ten + '0');
		S_UART_SEND_BYTE(R_ind + '0');
	}
	else
	{
		//百位
		if(R_hun > 0)
		{
			S_UART_SEND_BYTE(R_hun + '0');
			S_UART_SEND_BYTE(R_ten + '0');
			S_UART_SEND_BYTE(R_ind + '0');
		}	
		else
		{
			//十位
			if(R_ten > 0)
			{
				S_UART_SEND_BYTE(R_ten + '0');
				S_UART_SEND_BYTE(R_ind + '0');
			}
			else
			{
				//个位
				S_UART_SEND_BYTE(R_ind + '0');
			}
		}
	}
}

/**********************************************************
*Function:S_UART_SEND_ASCII_Hex
*Description: uart send ascii dec	
*Return: 	None
***********************************************************/
void S_UART_SEND_ASCII_Hex(unsigned char data)
{
	unsigned char i;
	//高位
	i = data & 0xF0;
	i >>= 4;
	if(i < 10)
	{	
		S_UART_SEND_BYTE(i + '0');
	}
	else
	{
		S_UART_SEND_BYTE( (i + 'A') - 10 );
	}
	//低位
	i = data & 0x0F;
	if(i < 10) 
	 {
	 	S_UART_SEND_BYTE(i + '0');
	 }	
	else
	{
		S_UART_SEND_BYTE( (i + 'A') - 10);
	}
}

/**********************************************************
*Function:S_UART_INTERRUPT
*Description: uart send ascii dec	
*Return: 	None
***********************************************************/
void S_UART_INTERRUPT(void)
{
	unsigned char i;
	i = R_UART_BUFFSIZE;
	if(_urxif == 1)
	{
		//数据接收
		if(R_UART_BUFFSIZE <= C_UART_RX_BUFFSIZE)
		{
			R_UART_BUFF[i] = _utxr_rxr;
			R_UART_BUFFSIZE ++;
		}
		if((R_UART_BUFFSIZE >= 11) || (i >= 11))
		{
			R_UART_BUFFSIZE = 0;
			i = 0;
		}
	}
}
/**********************************************************
*Function:S_UART_PTRESET
*Description: uart send ascii dec	
*Return: 	None
***********************************************************/
void S_UART_PTRESET(void)
{
	unsigned char i;
	F_UART_RX_RUNING = 0;
	R_UART_WAIT_CNT = 0;
	//接收长度清除
	R_UART_BUFFSIZE = 0;
	
	for(i = 0; i < C_UART_RX_BUFFSIZE; i++)
	{
		R_UART_BUFF[i] = 0;
	}
}

/**********************************************************
*Function:S_UART_FindByte
*Description: uart send ascii dec	
*Return: 	None
***********************************************************/
signed char S_UART_FindByte(unsigned char dat)
{
	signed char n = -1;
	unsigned char i;
	
	for (i = 0; i < C_UART_RX_BUFFSIZE; i++)
	{
		if (R_UART_BUFF[i] == dat)
		{
			n = i;
			break;
		}
	}
	
	return n;
}

/**********************************************************
*Function:S_UART_ASCII_TO_HEX
*Description: uart send ascii dec	
*Return: 	None
***********************************************************/
unsigned char S_UART_ASCII_TO_HEX(unsigned char data1,unsigned char data2)
{
	unsigned char R_UART_TMP[2]; 
	if(data1 <= '9')
	{
		R_UART_TMP[1] = data1 - '0';
	}
	else
	{
		R_UART_TMP[1] = data1 - 'A' + 10;
	}
	
	if(data2 <= '9')
	{
		R_UART_TMP[0] = data2 - '0';
	}
	else
	{
		R_UART_TMP[0] = data2 - 'A' + 10;
	}		
	
	R_UART_TMP[0] = (R_UART_TMP[1] << 4) | R_UART_TMP[0];

	return 	R_UART_TMP[0];
}

/**************************************************************************************************************************
													Uart_Ascii_To_Hex
*Description:	Convert the ascii characters to the hex format
*Arguments:	data1		high byte of hex
			data2		low byte of hex
*Return: 	hex format character
***************************************************************************************************************************/
//unsigned char Uart_Ascii_To_Hex(unsigned char data1,unsigned char data2)
//{
//	unsigned char _bytes[2] = {0}; 
//	if(FLAG_TX_ENABLE){
//		_bytes[1] = (data1 <= '9')? (data1 - 48) : (data1 - 55);	/* ascii 0 is number 48, ascii A is number 65*/
//		_bytes[0] = (data2 <= '9')? (data2 - 48) : (data2 - 55);	
//		_bytes[0] = (_bytes[1] << 4) | _bytes[0];
//	}
//	return 	_bytes[0];
//}