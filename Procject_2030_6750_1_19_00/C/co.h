/*========================================================
*@File name:co.h
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#ifndef	_CO_H__
#define _CO_H__

#include "common.h"

#define  F_10K_ENBALE			0	
#define  F_10K_DISABLE			1

//变量外部声明
extern volatile _byte_type 	R_CO_RUNFLAG;

#define	F_CO_BD_EN			R_CO_RUNFLAG.bits.bit0	//标定使能标志位
#define FLAG_PCB_CAL_FLAG	R_CO_RUNFLAG.bits.bit1	//电路偏移量有没有校准
//#define	F_CO_BD_INI			R_CO_RUNFLAG.bits.bit1
#define	FLAG_PCB_CAL_FAULT	R_CO_RUNFLAG.bits.bit2	//电路偏移量校准是否成功
//#define	F_CO_BD_PWON		R_CO_RUNFLAG.bits.bit2
#define	F_CO_BD_DONE		R_CO_RUNFLAG.bits.bit3	//标定程序完成（不管是否成功），需要手动清零
#define	F_CO_BD_F1_OK		R_CO_RUNFLAG.bits.bit4
#define	F_CO_CAL_FAULT		R_CO_RUNFLAG.bits.bit5	//标定OK
#define	F_CO_ERR			R_CO_RUNFLAG.bits.bit6	//传感器故障标志
#define	F_CO_ALARM			R_CO_RUNFLAG.bits.bit7	//报警标志位

extern volatile _byte_type 	R_CO_RUNFLAG1;
#define FLAG_CAL_OPPM_FLAG		R_CO_RUNFLAG1.bits.bit0		//0PPM 有没有校准
#define FLAG_CAL_0PPM_FAULT		R_CO_RUNFLAG1.bits.bit1		//0PPM 校准是否成功
#define FLAG_CAL_150PPM_FLAG	R_CO_RUNFLAG1.bits.bit2		//150PPM 有没有校准
#define FLAG_CAL_150PPM_FAULT	R_CO_RUNFLAG1.bits.bit3		//150PPM 校准是否成功
#define FLAG_CAL_300PPM_FLAG	R_CO_RUNFLAG1.bits.bit4		//300PPM 有没有校准
#define FLAG_CAL_300PPM_FAULT	R_CO_RUNFLAG1.bits.bit5		//300PPM 有没有校准
#define FLAG_HIGH_ERROR			R_CO_RUNFLAG1.bits.bit6		//高浓度
#define F_CO_FAULT_FLAG			R_CO_RUNFLAG1.bits.bit7		//CO sensor fault

extern volatile _byte_type 	R_CO_RUNFLAG2;
#define	SYSTEM_ALARM_MODE		R_CO_RUNFLAG2.bits.bit0
#define	SYSTEM_ALARM_CONSERVE	R_CO_RUNFLAG1.bits.bit1
#define	SYSTEM_EOL_FATAL		R_CO_RUNFLAG1.bits.bit2
#define SYSTEM_MEMORY_FAULT		R_CO_RUNFLAG1.bits.bit3
#define SYSTEM_STUCK_BUTTON		R_CO_RUNFLAG1.bits.bit4
#define	SYSTEM_EOL				R_CO_RUNFLAG1.bits.bit5
#define	SYSTEM_LOW_BAT_FATAL	R_CO_RUNFLAG1.bits.bit6
#define	SYSTEM_LOW_BAT			R_CO_RUNFLAG1.bits.bit7

//extern volatile _byte_type	R_CO_RUNFLAG2;
//#define	F_CO_BD_H_EN		R_CO_RUNFLAG2.bits.bit0
//#define	F_CO_BD_H_OK		R_CO_RUNFLAG2.bits.bit1
//#define	F_CO_BD_L_OK		R_CO_RUNFLAG2.bits.bit2

//extern volatile _byte_type	R_CO_RUNFLAG3;
//#define F_CO_SHORT_FLAG		R_CO_RUNFLAG3.bits.bit0
//#define F_CO_FAULT_FLAG		R_CO_RUNFLAG3.bits.bit1

//R_CO_LIB_AUX.bits.bit1已经占用
//extern volatile _byte_type 	R_CO_LIB_AUX;
//#define	F_CO_ERRCK_EN		R_CO_LIB_AUX.bits.bit0

//#define	F_AUTOBD_EN			R_CO_LIB_AUX.bits.bit2
//#define	F_HISTORY_ZERO		R_CO_LIB_AUX.bits.bit3
	


//==========================================

#define BSI_CONFIG		1
#define UL_CONFIG		0

// year correction percent
#define YEAR_PERCENT_INCREASE	2.5
#define PERCENT_PER_QTR 	((YEAR_PERCENT_INCREASE*1000) / 4)

// These constants are used with the Alarm Curve calculations.
// A value of 1.0 is equal to 0x10000, therefore these hex values are fractional
// constants.
#define AL_ALARM_SUM_CONSTANT		0xb80b		// == 0.7189
#define AL_PREALARM_SUM_CONSTANT	0x49fb		// == 0.289
#define	FUNC_CO_ALARM_THRESHOLD		40			// PPM

//
//  Definition Statments
//
//#ifdef	UL_CONFIG
//	
//	#define	AL_ALARM_THRESHOLD			8260
//	
//	//
//	//  The threshold limit constant sets the accumulator limit.  The accumulator
//	//  limit is not set to the alarm threshold because algorithm isn't allowed to
//	//  to decay quickly enough.  The limit is set by subtracting the PPM value or
//	//  a multiple thereof where the decay should begin, from the threshold.
//	//
//	#define	AL_ALARM_THRESHOLD_LIMIT		8108
//	#define	AL_REALARM_THRESHOLD			7489
//	
//	#define AL_ACCUMULATOR_MINIMUM_THRESHOLD	38
//	#define AL_ACCUMULATOR_COMP_THRESHOLD		38
//	#define AL_ACCUMULATOR_DECAY_CONSTANT		13
//	#define AL_ACCUMULATOR_DECAY_THRESHOLD		97
//	
//	#define AL_THRESHOLD_SLOPE_MULTIPLE		4
//	#define AL_THRESHOLD_SLOPE_HIGH_MULTIPLE	8
//	#define AL_THRESHOLD_SLOPE_THRESHOLD		228
//
//#endif
//
//
//#ifdef	_CONFIGURE_UL_ALARM_LINEAR
//
////	#define	AL_ALARM_THRESHOLD			5900
////	#define	AL_ALARM_THRESHOLD_LIMIT		5816
////	#define	AL_REALARM_THRESHOLD			5460
////	
////	#define AL_ACCUMULATOR_MINIMUM_THRESHOLD	42
////	#define AL_THRESHOLD_SLOPE_MULTIPLE		2
////
//#endif

//
//  BSI Alarm Definitions
//
//#ifdef	BSI_CONFIG
#if 0

	#define AL_RESET_SINGLE_THRESHOLD		82
	#define AL_RESET_INHIBIT_THRESHOLD		229
	
	#define	AL_ALARM_THRESHOLD			(unsigned short)4200//3590//8260
	
	//
	//  The threshold limit constant sets the accumulator limit.  The accumulator
	//  limit is not set to the alarm threshold because algorithm isn't allowed to
	//  to decay quickly enough.  The limit is set by subtracting the PPM value or
	//  a multiple thereof where the decay should begin, from the threshold.
	//
	#define	AL_ALARM_THRESHOLD_LIMIT		(unsigned short)4124//3514// 8108
	
	//
	//  The realarm threshold must be high enough to ensure the test button logic
	//  activates the alarm immediately.
	//
	#define	AL_REALARM_THRESHOLD			4074//3388
	
	#define AL_ACCUMULATOR_MINIMUM_THRESHOLD	(unsigned short)38//45
	#define AL_ACCUMULATOR_COMP_THRESHOLD		(unsigned short)32//38
	#define AL_ACCUMULATOR_DECAY_CONSTANT		(unsigned short)18//13
	#define AL_ACCUMULATOR_DECAY_THRESHOLD		(unsigned short)63//65//97
	
	#define AL_THRESHOLD_SLOPE_MULTIPLE			(unsigned short)3//2//4
	#define AL_THRESHOLD_SLOPE_HIGH_MULTIPLE	(unsigned short)14//8
	#define AL_THRESHOLD_SLOPE_THRESHOLD		(unsigned short)192	//206//228

#endif

#if 1

    #define AL_RESET_SINGLE_THRESHOLD		82
    #define AL_RESET_INHIBIT_THRESHOLD		229

    // Set Forced Alarm Condition Threshold at 200 PPM
    #define	CENELEC_300_PPM_THRESHOLD	200

    #define	AL_ALARM_THRESHOLD			4200

    //
    //  The threshold limit constant sets the accumulator limit.  The accumulator
    //  limit is not set to the alarm threshold because algorithm isn't allowed to
    //  to decay quickly enough.  The limit is set by subtracting the PPM value or
    //  a multiple thereof where the decay should begin, from the threshold.
    //
    #define	AL_ALARM_THRESHOLD_LIMIT		4124

    //
    //  The realarm threshold must be high enough to ensure the test button logic
    //  activates the alarm immediately.
    //
    #define	AL_REALARM_THRESHOLD			4074

    #define AL_ACCUMULATOR_MINIMUM_THRESHOLD	38
    #define AL_ACCUMULATOR_COMP_THRESHOLD		32
    #define AL_ACCUMULATOR_DECAY_CONSTANT		18
    #define AL_ACCUMULATOR_DECAY_THRESHOLD		63
    #define	AL_ACCUMULATOR_FAST_DECAY			150

    #define AL_THRESHOLD_SLOPE_MULTIPLE		3
    #define AL_THRESHOLD_SLOPE_HIGH_MULTIPLE	14
    #define AL_THRESHOLD_SLOPE_THRESHOLD		192

    #define AL_CENELEC_PPM_AVG_MAX			90	//Performing Running Average if < 90 PPM

    #define AL_CENELEC_50_PPM_MAX			70
    #define AL_CENELEC_50_PPM			53
    #define AL_CENELEC_50_PPM_MIN			41

    #define AL_ACCUMULATOR_CENELEC_FAST_DECAY	150	//Not Currently Used




    #define CO_PRESENT_THRESHOLD			100
#endif

//后增加
extern u8 PCB_OFFSET;
extern u8 Sys_Cal_State;
extern u16 OPPM_OFFSET;
extern u16 Scale;
extern u16 wPPM;
extern u16 wPrev_wPPM;
extern u8 OPPM_Max;
extern u8 CO_Compensation;
extern u16 Recovery_Corr_Fac;
extern u16 Recovery_Time_Com;

extern u8 Recovery_State;

extern unsigned int w1AlarmPastSum ;	
extern unsigned int w1AlarmInterimSum ;	
extern unsigned int wAlarmSum ;

//unsigned int wPPM ;
//unsigned int local ;
//
//unsigned int wPPMBuffer;
//
//unsigned int w1AlarmPastSum;
//unsigned int w1AlarmInterimSum;
//unsigned int wAlarmSum;


//unsigned int w1SlopeAverage;

void CO_Init(void);
void Opamp_Calibration(void);
void CO_ERR_CHECK(void);
void Senser_Short_Process(void);
void Calibration_Task (void);
void PCB_Calibration(u16 u16_co_count);
void Cal_0PPM(u16 u16_co_count);
void Cal_150PPM(u16 u16_co_count);
void Cal_300PPM(u16 u16_co_count);
void FCT_Test(void);
void Calculate_PPM(u16 raw_counts);
u16 Age_Correct_PPM(u16 PPM);
void COALM_Init(void);
void CO_Alarm_Check(void);
void MEAS_Send_Charge_Pulse(void);
void Recovery_Process(void);


#endif

