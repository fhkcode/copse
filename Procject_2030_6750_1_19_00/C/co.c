/*========================================================
*@File name:co.c
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#include "co.h"
#include "sys_function.h"
#include "uart.h"
#include "adc.h"
#include "sys_init.h"
#include "eeprom.h"

// Sensor output must be above this to begin sensor testing.
#define	SENSOR_RECOV_DELTA_LOW		10
#define	SENSOR_RECOV_DELTA_HIGH		20
#define	RECOVERY_COUNTS				10
#define	RECOVERY_CORRECTION_TIME	7200	// 2 hours * 60 * 60

#define			SENSOR_RAILED_LIMIT			2000

volatile _byte_type 	R_CO_RUNFLAG;
volatile _byte_type		R_CO_RUNFLAG2;
//volatile _byte_type 	R_CO_LIB_AUX;


u8 PCB_OFFSET = 0;
u8 OFFSET = 0;
u16 OPPM_OFFSET = 0;
u16 Scale = 0;
u16 wPPM = 0;
u16 wPrev_wPPM = 0;
u8 Sys_Cal_State;
u8 OPPM_Max = 0;
u8 CO_Compensation = 0;
u16 Recovery_Corr_Fac = 0;
u16 Recovery_Time_Com = 0;

unsigned int local = 0;
unsigned int w1AlarmPastSum = 0;	
unsigned int w1AlarmInterimSum = 0;	
unsigned int wAlarmSum = 0;
unsigned int wPPMBuffer = 0;
unsigned int w1SlopeAverage = 0;

static u16 wRecovery_Corr_Time = 0;
u8 Recovery_State = 0;		//static 

volatile _byte_type R_CO_RUNFLAG1;
//volatile _byte_type	R_CO_RUNFLAG3;


void Opamp_Calibration(void)
{
	u8 TMP0,TMP1;
	TMP0 = 0x00;
	TMP1 = 0x00;
	//_open = 1;
	
	_opvos = 0b11000000;
	GCC_DELAY(10000);
	while(!_opo)
	{
		_opvos ++;	
		GCC_DELAY(10000);
	}
	TMP0 = _opvos;
	TMP0 &= 0x3f;
	
	_opvos = 0b11111111;
	GCC_DELAY(10000);
	while(_opo)
	{
		_opvos--;	
		GCC_DELAY(10000);
	}
	TMP1 = _opvos;
	TMP1 &= 0x3f;
	
	_opvos =(TMP0 + TMP1)/2;
	
	_opvos = _opvos + 3;	//3
	
//	if(_opvos >=36)
//	{
//		_opvos = _opvos - 1;
//	}
	
	OFFSET = _opvos;
	
	
	//======================================================
	//short co sensor , opa output is not over 4000, 12bit adc max is 4096
	//
	#if 0
	if(Sys_Cal_State != 0)
	{
		u8 OPA_Read_Data = S_RD_EE(OPA_OFFSET);
		if(OPA_Read_Data != 0x00 && OPA_Read_Data != 0xFF)
		{
			if(OFFSET < OPA_Read_Data)
			{
				if((OPA_Read_Data - OFFSET) <= 5)
				{
					OFFSET = OPA_Read_Data;
				}
			}
		}	
	}
	#endif
//	OPA not need save to eeprom
	S_WR_EE(OPA_OFFSET,OFFSET);
	
/*	_opc = 0b01000010;			// opa on  600k  校准时必须时5KHz，需要恢复高采样频率？高频率采集增加功耗*/

#if 0
	S_UART_SEND_BYTE('\n');
	S_UART_SEND_Str("opvos:");
	S_UART_SEND_ASCII_Dec((OFFSET&0xff00)>>8,OFFSET&0x00ff);
	S_UART_SEND_BYTE('\r');
#endif
}

/**************************************
*@neam:CO_Init
*@brief: 
*@param[in]:
*@retval: 
*@notes:
**************************************/
void CO_Init(void)
{
	 //set OPA param
	 _oppw = 0b00000011;
	 _opsw0 = 0b00001010;
	 _opsw1 = 0b00000001;
	 //OPA enable， 5KHz
	 _opc = 0b01000000;
	 //_opc = 0b01000001;
	 //R:1915K-119*5K-1*640K=680K
	 _oppgac1 = 1;
	 _oppgac0 = 119;
	 

	//OPA output allaways 0
	 _ops2 = 1;
	 //delay
	 S_Delay_ms(5);	
	 //OPA Calibration
	 Opamp_Calibration();
	 _ops2 = 0;
	 
//	_ops2 = 1;
//	u16 wCount = 0;
//	wCount = Read_ADC_OPAOUT(ADC_12BIT);
//	#if 1
//	S_UART_SEND_BYTE('\n');
//	S_UART_SEND_Str("wCount:");
//	S_UART_SEND_ASCII_Dec((wCount&0xff00)>>8,wCount&0x00ff);
//	S_UART_SEND_BYTE('\r');
//	#endif
//	_ops2 = 0;
}

void CO_ERR_CHECK(void)
{
	static u8 co_err_check_count = 0;
	//static u8 pre_co_short_count = 0;
	
	co_err_check_count ++;
	if(co_err_check_count >= 10)
	//if((co_err_check_count >= 10) && (FLAG_PTT_CONDITION == 0) && (Sys_Cal_State == 5) && (FLAG_CO_ERR_ENABLE == 0))//
	//if(((co_err_check_count >= 10)  || (FLAG_PTT_CONDITION == 1)) && (Sys_Cal_State == 5)) //60 && (F_POWERON_MODE==0)&& (F_BD_M_INI==0) 
	//if(F_CO_ERRCK_EN)
	{
		co_err_check_count = 0;
		
		if((FLAG_PTT_CONDITION == 0) && (Sys_Cal_State == 5) && (FLAG_CO_ERR_ENABLE == 0))
		{
			unsigned int CO_ADC_Value = 0;
			CO_ADC_Value = Read_ADC_OPAOUT(ADC_8BIT);
			
			if(CO_ADC_Value < 10)	//10
			{	
				F_CO_FAULT_FLAG = 1;
				
				unsigned int Temp_a = 0,Temp_b = 0,Temp_c = 0; 
				u8 OPVOS_Recover = 0;
				
				OPVOS_Recover = _opvos;
				Temp_a = Read_ADC_OPAOUT(ADC_8BIT);
				_opvos = _opvos + 5;	//5
				_opofm = 1;
				//_oprsp = 0;
				//5ms
				S_Delay_ms(5);
				Temp_b = Read_ADC_OPAOUT(ADC_8BIT);
				_opofm = 0;
				//_oprsp = 1;
				_opvos = OPVOS_Recover;
				//5ms
				S_Delay_ms(5);
				Temp_c = Read_ADC_OPAOUT(ADC_8BIT);
						
				//if(((Temp_b - Temp_c) > 15)&&((Temp_c - Temp_a) < 5 ))
				if(((Temp_b - Temp_c + 300) > 315))
				{
					//if((Temp_c - Temp_a) < 5)
					if((Temp_c - Temp_a + 300) < 305)		//300 偏移量  无符号数的比较，加上偏移量后都为正数
					{
						F_CO_ERR = 1;
						//发生故障的次数咋记录？发生一次就记录一次？eerpom读写次数有限
						/*Temp_a = Temp_b = Temp_c = 0;*/
						#if 0
							S_UART_SEND_BYTE('\n');
							S_UART_SEND_Str("open Fault");
							S_UART_SEND_BYTE('\r');
						#endif
					}
				}
				else
				{
					if(F_CO_ERR == 1)
					{
						F_CO_ERR = 0;
						FLAG_SHORT_MEMORY = 0;	
					}	
					#if 0
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("open Fault clear");
					S_UART_SEND_BYTE('\r');
					#endif
				}
			}
			else if(CO_ADC_Value >175)		//200  180  243
			//else if(CO_ADC_Value> CO_SHORT_VALUE)
			{
				unsigned int Temp_a = 0,Temp_b = 0; 
				u8 OPVOS_Recover = 0;
				//u16  Short_wCount = 0;
				
				F_CO_FAULT_FLAG = 1;
				
							
				OPVOS_Recover = _opvos;
				Temp_a = Read_ADC_OPAOUT(ADC_8BIT);
				_opvos = _opvos - 5;
				_opofm = 1;
				S_Delay_ms(100);	// 100ms
				_opofm = 0;
				_opvos = OPVOS_Recover;
				S_Delay_ms(350);
				Temp_b = Read_ADC_OPAOUT(ADC_8BIT);
				
				
				//========================================
				//
				#if 1
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("Temp_a:");
					S_UART_SEND_ASCII_Dec((Temp_a&0xff00)>>8,Temp_a&0x00ff);
					S_UART_SEND_BYTE('\r');
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("Temp_b:");
					S_UART_SEND_ASCII_Dec((Temp_b&0xff00)>>8,Temp_b&0x00ff);
					S_UART_SEND_BYTE('\r');	
				#endif
				
				//if((Temp_a >= 218 ) )//|| (F_BAT_ISLOW == 1)240
				{
					if(((Temp_a - Temp_b + 300) >= 360) && (Temp_b < 243))		//367 370 400  300 偏移量
					{
						if(F_CO_ERR == 0)
						{
							F_CO_ERR = 1;
							F_ERR_M_INI = 0;
						}
					}
					else
					{
						if(F_CO_ERR == 1)
						{
							F_CO_ERR = 0;
							COALM_Init();
							FLAG_SHORT_MEMORY = 1;	
						}	
					}
				}
//				else
//				{
//					if(((Temp_a - Temp_b + 300)> 425) && (Temp_b < 243))		//400  300 偏移量
//					{
//						if(F_CO_ERR == 0)
//						{
//							F_CO_ERR = 1;
//							F_ERR_M_INI = 0;
//							FLAG_SHORT_MEMORY = 0;
//						}
//					}
//					else
//					{
//						if(F_CO_ERR == 1)
//						{ 
//							F_CO_ERR = 0;
//							COALM_Init();
//							FLAG_SHORT_MEMORY = 1;
//						}	
//					}		
//				}	
				
				//High voltage is detected for 3 consecutive times
				//if Temp_b > 240 is HPPM , so disable co sensor check
//				pre_co_short_count++;
//				if(pre_co_short_count >= 3)
//				{
//					pre_co_short_count = 0;
//					if(Temp_b >243)
//					{
//						FLAG_CO_ERR_ENABLE = 1;
//					}
//				}
	
	//			Temp_a = 0;
	//			Temp_b = 0;	
			}
			else
			{
				if(F_CO_ERR == 1)
				{
					F_CO_ERR = 0;
					//F_CO_SHORT_FLAG = 0;
					F_CO_FAULT_FLAG = 0;
//					pre_co_short_count = 0;
					//F_BDE_M_INI = 0;
					COALM_Init();
					FLAG_SHORT_MEMORY = 1;
				}
				
				#if 0
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("Fault clear");
				//_itoa(F_CO_ERR,10);
				S_UART_SEND_ASCII_Dec((F_CO_ERR&0xff00)>>8,F_CO_ERR&0x00ff);
				S_UART_SEND_BYTE('\r');
				#endif
			}
			
			//serial output co fault
			if(F_CO_ERR) 
			{
				#if Mode_Serial_Output
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("CO Fault");
				S_UART_SEND_BYTE('\r');	
				#endif
			}
		}
	}		
}

/**********************************************************
*Function: short senser after process
*Description: if short senser ,then remove short, the 
* OPA voltage is very high, so process
*Return: 	None
***********************************************************/
void Senser_Short_Process(void)
{
	//====================================================
	//during co alarm memory,then short co sensor,after 60s
	//clear co alarm

	if(FLAG_SHORT_MEMORY == 1)
	{
		static u8 memory_short_count = 0;
		//F_CO_ALARM = 0;
		COALM_Init();
		memory_short_count ++;
		if(memory_short_count>=60)
		{
			memory_short_count = 0;	
			FLAG_SHORT_MEMORY = 0;
		}
	}
}


/**********************************************************
*Function:Calibration_Task
*Description: Calibration_Task	
*Return: 	None
***********************************************************/
void Calibration_Task (void)
{
	u8 Sys_State = 0;
	
	Sys_State =	Sys_Cal_State;
	
	switch (Sys_State)
	{
		//PCB get
		case 0:		
			PCB_Calibration(average_counts);
			break;
		//CAL_STATE_ZERO
		case 1:		
		//no calibration OPPM or 0PPM Calibration fail
			Cal_0PPM(average_counts);		
			break;
		//300PPM verfiy
		case 2:			//CAL_STATE_150
			Cal_150PPM(average_counts);
			break;
		case 3:
			//if(F_POWERON_MODE==0)
			{
				//Calculate_PPM(average_counts);
				CO_Alarm_Check();
				//S_CO_EN_ALARM_CHECK_FUN(300,200,75,40);		
				Cal_300PPM(average_counts);
				F_CO_ALARM = 0;
				//S_UART_SEND_Str("clear alarm memory");
			}
			//CAL_STATE_VERIFY
			break;
		//no case 3 4  within hotek mcu have proble？
		//FCT test
		case 4:
			FCT_Test();
			break;
		
		case 5:		//CAL_STATE_FINAL_COMPLETE
			if((F_CO_ERR == 0)&& (FLAG_SHORT_MEMORY == 0))
			{
				CO_Alarm_Check();
			}
		break;
		
		default:
		break;
	}
	
}

/**********************************************************
*Function:Calibration_Task
*Description: Calibration_Task	
*param[in]:
*Return: 	None
*notes:
***********************************************************/
void PCB_Calibration(u16 u16_co_count)
{
	//1S cycle to run
	if((!FLAG_PCB_CAL_FLAG) && (F_POWERON_MODE==0))		//wait opa stable,wait time 50s
	{
		static u8 Cal_PCB_Count = 0;
//		static u16 Cal_PCB_Sum = 0;
//		static u16 Cal_PCB_Avg = 0;
		
		
		Cal_PCB_Count ++;
/*		F_CO_BD_EN = 1;*/
		
		#if 0
		S_UART_SEND_BYTE('\n');
		S_UART_SEND_Str("Cal_PCB_Count:");
		//_itoa(u16_co_count,10);
		S_UART_SEND_ASCII_Dec((Cal_PCB_Count&0xff00)>>8,Cal_PCB_Count&0x00ff);
		S_UART_SEND_BYTE('\r');	
		#endif
		
		
		if(Cal_PCB_Count >= 10)	
		{
			//Cal_PCB_Sum = Cal_PCB_Sum + u16_co_count;
		#if F_10K_DISABLE	
			if((u16_co_count > 15) || (u16_co_count < 2))		// sample data out of range
		 	#else
			if((u16_co_count > 180) || (u16_co_count < 90))
			#endif
			{
				FLAG_PCB_CAL_FAULT = 1; 		//PCB Calibration fail
				Sys_Cal_State = 0;		//CAL_STATE_PCB
				S_WR_EE(0x00,0);
				#if 1
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("PCB fial OFFSET:");
				//_itoa(u16_co_count,10);
				S_UART_SEND_ASCII_Dec((u16_co_count&0xff00)>>8,u16_co_count&0x00ff);
				S_UART_SEND_BYTE('\r');	
				#endif
			}
		}
		F_BD_M_INI = 0;						//show led flag
		
		if(Cal_PCB_Count >= 34)		// 34  29  10s
		{
			//Cal_PCB_Avg = Cal_PCB_Sum/6;
			Cal_PCB_Count = 0;
			FLAG_PCB_CAL_FLAG = 1;
			//S_WR_EE(PCB_OFFSET_Add,Cal_PCB_Avg);
			if((u16_co_count > 15) || (u16_co_count < 2))		// sample data out of range
			{
				#if 1
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("PCB fial OFFSET:");
				//_itoa(u16_co_count,10);
				S_UART_SEND_ASCII_Dec((u16_co_count&0xff00)>>8,u16_co_count&0x00ff);
				S_UART_SEND_BYTE('\r');	
				#endif
			}
			else
			{
				S_WR_EE(0x00,1);
				S_WR_EE(PCB_OFFSET_Add,u16_co_count);
				#if 1
				if(F_DEBUG_OUTPUT == 1)
				{
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("PCB save value:");
					S_UART_SEND_ASCII_Dec((u16_co_count&0xff00)>>8,u16_co_count&0x00ff);
					S_UART_SEND_BYTE('\r');	
	//				S_UART_SEND_BYTE('\n');
	//				S_UART_SEND_Str("PCB sum value:");
	//				S_UART_SEND_ASCII_Dec((Cal_PCB_Sum&0xff00)>>8,Cal_PCB_Sum&0x00ff);
	//				S_UART_SEND_BYTE('\r');	
				}
				#endif
			}
			
			FLAG_PCB_CAL_FAULT = 0; 		//PCB calibration ok
			//S_WR_EE(0x00,1);
		}
	}
}

/**************************************
*@neam:Calibration 0PPM
*@brief: 
*@param[in]:
*@retval: 
*@notes:  
*
**************************************/
void Cal_0PPM(u16 u16_co_count)
{
	//1S cycle to run
	//0ppm need sample and cal average
	if((!FLAG_CAL_OPPM_FLAG) && (F_POWERON_MODE==0))			//not Calibration 0PPM  F_POWERON_MODE==0 预热完成
	{
		static u16 Cal_OPPM_Count = 0;	
		static u32 Cal_0PPM_Sum = 0;
		static u16 Cal_0PPM_Avg = 0;
		
		F_CO_BD_EN = 1;	// calibration mode
		Cal_OPPM_Count ++;		//sample time
				
		//#if F_10K_DISABLE
		if(Cal_OPPM_Count >=301)
		{
			Cal_0PPM_Sum = Cal_0PPM_Sum + u16_co_count;
			if(Cal_OPPM_Count >=600)
			{
				Cal_0PPM_Avg = 	Cal_0PPM_Sum/300;
				OPPM_OFFSET = Cal_0PPM_Avg;
				#if 1
				if(F_DEBUG_OUTPUT == 1)
				{
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("0PPM Cal PCB Value:");
					S_UART_SEND_ASCII_Dec((Cal_0PPM_Avg&0xff00)>>8,Cal_0PPM_Avg&0x00ff);
					S_UART_SEND_BYTE('\r');	
				}
				#endif
				
				FLAG_CAL_OPPM_FLAG = 1;	//have Calibration
				//if((Cal_0PPM_Avg <= (PCB_OFFSET + 5)) && ((Cal_0PPM_Avg + 100 )>= (PCB_OFFSET - 3 + 100)))
				if((Cal_0PPM_Avg <= (PCB_OFFSET + 3)) && ((Cal_0PPM_Avg + 100 )>= (PCB_OFFSET - 5 + 100)))
				//if((Cal_0PPM_Avg >= (PCB_OFFSET + 5)) || (Cal_0PPM_Avg <= (PCB_OFFSET- 5)))
				{
					Cal_OPPM_Count = 0;
					FLAG_CAL_0PPM_FAULT = 0;		//Calibration ok
					F_CO_BD_F1_OK = 0;
					F_BD_M_INI = 0;						//show led flag
					OPPM_OFFSET  = Cal_0PPM_Avg;
					S_WR_EE(0x00,2);
					S_WR_EE(ZERO_PPM_OFFSET,OPPM_OFFSET);	
					S_WR_EE(CLEAR_AIR_MAX_Add,OPPM_Max);
						
				}
				else
				{
					FLAG_CAL_0PPM_FAULT = 1;			//Calibration fail
					F_BD_M_INI = 0;						//show led flag
					Sys_Cal_State = 1;		//CAL_STATE_ZERO
				}
		
			}
		}
	}

}

/**************************************
*@neam:Calibration 150PPM
*@brief: 
*@param[in]:
*@retval: 
*@notes:  
*
**************************************/
void Cal_150PPM(u16 u16_co_count)
{
	static u8 Cal_15OPPM_Count = 0;
	static u16 Cal_150PPM_Dealy = 0;
	static u32 Cal_150PPM_Sum = 0;
	static u16  delta = 0;
	u16 temp_ppm = 0;
	
	if((!FLAG_CAL_150PPM_FLAG) && (F_POWERON_MODE==0))
	{
		Cal_150PPM_Dealy ++;
		
		if(Cal_150PPM_Dealy == 300)  	//wait 6 minutes
		{
			if((u16_co_count > 550) || (u16_co_count < 150))		//50 ? 250?rang must be provided by an electrical engineer
			{
				FLAG_CAL_150PPM_FAULT = 1;	//150PPM校准失败
				F_BD_M_INI = 0;						//显示灯标志
				FLAG_CAL_150PPM_FLAG = 1;		//有校准
			}
		}//else 
		if((Cal_150PPM_Dealy > 300) && (Cal_150PPM_Dealy <= 428))  	//420  6 minutes after
		{
			Cal_150PPM_Sum = Cal_150PPM_Sum + u16_co_count;
			Cal_15OPPM_Count ++;
			if(Cal_15OPPM_Count >= 128)	//120
			{
				u16 Cal_150PPM_AVR = 0;
				
				//Cal_150PPM_AVR = Cal_150PPM_Sum/120;
				Cal_150PPM_AVR = Cal_150PPM_Sum >>7;
				if((Cal_150PPM_AVR > 550) || (Cal_150PPM_AVR < 150))
				{
					FLAG_CAL_150PPM_FAULT = 1;	//150PPM校准失败
					F_BD_M_INI = 0;						//显示灯标志
					Sys_Cal_State = 2;	//CAL_STATE_150
					FLAG_CAL_150PPM_FLAG = 1;		//有校准

				}
				else
				{
					
					delta =  (Cal_150PPM_AVR - OPPM_OFFSET)*100/150;		//斜率放大100倍
					//delta =  ((Cal_150PPM_AVR - OPPM_OFFSET) << 8)/150;
					Scale =delta;	
				}
				Cal_150PPM_AVR  =0;
				Cal_150PPM_Sum = 0;
				Cal_15OPPM_Count = 0;
	
			}	
		}//else 
		if(Cal_150PPM_Dealy > 450)
		{
/*			u16 temp_scale = 0,temp_ppm = 0;*/
/*			temp_scale = (u16)(S_RD_EE(Cal_Scale_MSB) << 8) | S_RD_EE(Cal_Scale_LSB);*/
/*			temp_ppm = (u16)(((u32)(u16_co_count - OPPM_OFFSET) * 100) / temp_scale) ;*/
			
			temp_ppm = (u16)(((u32)(u16_co_count - OPPM_OFFSET) * 100) / delta) ;
			//temp_ppm = (u16)(((u32)(u16_co_count - OPPM_OFFSET) << 8) / delta) ;
			//if((145 < temp_ppm < 165) || (temp_scale !=0))
			if((temp_ppm >= 145) && (temp_ppm <= 165))
			{
				//校准成功，下次上电状态为300PPM验证
				FLAG_CAL_150PPM_FAULT = 0;	//150PPM校准成功
				F_BD_M_INI = 0;						//显示灯标志
				//S_WR_EE(Calibration_Address,CAL_STATE_VERIFY);
				S_WR_EE(0x00,3);
				S_WR_EE(Cal_Scale_LSB,delta&0x00ff);
				S_WR_EE(Cal_Scale_MSB,(delta&0xff00)>>8);

			}
			else
			{
				FLAG_CAL_150PPM_FAULT = 1;	//150PPM Calibration fail
				F_BD_M_INI = 0;						//show led flag
				//Sys_Cal_State = CAL_STATE_150;
				Sys_Cal_State = 2;	//CAL_STATE_150
			}

			Cal_150PPM_Dealy = 0;
			FLAG_CAL_150PPM_FLAG = 1;		//have Calibrationed
		}
	}
}

/**************************************
*@neam:Calibration 300PPM
*@brief: 
*@param[in]:
*@retval: 
*@notes:  
*
**************************************/
void FCT_Test(void)
{
	if(F_BUZZER_KEEP_ON && (Sys_Cal_State !=5))
	{
		_pa5 = 1;
		_papu5 = 1;
		_pac5 = 1;
		_pawu5 = 1;
		
		if(1==_pa5)
		{
			F_BUZZER_KEEP_ON = 0;
		}
		
		if(0==_pa5)
		{
			F_BUZZER_KEEP_ON = 1;
		}
	
		
		_pa5 = 0;
		_papu5 = 0;
		_pac5 = 0;
		_pawu5 = 0;
		
		if(F_BUZZER_KEEP_ON)
		{
			_pa5 = 0;
			_pa3 = 1;
			GCC_CLRWDT();
			_pa6 = 0;
			_pb3 = 0;
			_pb2 = 0;
		}
		else
		{
			_pa5 = 1;
			_pa3 = 0;
			GCC_CLRWDT();
			_pa6 = 1;
			_pb3 = 1;
			_pb2 = 1;
		}
	}	
}

/**************************************
*@neam:Calibration 300PPM
*@brief: 
*@param[in]:
*@retval: 
*@notes:  
*
**************************************/
//???  300 or 400  PPM
void Cal_300PPM(u16 u16_co_count)		
{
	static u16 Cal_300PPM_Dealy = 0;
//	static u8 Cal_30OPPM_Count = 0;
	
	if((!FLAG_CAL_300PPM_FLAG))		//no Calibration		&& (F_POWERON_MODE==0)
	{
		Cal_300PPM_Dealy ++;
		
		if(Cal_300PPM_Dealy <= 300)		//350
		{
			if(F_CO_ALARM == 1)
			{
				//Sys_Cal_State = 5;	//CAL_STATE_FINAL_COMPLETE
				//S_WR_EE(Calibration_Address,CAL_STATE_FINAL_COMPLETE);
				S_WR_EE(0x00,5);
				FLAG_CAL_300PPM_FAULT = 0;	//Calibration ok
				F_BD_M_INI = 0;						//show led flag
				F_CO_CAL_FAULT = 1; 
				
				Cal_300PPM_Dealy = 0;
				FLAG_CAL_300PPM_FLAG = 1;
				DATA_Backup();
			}
		}
		else
		{
			if(F_CO_ALARM == 0)
			{
				FLAG_CAL_300PPM_FAULT = 1;			//Calibration fail
				F_BD_M_INI = 0;						//show led flag
				FLAG_CAL_300PPM_FLAG = 1;
				
				Cal_300PPM_Dealy = 0;
				FLAG_CAL_300PPM_FLAG = 1;
			}
		}							
	}
}


/**************************************
*@neam:Calibration PPM
*@brief: 
*@param[in]:
*@retval: 
*@notes:  
*
**************************************/
void Calculate_PPM(u16 raw_counts)
{
	if((Sys_Cal_State == 5) || (Sys_Cal_State == 3) || (Sys_Cal_State == 2))
	{	
		u16 offset;
	
		//offset = OPPM_OFFSET*1.5;
		offset = OPPM_OFFSET << 1;
		//offset how to define
		if((raw_counts < offset) || (F_CO_ERR == 1))		//why ? 
		//if(raw_counts < OPPM_Max)
		{
			wPPM = 0;	
		}
		else
		{
			// subtract the offset and multipy by the scale.
			wPPM = (u16)(raw_counts - OPPM_OFFSET) + CO_Compensation;
			
			// Calculate ppm using scale (slope)
			//wPPM = (uint16_t)(((uint32_t)wPPM * Scale) >> 8);
			wPPM = (u16)(((u32)wPPM * 100) /Scale);/// Scale ;
			//wPPM = (u16)(((u32)raw_counts << 8) /Scale);
			
			//Correct the ppm value by the age correction
			wPPM = Age_Correct_PPM(wPPM);
		
//			if(Sys_Cal_State == 5)
//			{
//				wPPM = (wPrev_wPPM*2 + wPPM)/3;
//			
//				wPrev_wPPM  = wPPM;	
//			}
		}	
	}
}

/**************************************
*@neam: Age Correct PPM
*@brief: 
*@param[in]:
*@retval: 
*@notes:  
*
**************************************/
u16 Age_Correct_PPM(u16 PPM)
{
	u32 Life_qtrs = 0;
	u16 PPM_Corrected = 0;
//	u16  Days = 0;
//	
//	Days = DAY;
	
//#if 0
//	S_UART_SEND_BYTE('\n');
//	S_UART_SEND_Str("Current Days:");
//	S_UART_SEND_ASCII_Dec((Days&0xff00)>>8,Days&0x00ff);
//	S_UART_SEND_BYTE('\r');
//#endif

	//CO Sensor Life qtrs 
	Life_qtrs = ((u32)DAY*(u32)100 / (((u32)365 * 100) /4));
	//Life_qtrs = DAY/90;
	
	PPM_Corrected = (PPM + (((u32)PPM * (Life_qtrs *(u32)PERCENT_PER_QTR))/100000));
	
	//PPM_Corrected =  PPM + ((u32)PPM) / ((u32)14600);

	return PPM_Corrected;
}

/**************************************
*@neam: CO alarm init
*@brief: 
*@param[in]:
*@retval: 
*@notes:  
*
**************************************/
void COALM_Init(void)
{
	w1AlarmPastSum = 0 ;	
	w1AlarmInterimSum = 0 ;	
	wAlarmSum = 0 ;		
}


/**************************************
*@neam: CO_Alarm_Check
*@brief: 
*@param[in]:
*@retval: 
*@notes:  
*

30PPM  >120min
41  	83min
53     80.5min
100    25.5min
150    15.5min
300    0.5min
**************************************/
void CO_Alarm_Check(void)
{
	static unsigned char alarm_count = 0;
	
//	if(F_CO_ERR)
//	return;
//	
	if(FLAG_PTT_CONDITION)
	return;
	
	alarm_count ++;		//30s cycle time
//	wPPM ++;
//	if(wPPM >= 48) wPPM = 48;
	if((alarm_count >= 30))//  && (F_CO_ERR ==0) && (FLAG_PTT_CONDITION == 0)  && (F_POWERON_MODE == 0)// && FLAG_SENSOR_CIRCUIT_STABLE)  //等待预热完成
	{
		alarm_count = 0;
		//for test
		//wPPM = wPPM - 60;
		//wPPM = 300;
		//wPPM ++;
	    //wPPM =wPPM -20;
	    //if(wPPM ==0) wPPM = 0;
	    //if(wPPM>=48)wPPM = 48;
	    //wPPM = 300;
	
//	    if(wPPM < 70 || wPPM > 41)
//	    {
//	        wPPM = 53;
//	    }
		
		// Store the last accumulation value
		w1AlarmPastSum = w1AlarmInterimSum ;
		
		//
		//  Multiply previous accumulation value by fraction.  
		//
		w1AlarmInterimSum = wAlarmSum ;
		local = (unsigned int)( ((long)w1AlarmInterimSum * AL_ALARM_SUM_CONSTANT) >> 16) ;
		
		#if 1
			//if (FLAG_CALIBRATION_COMPLETE)
			if(Sys_Cal_State == 5)		//CALIBRATION_COMPLETE
			{
				if (wPPM < AL_CENELEC_PPM_AVG_MAX)
				{
					w1SlopeAverage = ((3 * w1SlopeAverage) + wPPM) / 4 ;
					wPPM = w1SlopeAverage ;
				}
			}
		
			wPPMBuffer = wPPM ;
			
			if ( (wPPM < AL_CENELEC_50_PPM_MAX) && (wPPM > AL_CENELEC_50_PPM_MIN) )
			{
				wPPMBuffer = AL_CENELEC_50_PPM ;
			}
		#endif
		
		if ( wPPM >= AL_ACCUMULATOR_MINIMUM_THRESHOLD)
		{
			wAlarmSum = local + 
			(unsigned int)( ((long)w1AlarmPastSum * AL_PREALARM_SUM_CONSTANT) >> 16) ;
		}
	
		#if 1
		// Note: wPPMBuffer was getting destroyed here (modified)
		if (AL_ACCUMULATOR_DECAY_THRESHOLD < wPPMBuffer)
		{
			wAlarmSum = (wAlarmSum + wPPMBuffer + AL_ACCUMULATOR_DECAY_CONSTANT) ;
		}
		else if (AL_ACCUMULATOR_MINIMUM_THRESHOLD > wPPMBuffer)
		{
			if (wAlarmSum > AL_ACCUMULATOR_FAST_DECAY)
			{
				wAlarmSum -= AL_ACCUMULATOR_FAST_DECAY ;		
				wAlarmSum += wPPMBuffer ;		
			}
		}
		else
		{
			wAlarmSum += wPPMBuffer ;
		}
		#endif
	
		#if 0
		// Note: wPPM was getting destroyed here (modified)
		if (AL_ACCUMULATOR_DECAY_THRESHOLD < wPPM)
		{
			wAlarmSum = (wAlarmSum + wPPM + AL_ACCUMULATOR_DECAY_CONSTANT) ;
		}
		else
		{
			wAlarmSum += wPPM ;
		}
		#endif
		//
	
		// Note: Subtract AL_ACCUMULATOR_COMP_THRESHOLD
		if (wAlarmSum > AL_ACCUMULATOR_COMP_THRESHOLD)	//
		{
			wAlarmSum -= AL_ACCUMULATOR_COMP_THRESHOLD ;
			
			// check for alarm condition
			if (wAlarmSum > AL_ALARM_THRESHOLD)
			{
				wAlarmSum = AL_ALARM_THRESHOLD_LIMIT ;
				if(F_CO_ALARM == 0)F_CO_ALARM = 1; 
			}
			else
			{	
				
				#if 1
				// The Cenelec configuration is the same except that wPPMBuffer is used
				// the accumulator is below the alarm threshold
				if (wPPMBuffer > AL_THRESHOLD_SLOPE_THRESHOLD)
				{
					if( (AL_ALARM_THRESHOLD - wAlarmSum) < (wPPMBuffer * AL_THRESHOLD_SLOPE_HIGH_MULTIPLE) )
					{
//						if (FLAG_CO_ALARM_CONDITION == 0)
//							Diag_Hist_PPM_Last_Alarm(wPPM) ;
							
						wAlarmSum = AL_ALARM_THRESHOLD_LIMIT ; 
						if(F_CO_ALARM == 0)F_CO_ALARM = 1;
//						FLAG_PEAK_MEMORY = 1 ;  // peak memory is set if unit goes into alarm
					}
					else
					{
/*						COALM_Clear() ;	*/
						if(F_CO_ALARM == 1)
						{
							F_CO_ALARM = 0;	
							F_CO_ALARM_MEMEORY = 1;
							F_ALARM_MEMORY_M_INI = 0;
						}
					}
				}
				else
				{
					if( (AL_ALARM_THRESHOLD - wAlarmSum) < (wPPMBuffer * AL_THRESHOLD_SLOPE_MULTIPLE) )
					{
//						if (FLAG_CO_ALARM_CONDITION == 0)
//							Diag_Hist_PPM_Last_Alarm(wPPM) ;
							
						wAlarmSum = AL_ALARM_THRESHOLD_LIMIT ;
						if(F_CO_ALARM == 0)F_CO_ALARM = 1;
//						FLAG_PEAK_MEMORY = 1 ;  // peak memory is set if unit goes into alarm
					}
					else
					{
/*						COALM_Clear() ;	*/
						if(F_CO_ALARM == 1)
						{
							F_CO_ALARM = 0;	
							F_CO_ALARM_MEMEORY = 1;
							F_ALARM_MEMORY_M_INI= 0;
						}	
					}
				}
				#endif
				
				#if 0
				//USA alarm config
				//#ifdef  BSI_CONFIG
				if (wPPM > AL_THRESHOLD_SLOPE_THRESHOLD)
					{
						if( (AL_ALARM_THRESHOLD - wAlarmSum) < (wPPM * AL_THRESHOLD_SLOPE_HIGH_MULTIPLE) )
						{
							//if (FLAG_CO_ALARM_CONDITION == 0)
							//	Diag_Hist_PPM_Last_Alarm(wPPM) ;
						
							wAlarmSum = AL_ALARM_THRESHOLD_LIMIT ; 
							F_CO_ALARM = 1;
							//FLAG_CO_ALARM_CONDITION = 1 ;
							//FLAG_PEAK_MEMORY = 1 ;  // peak memory is set if unit goes into alarm
						}
						else
						{
							//COALM_Clear() ;	
							F_CO_ALARM = 0;
						}
					}
					else
					{
						if( (AL_ALARM_THRESHOLD - wAlarmSum) < (wPPM * AL_THRESHOLD_SLOPE_MULTIPLE) )
						{
							//if (FLAG_CO_ALARM_CONDITION == 0)
								//Diag_Hist_PPM_Last_Alarm(wPPM) ;
	
							wAlarmSum = AL_ALARM_THRESHOLD_LIMIT ; 
							F_CO_ALARM = 1;
							//FLAG_CO_ALARM_CONDITION = 1 ;
							//FLAG_PEAK_MEMORY = 1 ;  // peak memory is set if unit goes into alarm
						}
						else
						{
							//COALM_Clear() ;
							F_CO_ALARM = 0;	
						}
					}
				
				#endif
				//#endif
				
				//Europe alarm config
	//			#ifdef	UL_CONFIG
	//			
	//			#endif
			}
		}
		else
		{
			wAlarmSum = 0 ;
			w1AlarmInterimSum = 0 ;
			//COALM_Clear() ;
			if(F_CO_ALARM == 1)
			{
				F_CO_ALARM = 0;	
				F_CO_ALARM_MEMEORY = 1;
				F_ALARM_MEMORY_M_INI = 0;
			}
		}
	}	
}


/**************************************
*@neam: CO sensor  Reverse charging
*@brief: 
*@param[in]:
*@retval: 
*@notes:  
*
**************************************/
void MEAS_Send_Charge_Pulse(void)
{ 
	//need to test 
	//connect load can fast discharing？
	//use mcu R dicharing？
	//Reverse charging
//	_opsw0 = 0b00001111;		// ops0、1、2、3 on
//	_opsw1 = 0b00000000;		// ops8 off
//	_ops6 = 1;
		
//=========================================
//charge
#if 0
	u8 OPVOS_Recover = 0;
	OPVOS_Recover = _opvos;
	_opvos = _opvos + 5;	//5
	_opofm = 1;
	S_Delay_ms(5);
	_opofm = 0;
	_opvos = OPVOS_Recover;	
	S_Delay_ms(5);
	//delay 60us
	//GCC_DELAY(120);	
#endif

//==========================================
//discharge
#if 0
	u8 OPVOS_Recover = 0;
	OPVOS_Recover = _opvos;
	_opvos = _opvos - 5;
	_opofm = 1;
	//GCC_CLRWDT();
	S_Delay_ms(100);	// 100ms
	_opofm = 0;
	_opvos = OPVOS_Recover;
	S_Delay_ms(500);
#endif

//==========================================
//short
#if 0

	_ops2 = 1;
	//delay 60us
	GCC_DELAY(120);	
	_ops2 = 0;
#endif

//==========================================
//current
#if 1
	 _opsw0 = 0b00000000;
	 _opsw1 = 0b00000000;
	 
	 
	 _ops0 = 1;
	 _ops1 = 1;
	 _ops4 = 1;
	 _ops5 = 1;
	//delay 60us
	//GCC_DELAY(120);	
	S_Delay_ms(6);	// 60100ms
	
	 _opsw0 = 0b00001010;
	 _opsw1 = 0b00000001;
#endif
	
	//recover default set
//	_opsw0 = 0b00001010;		// ops1、3 on
//	_opsw1 = 0b00000001;		// ops8 on
//	_ops6 = 0;
	

}


/**************************************
*@neam: CO sensor  high PPM recover
*@brief: 
*@param[in]:
*@retval: 
*@notes:  
*
**************************************/

#define MEAS_RECOVERY_IDLE 0
#define MEAS_RECOVERY_VHIGH 1
#define MEAS_RECOVERY_CLEAR	2
#define	MEAS_RECOVERY_WAIT	3
#define	MEAS_RECOVERY_TEST	4
void Recovery_Process(void)
{
//	static u8 Charge_count = 0;
	static u8 State_count = 0;
	static u8 Charge_Exit_count = 0;
//	static u16 Stop_count = 0;
	static u16 Clear_count = 0;
	static u16 Charge_stop_count = 0;
	//static u16 Current_count = 0;
	static u16 Compensation_count = 0;
	
	static u16 Recovery_wait_count = 0;
	static u8 Wait_exit_count = 0;

	
	if (FLAG_RECOVERY_CORR_ACTIVE)
	{
		wRecovery_Corr_Time ++;
		if (wRecovery_Corr_Time > RECOVERY_CORRECTION_TIME)
		{
			Clear_count = 0;
			State_count = 0;
			Recovery_State = 0 ;
			FLAG_CO_ERR_ENABLE = 0; 
			FLAG_RECOVERY_CORR_ACTIVE = 0;	
			
			S_UART_SEND_BYTE('\n');
			S_UART_SEND_Str("HPPM EX timeout:");
			S_UART_SEND_BYTE('\r');	
		}
	}
	
	switch (Recovery_State)
	{
		case MEAS_RECOVERY_IDLE:
			if ((average_counts > SENSOR_RAILED_LIMIT) && (!F_CO_ERR))
			{
				State_count ++;
				if(State_count >=50)
				{
					State_count = 0;
					Recovery_State++ ;
					FLAG_CO_ERR_ENABLE = 1;
				}
			}		
			else
			{
				State_count = 0;
				FLAG_RECOVERY_CORR_ACTIVE = 0 ;	
			}
		break ;
		
		case MEAS_RECOVERY_VHIGH:
			//if(average_counts < 900)
			if((F_CO_ALARM_MEMEORY == 1) && (wPPM == 0))
			{
				State_count ++;
				if(State_count >=30)
				{
					State_count = 0;
					Recovery_State++ ;	
				}		
			}

			S_UART_SEND_BYTE('\n');
			S_UART_SEND_Str("HPPM:");
/*			S_UART_SEND_ASCII_Dec((count_opaap&0xff00)>>8,count_opaap&0x00ff);*/
			S_UART_SEND_BYTE('\r');
		break ;
		
		case  MEAS_RECOVERY_CLEAR:
		
			Clear_count ++;
			if((Clear_count%30) == 0)
			{
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("AIR:");
				S_UART_SEND_BYTE('\r');
				//FLAG_CO_ERR_ENABLE = 1;
			}
			
			Charge_stop_count++;
			if(Charge_stop_count <= 60)//2(Charge_stop_count%1) == 0) && 
			{			

				int i ;
				for (i=0 ; i<5 ; i++)
				{
					MEAS_Send_Charge_Pulse();
				}
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("C_charge:");
				S_UART_SEND_BYTE('\r');	
			}

			
			if(Charge_stop_count >=140)
			{
				static u8 Exit_count = 0;
				if ((average_counts >= 25) &&(average_counts <=120))
				{
					Exit_count ++;
					if(Exit_count >= 5)
					{
						Exit_count = 0;
						Charge_Exit_count ++;
						if(Charge_Exit_count>=10)
						{
							Recovery_State++ ;	
							S_UART_SEND_BYTE('\n');
							S_UART_SEND_Str("exit1 air:");
							S_UART_SEND_BYTE('\r');		
						}	
					}
				}
				else
				{
					Exit_count = 0;	
				}
			}
			if(Charge_stop_count >= 150)
			{
				Charge_stop_count = 0;
			}
			
			wPPM = 0;
			//FLAG_CO_ERR_ENABLE = 1;
			
			if(Clear_count >= 3000)		//50mins
			{
					Recovery_State++ ;	
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("exit air:");
					S_UART_SEND_BYTE('\r');	
			}
			
		break;
		
		case MEAS_RECOVERY_WAIT:
			
			if((average_counts >= 25))/*(OPPM_OFFSET + 5)*/
			{
				Wait_exit_count ++;
				if(Wait_exit_count >= 180)
				{
					Wait_exit_count	= 0;
					Recovery_State++ ;	
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("exit wait:");
					S_UART_SEND_BYTE('\r');	
				}
				
				wPPM = 0;
				//FLAG_CO_ERR_ENABLE = 1;
			}
			else
			{
				Wait_exit_count = 0;	
			}
			
			Recovery_wait_count ++;
			if(Recovery_wait_count >= RECOVERY_CORRECTION_TIME)
			{
				Recovery_State = 0 ;
				//FLAG_CO_ERR_ENABLE = 0; 
				FLAG_RECOVERY_CORR_ACTIVE = 0;	
			}
			
		break;
		
		case  MEAS_RECOVERY_TEST:
			
			if((Compensation_count%30) == 0)
			{
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("Recovery:");
				S_UART_SEND_BYTE('\r');
			//	FLAG_CO_ERR_ENABLE = 1;
			}

			if ((average_counts < (OPPM_OFFSET + 5)) || (average_counts >=150)) 
			{
				State_count ++;
				if(State_count >=40)
				{
					Clear_count = 0;
					State_count = 0;
					Recovery_State = 0 ;
					FLAG_CO_ERR_ENABLE = 0; 
					FLAG_RECOVERY_CORR_ACTIVE = 0;	
				}
			}
			else if((average_counts >= (OPPM_OFFSET + 5)) && (average_counts < 150))// <(u16)((u32)(100)*Scale/100)
			{
				
				FLAG_RECOVERY_CORR_ACTIVE = 1;
				Compensation_count ++;
				if(Compensation_count >=1200)
				{
					if(Recovery_Corr_Fac == 0)
					{
						FLAG_RECOVERY_CORR_ACTIVE = 1;
						COALM_Init();
						Recovery_Corr_Fac = (u16)((u32)(75 - wPPM)*Scale/100);
						
						if(Recovery_Corr_Fac != 0)
						{
							S_UART_SEND_BYTE('\n');
							S_UART_SEND_Str("Recovery_Corr_Fac2:");
							S_UART_SEND_ASCII_Dec((Recovery_Corr_Fac&0xff00)>>8,Recovery_Corr_Fac&0x00ff);
							S_UART_SEND_BYTE('\r');
						}	
					}	
				}
			}
		break;
	}
}