/*========================================================
*@File name:timer.h
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#ifndef	_TIMER_H__
#define _TIMER_H__

#include "BA45F6750.h"
#include "common.h"


//void S_TB1_INIT(void);
//void S_STM_INIT(void);
//void S_STM_INTERRUPT(void);
void S_TB1_INTERRUPT(void);
void S_Clock_CALC(void);
void S_Clock_Count(void);
void S_Clock_CALC_Enable(void);


extern	unsigned char R_Clock_count;
extern	unsigned char R_Clock_Differ;
extern	unsigned char R_Clock_valsum;
extern	unsigned char R_Clock_sec;
extern	unsigned char R_Clock_min;
extern	unsigned char R_Clock_hour;

extern	unsigned char R_Clock_HIRC_sec;
extern	unsigned char R_Clock_HIRC_min;
extern	unsigned char R_Clock_HIRC_hour;

#endif

