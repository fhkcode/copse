2022/2/7 Revision changes:
1. Update FW rev 1.20.0
2. alarm memory limit is 7 day, is error.now is change 14 day
3. HT-IDE3000 is update, the timer regeister is change. before is _psc0r ,now is _pscr


2022/1/21 Revision changes:
1. Update FW rev 1.19.0
2. FW version Revision, before is 1.0.18  after is 1.19.0

2021/12/24 Revision changes:
1. Update FW rev 1.1.0
2. TUV check low battery hush time is over 24hour, 25.35.37min.
so debug low battery hush time , EOL hush time, alarm memory timeout 

2021/11/16 Revision changes:
1. Update FW rev 1.0.18
2. fix PTT bug,during PTT the OPA output not enough

2021/10/29 Revision changes:
1. Update FW rev 1.0.17
2. change 0PPM calibration limit +3~-5

2021/10/14 Revision changes:
1. Update FW rev 1.0.16
2. add FCT test mode

2021/9/28 Revision changes:
1. Update FW rev 1.0.15
2. static check

2021/9/27 Revision changes:
1. Update FW rev 1.0.14
2. standy current is high , set PD,PE GPIO

2021/8/23 Revision changes:
1. Update FW rev 1.0.13
2. add resistor R28  to change FW
3. T4 on and or time ad


2021/8/18 Revision changes:
1. Update FW rev 1.0.12
2. EOL HUSH and Low battery hush timeout fix
3. low battery hush priority less then co alarm memory

2021/8/16 Revision changes:
1. Update FW rev 1.0.11
2. change limit +-5 to +5~-3 with circuit calibration limit.
3. Compenstation High PPM


2021/7/17 Revision changes:
1. Update FW rev 1.0.10
2. during co alarm memory,if long button to stuck button,then release button, work mode still stuck button.
3. if check  low battery is true,then updata once low battery flag,before 12 min if all is low battery,only 
update once flag.
4. add time quickly  uart cmd for good test.
5. add if CO sensor is Fault, don't check co alarm 

2021/7/5 Revision changes:
1. Update FW rev 1.0.9
2. change PCB sample data time, 15s to 35s
3. if co sensor short, clear co alarm flag
4. if during co alarm memory mode, low battery mode,co sensor fault,break co alarm memory mode, 
then other mode clear,still go to co alarm memory mode.
5. change stuck button 4s blink 1 time,to 5s blink 1 time


2021/7/5 Revision changes:
1. Update FW rev 1.0.8
2. after power on ,don't write day to eeprom.  after power on 12 min to check day and updata to eeprom

2021/6/31 Revision changes:
1. Update FW rev 1.0.7
2. write pcb data jump to 0PPM calibration

2021/6/29 Revision changes:
1. Update FW rev 1.0.6
2. power on then ptt ,reset opa is 0, add procese opa is 0

2021/6/24 Revision changes:
1. Update FW rev 1.0.5
2. add event timer

2021/6/16 Revision changes:
1. Update FW rev 1.0.4
2. remove 10k R, reduce  PCB calibration  time  from 60s to 15s. during the pcb
calibration pwoer on  shuld be short C8 and pressed button


2021/6/12 Revision changes:
1. Update FW rev 1.0.3
2. fix uart cmd  C1 bug
3. during low battery mode  chirp and check low battery sign is same time
 


2021/6/8 Revision changes:
1. Update FW rev 1.0.2
2. Add uart cmd,add offset compenstation,
3. if use lvd&lvr function , current is 20uA in standby mode .so change power on  enable lvr&lvd ,before standby mode disable lvr&lvd
4. remove co alarm conseve
5. EOL fatal blink 2 times/30s to change 2 times/60s, chirp 2 times/30s to change 2 times/60s
6. low battery fatal 1 time/30s to change 1 time/60s
7. co alarm memory blink  2 times/15s to chanege  1 time/60s
8. eol/low battery hush exit time to change 7 day
9. co alarm memory status for 14 days ,then exit
10.change T4 and alarm led blink time,
11.remove PTT chirp
12.add pcb calibration connect 10k R,during 0PPM calibartion check co sensor bad or ok



2021/6/1 Revision changes:
1. creat project to BA45F6750
2. source code regestier to BA45F6740, ROM bank size, TB1 TB0 time clock,uart interupt regesiter
3. change PBC GPIO input, to fix  current is 13uA, to fixed current is 7~9uA

2021/3/10 Revision changes:
 1. change 0 PPM calibration limit,5~12 change to -offse ~offset
 2. change 150 PPM calibation limit 

2021/3/5 Revision changes:
 1. add COS cal to PPM,alarm source code
 2. use UK pa

2021/3/2 Revision changes:
 1. add co opa sample,opa votalage is unstable, so average value with 1/8
 2. add calibration O/150/300 calibartion
 

2021/2/18 Revision changes:
 1. Fixed bug that _2_Hours_Averaged_CAV does not be clear after 2 hours c

2021/2/17 Revision changes:
 1. calibration clock 
 2. MCU is BA45F6740

 2021/2/2 Revision changes:
  1. creat project
  2. use BA45F6740
