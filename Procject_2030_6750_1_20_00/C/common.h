/*========================================================
*@File name:common.h
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#ifndef	_COMMON_H__
#define _COMMON_H__

typedef unsigned char	u8;
typedef unsigned int	u16;		// u16 应该为unsigned char 为什么要定义成unsigned int？
									//8位单片机可以使用32位单片机数据定义？
typedef unsigned long	u32;

typedef	struct	{
	unsigned char bit0:1;
	unsigned char bit1:1;
	unsigned char bit2:1;
	unsigned char bit3:1;
	unsigned char bit4:1;
	unsigned char bit5:1;
	unsigned char bit6:1;
	unsigned char bit7:1;
}_byte_bits;

typedef	union	{
	_byte_bits	bits;
		u8		byte;
}_byte_type;


//ADC相关变量外部声明
extern	u8 R_ADC_DATA[2];

//蜂鸣器相关变量外部声明
extern	u16	R_BUZZER_STATE;
extern	u16	R_BUZZON_CNTSET;
extern	u16	R_BUZZOFF_CNTSET;
extern	u16	R_BUZZWAIT_CNTSET;
extern	u16	R_BUZZ_SERIESSET;

extern 	u16	R_BUZZER_CNT;
extern	u16	R_BUZZER_SERIES;

extern volatile _byte_type	R_BUZZ_FG;

#define	F_BUZZER_EN			R_BUZZ_FG.bits.bit0
#define	F_BUZZ_SHIFT		R_BUZZ_FG.bits.bit1
#define	F_BUZZER_RESET		R_BUZZ_FG.bits.bit2
#define	F_BUZZER_ONEPERIOD	R_BUZZ_FG.bits.bit3
#define	F_BUZZER_STOP		R_BUZZ_FG.bits.bit4
#define	F_BUZZER_ONCE		R_BUZZ_FG.bits.bit5
#define F_BUZZER_KEEP_ON	R_BUZZ_FG.bits.bit6


//闪灯变量外部声明
extern	u8	R_RED_SET_ON;
extern	u8	R_RED_SET_OFF;
extern	volatile _byte_type	R_LED_FG;
#define	F_LED_FLASH_EN		R_LED_FG.bits.bit0
#define	F_LED_R				R_LED_FG.bits.bit1
#define	F_LED_G				R_LED_FG.bits.bit2
#define	F_LED_Y				R_LED_FG.bits.bit3
#define	F_LED_SW			R_LED_FG.bits.bit4

//LED相关变量外部声明
extern	u8	R_LED_STATE;
extern	u8	R_LEDON_CNTSET;
extern	u8	R_LEDOFF_CNTSET;
extern	u16	R_LEDWAIT_CNTSET;
extern	u8	R_LED_SERIESSET;

extern volatile _byte_type	R_LED_FG1;

#define	F_LED_EN			R_LED_FG1.bits.bit0
#define	F_LED_SHIFT			R_LED_FG1.bits.bit1
#define	F_LED_RESET			R_LED_FG1.bits.bit2
#define	F_LED_ONEPERIOD		R_LED_FG1.bits.bit3
#define	F_LED_STOP			R_LED_FG1.bits.bit4
#define	F_LED_ONCE			R_LED_FG1.bits.bit5

//系统变量外部声明
//extern	u8	R_SOFT_TIMER_CNT;
extern	u8	R_START_CNT;
//extern	u8	R_PWRDOWN_CNT;
extern	volatile _byte_type	R_SYS_FG;
#define	F_TIMER				R_SYS_FG.bits.bit0
#define	F_OneSec			R_SYS_FG.bits.bit1
#define	F_TEST				R_SYS_FG.bits.bit2
#define	F_POWERON_MODE		R_SYS_FG.bits.bit3
#define	F_ERR_ISSET			R_SYS_FG.bits.bit4
#define	F_ALARM_ISSET		R_SYS_FG.bits.bit5
#define	F_DEBUG_OUTPUT		R_SYS_FG.bits.bit6
#define	F_LIFE_REACH		R_SYS_FG.bits.bit7

extern	volatile _byte_type	R_SYS_WORK_FG;
#define	F_SYS_WMODE			R_SYS_WORK_FG.bits.bit7


extern	volatile _byte_type	R_SYS_FG1;
//#define	F_PWRDOWN_FLAG		R_SYS_FG1.bits.bit0
#define	F_PWRDOWN_CNTNX		R_SYS_FG1.bits.bit1
//#define	F_SYS_SLOW			R_SYS_FG1.bits.bit2
#define	F_CAL_OneHour		R_SYS_FG1.bits.bit2
#define	F_CAL_Onemin		R_SYS_FG1.bits.bit3
#define F_IICEEP_ACK		R_SYS_FG1.bits.bit4
#define	F_IICEEP_WNEXT		R_SYS_FG1.bits.bit5
#define	F_IICEEP_WERR		R_SYS_FG1.bits.bit6
#define	F_ONESEC_MODE		R_SYS_FG1.bits.bit7

extern	volatile _byte_type R_INI_FG;
#define	F_TEST_M_INI		R_INI_FG.bits.bit0
#define	F_POWERON_M_INI		R_INI_FG.bits.bit1
#define	F_ERR_M_INI			R_INI_FG.bits.bit2
#define	F_BD_M_INI			R_INI_FG.bits.bit3
#define	F_BDE_M_INI			R_INI_FG.bits.bit4	
#define	F_ALARM_M_INI		R_INI_FG.bits.bit5
#define	F_NORMAL_M_INI		R_INI_FG.bits.bit6
#define	F_EOL_M_INI			R_INI_FG.bits.bit7

extern	volatile _byte_type 	R_INI_FG1;
#define	 F_MEMORY_FAULT_M_INI	R_INI_FG1.bits.bit0
#define	 F_LOW_BAT_M_INI		R_INI_FG1.bits.bit1
#define	 F_ALARM_MEMORY_M_INI	R_INI_FG1.bits.bit2
#define	 F_PTT_FAULT_M_INI		R_INI_FG1.bits.bit3
#define	 F_LB_FATAL_M_INI		R_INI_FG1.bits.bit4
#define	 F_EOL_HUSH_M_INI		R_INI_FG1.bits.bit5
#define	 F_LB_HUSH_M_INI		R_INI_FG1.bits.bit6




//EEPROM变量外部声明
//extern	u8	R_EEPROM_DATA;

//debug串口数据变量外部声明
//extern	u8	R_RS_CHEAKSUM;

//extern	u8	R_UTX_BUFFER[16];
//extern	u8	R_SFUART_RXCNT;
//extern	u8	R_UART_RUNCNT;
//历史记录通信查询相关变量外部声明
extern	volatile _byte_type	R_HISTORY_FG;
#define	F_ORDER_NTIME		R_HISTORY_FG.bits.bit0
#define	F_SEARCH_NCNT		R_HISTORY_FG.bits.bit1
#define	F_SEARCH_RESET		R_HISTORY_FG.bits.bit2
#define	F_MODIFY_TIME		R_HISTORY_FG.bits.bit3
#define	F_CHECK_HISTORY		R_HISTORY_FG.bits.bit4
#define	F_ORDER_TXTIME		R_HISTORY_FG.bits.bit5
#define F_NORLFAIL			R_HISTORY_FG.bits.bit6
#define	F_ORDER_N2			R_HISTORY_FG.bits.bit7


//key 相关变量外部声明
extern	volatile u8	R_KEY_RAM;
extern	volatile u8	R_KEY_OLD;
extern	volatile u8	R_KEY_CHANGE;
extern	volatile u8	R_KEY_DEBOUNCE;

extern volatile _byte_type	R_KEY_FG;
#define F_DOUBLE_CONDITION	R_KEY_FG.bits.bit0
#define F_DOUBLE_FLAG		R_KEY_FG.bits.bit1


//低压标志位
extern	volatile bit F_BAT_ISLOW;
extern	u8 R_BAT_ADVAL;

//
extern	_byte_type	R_UART_FG;
#define	F_UART_RX_RUNING	R_UART_FG.bits.bit0
#define	F_UART_RX_COMTE		R_UART_FG.bits.bit1

extern	_byte_type R_UART_CMD_FG;
#define	F_UART_CMD_Sougle		R_UART_CMD_FG.bits.bit0
#define	F_UART_CMD_Output_ALL	R_UART_CMD_FG.bits.bit1
#define	F_UART_CMD_Output_CO	R_UART_CMD_FG.bits.bit2


extern	_byte_type R_Clock_CALC_FG;
#define	F_Clock_CALC_EN		R_Clock_CALC_FG.bits.bit0
#define	F_Clock_CALC_EQU	R_Clock_CALC_FG.bits.bit1
#define	F_Clock_CALC_MAX	R_Clock_CALC_FG.bits.bit2
#define	F_Clock_CALC_MIN	R_Clock_CALC_FG.bits.bit3
#define	F_Clock_C			R_Clock_CALC_FG.bits.bit4
#define	F_Clock_CALC_FG		R_Clock_CALC_FG.bits.bit5
#endif


