/*========================================================
*@File name:key.c
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#include "key.h"
#include "co.h"
#include "eeprom.h"
#include "sys_function.h"
#include "adc.h"
#include "sys_init.h"
#include "buzzer.h"
#include "led.h"

/**************************************
*@neam:S_KEY_INIT
*@brief:按键初始化 
*@param[in]:
*@retval: 
*@notes:
**************************************/
//void S_KEY_INIT(void)
//{
//	
//	R_KEY_RAM=0;
//	if(1 == P_TEST_KEY)
//		R_KEY_RAM|=0x01;
////	if(1==P_BD)
////		R_KEY_RAM|=0x02;
//
//}

/**************************************
*@neam:S_Key_Scan
*@brief:按键扫描 
*@param[in]:
*@retval: 
*@notes:
**************************************/
#if Old_Key
void S_Key_Scan(void)
{
	static u8 keypress = 0x00;
	static u8 stopcnt;
	
	u8 temp;
	R_KEY_RAM=0;
	if(1==P_TEST_KEY)
		R_KEY_RAM|=0x01;
//	if(1==P_BD)
//		R_KEY_RAM|=0x02;
	temp =R_KEY_RAM;
	temp^=R_KEY_OLD;	
	if(temp!=0)
	{
		R_KEY_CHANGE=temp;
		R_KEY_DEBOUNCE|=0X80;
		if((R_KEY_DEBOUNCE&0x04)==0x04)//消抖
		{
			R_KEY_DEBOUNCE=0;
			
			if((R_KEY_CHANGE&0x01)==0x01)	//测试按键处理
			{
				R_KEY_OLD^=0x01;
				if((R_KEY_OLD&0x01)==0x00)	//按下处理
				{
					if((0==F_CO_BD_EN)&&(0==F_CO_ALARM)&&(0==F_CO_ERR))
					{
						keypress = 0x01;
						F_TEST=1;
						stopcnt = 0;
					}
				}
				else						//松开处理
				{
					keypress = 0x00;
					//F_TEST=0;
				}
				
			}
		}
		
	}
	else
	{
		R_KEY_DEBOUNCE=0;
	}
	
	if (F_TEST)
	{
		if (!keypress)
		{
			if (F_BUZZER_ONEPERIOD)
			{
				//button cycle，TWO T-4
				stopcnt++;
				if (stopcnt < 2);
				else
				{
					F_TEST =0;
					stopcnt = 0;
					//==================
					//reset
					_wdtc = 0;
					while(1);	
				}
				F_BUZZER_ONEPERIOD = 0;
			}
			
		}
		
	}
}

#endif

#if New_Key
void Button_Scan(void)
{
	static u8 stopcnt = 0;
	static u16 PTT_Wait_Count = 0;
	static u8 PTT_Count ;
	//static u16 Double_Press_Count = 0;
	//static u16 Press_Count = 0;,Release_Count = 0;
	
	if(0==P_TEST_KEY)		//press
	{	
		if((FLAG_PTT_CONDITION == 0) && (FLAG_PTT_WAIT == 0) &&  (F_TEST == 0) && (F_MEMORY_FAULT == 0) 
		&& (Sys_Cal_State == 5))
		{
			Key_Count++;		//if go oning PTT ,disable Button  if(!F_TEST)		
		}

		//long press
		if(Key_Count>=1000)	//8s
		{
			F_STUCK_FALUT = 1;
			if(Key_Count ==1000)
			{
				//F_STUCK_FALUT = 1;
				F_TEST_M_INI = 0;
				F_TEST = 0;
				FLAG_PTT_CONDITION = 0;
			}
		}
	}
	else				//release
	{	
		if(Key_Count>=12)	//12*8=96ms
		{	
			if(F_CO_ALARM)
			{
				_ops2 = 0;
				 _ops1 = 0;
				S_Delay_ms(3000);
				_wdtc = 0;
				while(1);	
			}
			else if(FLAG_END_FATAL)
			{
				_ops2 = 0;
				 _ops1 = 0;
				S_Delay_ms(3000);
				_wdtc = 0;
				while(1);	
			}
			else if((F_MEMORY_FAULT == 1) || (F_EEPROM_FALULT == 1))
			{
				#if Mode_Serial_Output
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("Memory Fault");
				S_UART_SEND_BYTE('\r');	
				#endif
			}	
			else if(F_CO_ERR == 1)
			{
				u8 i = 0;
				for(i = 0; i <CO_FAULT_CODE; i++)
				{
					Amber_Blink(Amber_Blink_Width);
					S_Delay_ms(Amber_Close_Delay);
					GCC_CLRWDT();
					#if 0
					S_UART_SEND_Str("CO FAULT BLINK:");
					S_UART_SEND_ASCII_Dec((i&0xff00)>>8,i&0x00ff);
					#endif
				}
				S_Delay_ms(1000);
				_wdtc = 0;
				while(1);
			}
			else if(F_STUCK_FALUT == 1)
			{
				FLAG_PTT_CONDITION = 0;	
				F_STUCK_FALUT = 0;
				F_BUZZER_ONEPERIOD = 0;
			}
			else if((F_LIFE_REACH == 1) && (FLAG_EOL_HUSH == 0))
			{
				FLAG_EOL_HUSH = 1;
				F_EOL_M_INI = 1;
				F_LIFE_REACH = 0;
				u8 i = 0;
				for(i = 0; i <EOL_CODE; i++)
				{
					Amber_Blink(Amber_Blink_Width);
					S_Delay_ms(Amber_Close_Delay);
					GCC_CLRWDT();
				}	

				#if Mode_Serial_Output
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("EOL HUSH");
				S_UART_SEND_BYTE('\r');	
				#endif
				
				S_Delay_ms(3000);
				F_EOL_HUSH_M_INI = 0;
				
			}
			else if(F_LOW_BAT_FATAL == 1)
			{
				Amber_Blink(Amber_Blink_Width);	
				
				#if Mode_Serial_Output
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("LB FATAL");
				S_UART_SEND_BYTE('\r');	
				#endif
			}
			else if((F_BAT_ISLOW == 1) && (FLAG_LOW_BAT_HUSH == 0))
			{
				Amber_Blink(Amber_Blink_Width);	
				FLAG_LOW_BAT_HUSH = 1;
				F_LB_HUSH_M_INI = 0;
				F_BAT_ISLOW= 0;
				
				#if Mode_Serial_Output
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("LB HUSH");
				S_UART_SEND_BYTE('\r');	
				#endif
			}
			else if(F_CO_ALARM_MEMEORY == 1)
			{
				F_CO_ALARM_MEMEORY = 0;	
				F_TEST = 0;	
				FLAG_PTT_CONDITION = 0;
				R_INI_FG.byte = 0; 
				R_INI_FG1.byte = 0;
				F_BUZZER_ONEPERIOD = 0;
					
				#if 1
				//save day
				S_WR_EE(CO_ALARM_EVENT,DAY&0x00ff);
				S_WR_EE(CO_ALARM_EVENT+0x01,(DAY&0xff00)>>8);	
						
				u8 temp_co_alarm_event =0;
				temp_co_alarm_event = S_RD_EE(CO_ALARM_EVENT+0x02);
				if(temp_co_alarm_event >= 255) temp_co_alarm_event = 0;
				temp_co_alarm_event = temp_co_alarm_event +1;
				S_WR_EE(CO_ALARM_EVENT+0x02,temp_co_alarm_event);
				//S_WR_EE(CO_ALARM_EVENT,S_RD_EE(CO_ALARM_EVENT) >= 255 ? 0:S_RD_EE(CO_ALARM_EVENT)+1);
				#endif
				
				#if Mode_Serial_Output
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("EXIT Alarm Memory");
				S_UART_SEND_BYTE('\r');	
				#endif
			}
			else if(FLAG_LOW_BAT_HUSH == 1)
			{
				F_BUZZER_ONEPERIOD = 0;
				FLAG_LOW_BAT_HUSH = 0;
				FLAG_PTT_CONDITION = 1;
				F_TEST_M_INI = 0;	
				
				#if Mode_Serial_Output
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("EXIT LB HUSH");
				S_UART_SEND_BYTE('\r');	
				#endif
			}
			else if(FLAG_EOL_HUSH == 1)
			{
				FLAG_EOL_HUSH = 0;
				u8 i = 0;
				for(i = 0; i <EOL_CODE; i++)
				{
					Amber_Blink(Amber_Blink_Width);
					S_Delay_ms(Amber_Close_Delay);
					GCC_CLRWDT();
				}
				
				#if Mode_Serial_Output
				S_UART_SEND_BYTE('\n');
				S_UART_SEND_Str("EXIT EOL HUSH");
				S_UART_SEND_BYTE('\r');	
				#endif	
				
				S_Delay_ms(3000);	
				F_LIFE_REACH = 1;
				F_EOL_M_INI = 0;
				
			}
			else
			{
				if(FLAG_PTT_CONDITION == 0)
				{	
					FLAG_PTT_CONDITION = 1;	
					//clear count
					R_BUZZON_CNTSET = 0;
					R_BUZZOFF_CNTSET = 0;
					R_BUZZ_SERIESSET = 0;
					R_BUZZWAIT_CNTSET = 0;
					F_BUZZER_ONEPERIOD = 0;
					stopcnt = 0;
					
					//OPA output allaways 0
					_ops2 = 1;
					//delay
					S_Delay_ms(5);	
					//OPA Calibration
					Opamp_Calibration();
					_ops2 = 0;
				}	
			}
		}
		Key_Count = 0;
		F_STUCK_FALUT = 0;
		//F_PTT_FAULT = 0;
	}
	
	if(FLAG_PTT_CONDITION)
	{
		FLAG_PTT_CONDITION = 0;
		//S_UART_SEND_Str("PTT_CONDITION");
		
//		_oppw0 = 1;
//		_ops8 = 0;
//		_ops0 = 1;
//		S_Delay_ms(100);
//		u16 key_adc = 0;
//		key_adc = Read_CO_Value();
//		_oppw = 0b00000011;
//	 	_opsw0 = 0b00001010;
//	 	_opsw1 = 0b00000001;
//	 	
//	 	S_UART_SEND_BYTE('\n');
//		S_UART_SEND_Str("key to adc:");
//		//_itoa(key_adc,10);
//		S_UART_SEND_ASCII_Dec((key_adc&0xff00)>>8,key_adc&0x00ff);
//		S_UART_SEND_BYTE('\r');
	 	
	 	
		
		#if 0
		u16 key_adc = 0;

			u8 OPVOS_Recover = 0;
			OPVOS_Recover = _opvos;
			_opvos = _opvos + 5;	//5
			_opofm = 1;
			S_Delay_ms(5);
			key_adc = Read_CO_Value();
			_opofm = 0;
			_opvos = OPVOS_Recover;
		#endif
		
		//current
	#if 1
		u16 key_adc = 0;
		int i;//,j
		for (i=0 ; i<500 ; i++)
		{
			//feed watchdog
			GCC_CLRWDT();
			key_adc = Read_CO_Value();
			if(key_adc<25)	//30  25 = 500*7/140
			{
				//for (i=0 ; i<5 ; i++)
				{
					MEAS_Send_Charge_Pulse();	
				}
			}
		}
		PTT_Count = 20;
	#endif	
		
		
		#if 0
		u16 temp_scale = 0,temp_ppm = 0;
		temp_scale = (u16)(S_RD_EE(Cal_Scale_MSB) << 8) | S_RD_EE(Cal_Scale_LSB);
		temp_ppm = (u16)(((u32)(key_adc - OPPM_OFFSET) * 100) / temp_scale) ;
		S_UART_SEND_BYTE('\n');
		S_UART_SEND_Str("key to adc:");
		//_itoa(key_adc,10);
		S_UART_SEND_ASCII_Dec((key_adc&0xff00)>>8,key_adc&0x00ff);
		S_UART_SEND_BYTE('\r');
		S_UART_SEND_BYTE('\n');
		S_UART_SEND_Str("key to ppm:");
		//_itoa(temp_ppm,10);
		S_UART_SEND_ASCII_Dec((temp_ppm&0xff00)>>8,temp_ppm&0x00ff);
		S_UART_SEND_BYTE('\r');
		#endif
		
		
		if((key_adc>=25) || (FLAG_CO_ERR_ENABLE == 1))		// && (key_adc <=420)420 is remove CO sensor adc value, 30 need to test
		{
			//F_TEST = 1;
			FLAG_PTT_WAIT = 1;
			F_PTT_FAULT = 0;
			PTT_Wait_Count = 0;	
		}
		else
		{
			if(--PTT_Count == 0)
            {
            	//  Unit has not gone into alarm. That's a PTT fault.
				FLAG_PTT_WAIT = 0;
				//F_TEST = 0;	
				F_PTT_FAULT_M_INI = 0;
				F_PTT_FAULT = 1;
           	}
           	else
            {
            	// Hold Test charge level during sampling
				MEAS_Send_Charge_Pulse();
            }
		}	
	}
	
	if(FLAG_PTT_WAIT)
	{
		PTT_Wait_Count ++;
		if(PTT_Wait_Count <4)
		{
			P_BUZZ_EN = 0;
			#if F_PWM
				_pton = 1;
			#else
				//_pa3 = 1;
			#endif
		}
		if(PTT_Wait_Count ==4)
		{
			//close horn
			//P_BUZZ_EN = 1;
			#if F_PWM
				_pton = 0;
			#else
				//_pa3 = 0;
			#endif
		}
		if(PTT_Wait_Count >=250)
		{
			PTT_Wait_Count = 0;
			FLAG_PTT_WAIT = 0;
			F_TEST = 1;		
		}	
	}			
	
	if(F_TEST)//&&(1==P_TEST_KEY)
	{
		if(F_BUZZER_ONEPERIOD)
		{
			F_BUZZER_ONEPERIOD = 0;
			//button cycle，TWO T-4
			stopcnt++;
			if (stopcnt < 2);
			else
			{
				F_TEST =0;
				stopcnt = 0;
				//==================
				
				S_WR_EE(PTT_Day_L_Address,DAY&0x00ff);
				S_WR_EE(PTT_Day_H_Address,(DAY&0xff00)>>8);
				
				u8 temp_ptt_count = 0;
				temp_ptt_count = S_RD_EE(PTT_Count_Address);
				if(temp_ptt_count >= 255) temp_ptt_count = 0;
				temp_ptt_count = temp_ptt_count + 1;
				S_WR_EE(PTT_Count_Address,temp_ptt_count);
//				S_WR_EE(PTT_Count_Address,S_RD_EE(PTT_Count_Address) >= 255 ? 0:S_RD_EE(PTT_Count_Address)+1);
				if(FLAG_PWR_12_Min == 1)
				{
					S_WR_EE(PTT_FLAG_Address,0x01);
					S_WR_EE(D_wPastSum_L_Address,w1AlarmPastSum&0x00ff);
					S_WR_EE(D_wPastSum_H_Address+0x01,(w1AlarmPastSum&0xff00)>>8);
					S_WR_EE(D_InterimSum_L_Address,w1AlarmInterimSum&0x00ff);
					S_WR_EE(D_InterimSum_H_Address+0x01,(w1AlarmInterimSum&0xff00)>>8);
					S_WR_EE(D_AlarmSum_L_Address,wAlarmSum&0x00ff);
					S_WR_EE(D_AlarmSum_H_Address+0x01,(wAlarmSum&0xff00)>>8);
				}


				//S_WR_EE(PTT_Count_Add,S_RD_EE(PTT_Count_Add) >= 255 ? 0:S_RD_EE(PTT_Count_Add)+1);
//				//reset 
//				//while(1);
//				//_pcl = 0x01F8;	//pc  jump to while(1)  address 
				S_Delay_ms(2000);
				_wdtc = 0;
				while(1);
				//S_Delay_ms(3000);
				//_fc1 = 0x55;
			}
		}
	}
}
#endif