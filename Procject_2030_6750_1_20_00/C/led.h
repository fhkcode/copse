/*========================================================
*@File name:led.h
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#ifndef	_LED_H__
#define _LED_H__

#include "BA45F6750.h"
#include "common.h"

#define		New_Led		0
#define 	Old_Led		1

#define		LED_Set		0
#define 	Buzzer_Set	1


#define	P_LED_R			_pb2
#define	P_LED_Y			_pb3
#define	P_LED_G			_pa6

#define LED_Close		_pb2 = 1; _pb3 =1; _pa6 = 1;R_LED_FG.byte = 0;

#if Old_Led
void S_LED_FLASH_CTRL(void);
void S_LED_FLASH_FUN(u8 R_LED_ON,u8 R_LED_OFF);
void LED_STOPFLASH(void);
void S_YLED_FLASH(u8 cnt);
void LED_BUZZER_ON(u8 Set,u8 R_ON, u8 R_OFF, u8 R_CNT,u16 R_WAIT,u8 R_ONCE);
#endif

#if New_Led
void Blink_Led(u16 Delay_Count,u8 Led_Set,u16 Blink_Count, u16 Blink_Wiat);
#endif

void S_LED_ON(u8 R_LED_ON, u8 R_LED_OFF, u8 R_LED_CNT,u16 R_LED_WAIT,u8 R_LED_ONCE);

void Amber_Blink(u16 Delay_ms);
#endif

