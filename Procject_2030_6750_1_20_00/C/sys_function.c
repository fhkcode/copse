/*========================================================
*@File name:sys_function.c
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#include "sys_function.h"
#include "co.h"
#include "eeprom.h"
#include "led.h"
#include "buzzer.h"
#include "sys_init.h"
#include "adc.h"
#include "uart.h"
#include "timer.h"

//数据段
//====================================
//const char ORDER_REV[] = "Rst-14\r\0";
//const char ORDER_Model[] = ">Model 2030-DCR\r\0";
//const char ORDER_FW[] = ">FW Ver 2.0.3\r\0";
//const char ORDER_V[] = ">V:\0";
//const char ORDER_C[] = ">C:\0";
//const char ORDER_B[] = ">B:\0";
//const char ORDER_L[] = ",L:\0";
//const char ORDER_ID[] = ">ID:0\r\0";
//=====================================

u8 Uart_Cmd_Add,Uart_Recive_Data = 0;
u8 R_MODE_FG = 0;
u16 Time_Rate = 0;
/*u8 Pre_MODE_FG = 0;*/
volatile _byte_type		F_RUN_FLAG;
volatile _byte_type		R_SYS_FG2;
volatile _byte_type		R_SYS_FG3;

/**************************************
*@neam:S_SysTimeTask
*@brief:系统1s计时
*@param[in]:
*@retval: 
*@notes:
**************************************/
//void S_SysTimeTask(void)
//{
//	R_SOFT_TIMER_CNT++;
//	if((R_SOFT_TIMER_CNT >= C_SOFT_TIMER_CNT)||(F_SYS_SLOW==1))
//	{
//		R_SOFT_TIMER_CNT = 0;		//125*8  = 1秒
//		F_OneSec = 1;
//		F_ONESEC_MODE=1;
//	}
//}




/**************************************
*@neam:S_SYSMODE_DEAL
*@brief:系统各种模式处理
*@param[in]:
*@retval: 
*@notes:
**************************************/
void S_SYSMODE_DEAL(void)
{
	static	u8 R_PROM_CNT;
	static  u32 EOL_CNT;
	static  u32	Low_CNT;
	
	//测试模式处理
	if(F_TEST == 1)											
	{
//		#if New_Led
//		Chirp_Once(0,12,4,250,2);	// T4
//		_LED_G_ON
//		_LED_Y_ON
//		Blink_Led(0,1,2,62);	//
//		#endif
//		
//		#if Old_Led
		if(F_TEST_M_INI == 0)
		{
			R_INI_FG.byte = 0;
			R_INI_FG1.byte = 0;
			F_TEST_M_INI = 1;
			LED_STOPFLASH();
			Horn_OFF();	
			_LED_G_ON
			_LED_Y_ON	
			F_LED_R = 1;
			//S_LED_FLASH_FUN(30,30);
			S_LED_ON(12,11,4,625,0);
			S_BUZZER_ON(12,11,4,625,0);	//250  625
//			LED_BUZZER_ON(0,12,12,4,625,0);
//			LED_BUZZER_ON(1,12,12,4,625,0);
		}
		//#endif
	}
	else													//其它模式处理
	{
		/*Pre_MODE_FG = R_MODE_FG;*/
		//u8 R_MODE_FG = 0;	
		R_MODE_FG = 0;		
		
		if(FLAG_EOL_HUSH == 1)			R_MODE_FG = 14;		//EOL HUSH
		if(F_CO_BD_EN == 1)				R_MODE_FG = 13;		//calibration 
		if(F_POWERON_MODE == 1)			R_MODE_FG = 12;		//preheat	
		if(F_PTT_FAULT == 1)			R_MODE_FG = 11;		//PTT FAULT
		if(FLAG_LOW_BAT_HUSH == 1)		R_MODE_FG = 10;		//low battery hush
		if(F_CO_ALARM_MEMEORY == 1)		R_MODE_FG = 9;		//alarm memory
		if(F_BAT_ISLOW == 1)			R_MODE_FG = 8;		//low battery
		if(F_LOW_BAT_FATAL == 1)		R_MODE_FG = 7;		//low battery fatal
		if(F_LIFE_REACH == 1)			R_MODE_FG = 6;		//end of life 														
		if(F_STUCK_FALUT == 1)			R_MODE_FG = 5;		//stuck fault
		if((F_MEMORY_FAULT == 1) || (F_EEPROM_FALULT == 1))			
										R_MODE_FG = 4;		//memory fault
		if(FLAG_END_FATAL == 1)			R_MODE_FG = 3;		///EOL Fatal
		if(F_CO_ERR == 1)				R_MODE_FG = 2;		//CO sensor fault
		if(F_CO_ALARM == 1)				R_MODE_FG = 1;		//co alarm &  CO alarm conserve
				
		switch(R_MODE_FG)
		{	
			//===================================================
			//EOL HUSH
			case 14:
				if(F_EOL_HUSH_M_INI==0)
				{
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					F_EOL_HUSH_M_INI = 1;
					LED_STOPFLASH();
					Horn_OFF();
					F_LED_Y = 1;
					S_LED_ON(12,38,2,7448,0);		//3750	amber blink 2 times /60s	
				}

				if(F_CAL_Onemin == 1)
				{
					F_CAL_Onemin = 0;
					EOL_CNT ++;
						
					if(EOL_CNT >= (1310/Time_Rate))		//24hour exit 1440  1410
					{
						EOL_CNT	= 0;
						F_LIFE_REACH = 1;
						FLAG_EOL_HUSH = 0;
						LED_STOPFLASH();
						Horn_OFF();
						F_EOL_HUSH_M_INI = 0;
						if(Time_Rate !=1)
						{
							DAY++;
							FLAG_HISTORY_UPDATA =1;	
						}
					}	
				}
			break;
			//===================================================
			//calibration mode
			case 13:
				if(F_BD_M_INI == 0)
				{	
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					LED_STOPFLASH();
					if(Sys_Cal_State == 0)		//PCB led
					{
						if(!FLAG_PCB_CAL_FLAG)		//正在校准
						{
							_LED_G_ON;				
						}							//校准完成
						else
						{
							if(FLAG_PCB_CAL_FAULT == 1)	//校准失败
							{
								_LED_R_ON;	
							}
							else
							{
								_LED_G_OFF;
							}
						}			
					}
					if(Sys_Cal_State == 1)		//OPPM led
					{

						if(!FLAG_CAL_OPPM_FLAG)		//正在校准
						{
							//7*8=56ms Green blink
							F_LED_G = 1;
							S_LED_ON(7,7,1,12,0);
/*							LED_BUZZER_ON(0,7,7,1,12,0);*/
							//S_LED_FLASH_FUN(7,7);	//20,20   50ms闪
											
						}							//校准完成
						else
						{
							if(FLAG_CAL_0PPM_FAULT == 1)	//校准失败
							{
								_LED_R_ON;
								S_BUZZER_ON(10,0,1,0,1);	
/*								LED_BUZZER_ON(0,10,0,1,0,1);*/
							}
							_LED_G_OFF;
							_LED_Y_OFF;
						}			
					}
					if(Sys_Cal_State == 2)		//15OPPM led
					{
						if(!FLAG_CAL_150PPM_FLAG)		//正在校准	
						{
							//62*8=496ms Green blink
							F_LED_G = 1;
							S_LED_ON(62,62,1,125,0);
	/*						LED_BUZZER_ON(0,62,62,1,125,0);*/
							//S_LED_FLASH_FUN(62,62);		//20,20  500ms闪				
						}
						else
						{
							if(FLAG_CAL_150PPM_FAULT == 1)	////校准失败
							{
								//红灯常亮
								_LED_R_ON;				
							}
							_LED_G_OFF;
							_LED_Y_OFF;
						}
					}
					if(Sys_Cal_State == 3)		//300PPM led
					{
						if(!FLAG_CAL_300PPM_FLAG)		//正在校准
						{
							_LED_G_ON;
							_LED_Y_OFF;
							_LED_R_OFF;
						}
						else
						{
							if(FLAG_CAL_300PPM_FAULT == 1)	//校准失败
							{		
								_LED_G_OFF;
								_LED_Y_OFF;
								_LED_R_ON;
							}
							else
							{
								//红灯闪		
								F_LED_R = 1;
								S_LED_ON(62,62,1,125,0);
/*								LED_BUZZER_ON(0,62,62,1,125,0);*/
								//S_LED_FLASH_FUN(62,62);		//20,20  500ms闪
							}
						}
					}
					F_BD_M_INI = 1;	
				}
			break;
			
			//===================================================
			//stable mode:  wait opa  voltage stable
			//stable time is 120s， blink  green/amber  1s
			case 12:
				if(F_POWERON_M_INI == 0)
				{
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					F_POWERON_M_INI=1;
					LED_STOPFLASH();
					Horn_OFF();	
					F_LED_G = 1;
					F_LED_Y = 1;
					S_LED_ON(122,122,1,12,0);
				}
			break;

			//===================================================
			//PTT FAULT
			//amber blink 7 times/30s？
			case 11:
				if(F_PTT_FAULT_M_INI == 0)
				{
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					F_PTT_FAULT_M_INI = 1;
					LED_STOPFLASH();
					Horn_OFF();		
					F_LED_Y = 1;
					S_LED_ON(12,38,7,3750,1);
				}
//				static u8 PTT_FAULT_Exit_Cnt = 0;
//				
//				PTT_FAULT_Exit_Cnt ++;
//				if(PTT_FAULT_Exit_Cnt >1)
//				{
//					F_PTT_FAULT = 0;
//					PTT_FAULT_Exit_Cnt = 0;
//				}
				
			break;
			
			//===================================================
			//low battery hush blink 1 times/60s？
			case 10:
				if(F_LB_HUSH_M_INI == 0)
				{
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("low battery Hush");
					S_UART_SEND_BYTE('\r');	

					R_INI_FG1.byte = 0;
					R_INI_FG.byte = 0;
					F_LB_HUSH_M_INI = 1;
					LED_STOPFLASH();
					Horn_OFF();
					F_LED_Y = 1;	
					S_LED_ON(12,38,1,7500,0);			//Amber blink 1 time/60s
				}
				if(F_CAL_Onemin == 1)
				{
					F_CAL_Onemin = 0;
					Low_CNT++;
					if(Low_CNT >= (1280/Time_Rate))		//24hour   1440  1410
					{
						#if Mode_Serial_Output
						S_UART_SEND_BYTE('\n');
						S_UART_SEND_Str("LB HUSH timeout");
						S_UART_SEND_BYTE('\r');	
						#endif
						Low_CNT = 0;
						FLAG_LOW_BAT_HUSH = 0;
						R_INI_FG1.byte = 0;
						R_INI_FG.byte = 0;
						LED_STOPFLASH();
						Horn_OFF();	
						S_BAT_LOWCHECK();
						if(Time_Rate !=1)
						{
							DAY++;
							FLAG_HISTORY_UPDATA =1;	
						}
					}
				}
			break;
			
			//===================================================
			//alarm memory 
			//red blink 1 /60s
			case 9:
				if(F_ALARM_MEMORY_M_INI==0)
				{
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					F_ALARM_MEMORY_M_INI = 1;
					LED_STOPFLASH();
					Horn_OFF();
					//if(!FLAG_FAULT_HUSH)
					if(!FLAG_ALARM_MEMORY_HUSH)
					{
						F_LED_R = 1;	
						S_LED_ON(12,38,1,7500,0);
					}
					else
					{
						F_CO_ALARM_MEMEORY = 0;
						F_NORMAL_M_INI = 0;
					}
					
					#if Mode_Serial_Output
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("Alarm Memory ");
					S_UART_SEND_BYTE('\r');	
					#endif
				}
				if(F_CAL_Onemin == 1)
				{
					F_CAL_Onemin = 0;
					static u32 alarm_memory_count = 0;
					
					if((alarm_memory_count ++) >= 20000/Time_Rate)		// 10080  14 day //1 hour 3600
					{	
						S_UART_SEND_Str("alarm memory event");
						
						F_CO_ALARM_MEMEORY = 0;	
						R_INI_FG.byte = 0;
						R_INI_FG1.byte = 0;
						alarm_memory_count = 0;	
						
						#if 1
						//save day
						S_WR_EE(CO_ALARM_EVENT,DAY&0x00ff);
						S_WR_EE(CO_ALARM_EVENT+0x01,(DAY&0xff00)>>8);	
						
						u8 temp_co_alarm_event =0;
						temp_co_alarm_event = S_RD_EE(CO_ALARM_EVENT+0x02);
						if(temp_co_alarm_event >= 255) temp_co_alarm_event = 0;
						temp_co_alarm_event = temp_co_alarm_event +1;
						S_WR_EE(CO_ALARM_EVENT+0x02,temp_co_alarm_event);
						//S_WR_EE(CO_ALARM_EVENT,S_RD_EE(CO_ALARM_EVENT) >= 255 ? 0:S_RD_EE(CO_ALARM_EVENT)+1);
						#endif
					}
			}
			break;
			
			//===================================================
			//Low Battery
			//Low Battery hush
			//Amber blink 1 time/30s
			//chirp 1 time/60s
			case 8:
				if(F_LOW_BAT_M_INI==0)
				{
					#if 0
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("low battery mode");
					S_UART_SEND_BYTE('\r');	
					#endif
					R_INI_FG1.byte = 0;
					R_INI_FG.byte = 0;
					F_LOW_BAT_M_INI = 1;
					LED_STOPFLASH();
					Horn_OFF();
					F_LED_Y = 1;	
					S_LED_ON(12,38,1,7500,0);			//Amber blink 1 time/60s
					S_BUZZER_ON(12,38,1,7500,0);		//chirp 1 time/60s	 12
				}

			break;
			
			//===================================================
			////low battery fatal
			case 7:
				if(F_LB_FATAL_M_INI==0)
				{
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					F_LB_FATAL_M_INI = 1;
					LED_STOPFLASH();
					Horn_OFF();
					F_LED_Y = 1;	
					S_LED_ON(12,38,1,3700,0);		//amber blink 1 times/ 30s
					//if(!FLAG_LOW_BAT_HUSH)
					S_BUZZER_ON(12,12,1,7500,0);		//1 chirp times/60s
				}
			break;
			
			//===================================================
			//End of life
			//amber blink 2 times /30s
			//chirp 2 times /30s
			case 6:	
				if(F_EOL_M_INI==0)
				{
					#if 0
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("eol mode");
					S_UART_SEND_BYTE('\r');	
					#endif
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					F_EOL_M_INI = 1;
					LED_STOPFLASH();
					Horn_OFF();
					F_LED_Y = 1;
					S_LED_ON(12,38,2,7448,0);		//3750	amber blink 2 times /30s
					S_BUZZER_ON(12,12,2,7500,0);		//  chirp 2 times /30s
				}
			break;
			
			//===================================================
			//Stuck button
			case 5:
				if(F_TEST_M_INI == 0)
				{
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					F_TEST_M_INI = 1;
					LED_STOPFLASH();
					Horn_OFF();		
					F_LED_Y = 1;
					S_LED_ON(12,38,1,625,0);
					S_BUZZER_ON(12,38,1,625,0);
					
					#if Mode_Serial_Output
					S_UART_SEND_BYTE('\n');
					S_UART_SEND_Str("Stuck Button");
					S_UART_SEND_BYTE('\r');	
					#endif
				}
			break;
			
			//===================================================
			//memory falult 
			//Amber Blink 3 times/30s
			//chirp 1 times /30
			case 4:
				if(F_MEMORY_FAULT_M_INI==0)
				{
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					F_MEMORY_FAULT_M_INI = 1;
					LED_STOPFLASH();
					Horn_OFF();
					//if(!FLAG_FAULT_HUSH)
					{
						F_LED_Y = 1;	
						S_LED_ON(12,38,3,3624,0);		//amber blink 3 times/ 30s
						S_BUZZER_ON(12,12,1,3750,0);		//1 chirp times/30s	
					}
				}
			break;
			
			//===================================================
			//EOL Fatal
			case 3:
				if(F_EOL_M_INI==0)
				{
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					F_EOL_M_INI = 1;
					LED_STOPFLASH();
					Horn_OFF();
					F_LED_Y = 1;
					S_LED_ON(12,38,2,7448,0);		//3750	amber blink 2 times /30s	
					S_BUZZER_ON(12,12,2,7500,0);		//  chirp 2 times /30s
				}
			break;
			
			//===================================================
			//CO sonser fault mode
			//30s amber blink 5 times/ 30s 1 chirp time）
			case 2:
				if(F_ERR_M_INI==0)
				{
					#if 0
					S_UART_SEND_Str("CO sonser fault mode");
					#endif
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					F_ERR_M_INI = 1;
					LED_STOPFLASH();
					Horn_OFF();
					S_BUZZER_ON(12,12,1,3750,0);		//30s amber blink 5 times/ 30s 1 chirp time
					F_LED_Y = 1;	
					S_LED_ON(12,38,5,3524,0);					//30s amber blink 5 times/ 30s 1 chirp time
					
					//save event
					S_WR_EE(CO_FAULT_EVENT,DAY&0x00ff);
					S_WR_EE(CO_FAULT_EVENT+0x01,(DAY&0xff00)>>8);
					
					u8 temp_co_fault = 0;
					temp_co_fault = S_RD_EE(CO_FAULT_EVENT+0x02);
					if(temp_co_fault >=255)temp_co_fault =0;
					temp_co_fault = temp_co_fault + 1;
					S_WR_EE(CO_FAULT_EVENT+0x02,temp_co_fault);
				}
			break;
			//===================================================
			//alarm mode
			//T4 
			case 1:
				if(F_ALARM_M_INI == 0)
				{
					R_INI_FG.byte = 0;
					R_INI_FG1.byte = 0;
					F_ALARM_M_INI = 1;
					LED_STOPFLASH();
					F_LED_R = 1;
//					if(F_CO_ALARM_CONSERVE)
//					{
//						S_LED_ON(12,38,4,7396,0);
//						S_BUZZER_ON(12,12,4,7500,0);	//250  625
//					}
//					else
					{
						S_LED_ON(12,11,4,625,0);
						S_BUZZER_ON(12,11,4,625,0);	//250  625
					}
				}
//				if(F_ONESEC_MODE == 1)
//				{
//					F_ONESEC_MODE = 0;
//					R_PROM_CNT++;
//					if((R_PROM_CNT > 240))	//alarm mode keep 4min
//					{
//						R_PROM_CNT = 0;	
//						F_CO_ALARM_CONSERVE = 1;
//						R_INI_FG.byte = 0;
//					}
//				}
			break;
			//===================================================
			//stadby  mode
			case 0:
				if(F_NORMAL_M_INI == 0)
					{
						R_INI_FG.byte = 0;
						R_INI_FG1.byte = 0;
						F_NORMAL_M_INI = 1;
						LED_STOPFLASH();
						Horn_OFF();	
						FLAG_ALARM_MEMORY_HUSH = 0;
						
						#if 0
						S_UART_SEND_Str("stand mode");
						#endif
					}
					if(F_ONESEC_MODE == 1)
					{
						F_ONESEC_MODE = 0;
						//normal blink 
						//green led blink 1/60s）
						R_PROM_CNT++;
						if(R_PROM_CNT >= 60)
						{
							R_PROM_CNT = 0;
							_LED_G_ON
							//_DELAY_MS(50);
							S_Delay_ms(50);
							_LED_G_OFF
						}
					}
			break;
			//===================================================
			//	
			default:
			break;
			
//			R_INI_FG.byte = 0;
//			F_BDE_M_INI = 1;
		}
	}
}

/**************************************
*@neam:S_BAT_LOWCHECK
*@brief:低压检测
*@param[in]:
*@retval: 
*@notes:低电压检测，使用VBG做检测，电压越低，AD值越大
**************************************/
void S_BAT_LOWCHECK(void)
{
//	static	u8 R_BAT_STACNT;
//	_regsw=0;
//	_vbgren=1;							//VBG	
//	S_Delay_ms(1);
//					
//	_sadc0=0b00001111;
//	_sadc1=0b00111101;
//	//S_ADC_START();
//	R_BAT_ADVAL = (Read_ADC_OPAOUT(ADC_12BIT)&0xff00)>>8;
//	_vbgren=0;
////	R_BAT_ADVAL=R_ADC_DATA[1];
//	if(R_BAT_ADVAL >= C_BAT_LOW_SET)
//	{
//		R_BAT_STACNT++;
//		if(R_BAT_STACNT>=C_BAT_LOW_CNTSET)
//		{
//			R_BAT_STACNT=C_BAT_LOW_CNTSET;
//			F_BAT_ISLOW=1;
//		}
//	}
//	else
//	{
//		if(R_BAT_STACNT>0)
//		{
//			R_BAT_STACNT--;
//			if(R_BAT_STACNT==0)
//				F_BAT_ISLOW=0;
//		}
//	}
	
	u8 Bat_Check_Count = 0;
	static	u8 R_BAT_STACNT;
	
	for(Bat_Check_Count = 0; Bat_Check_Count < 3; Bat_Check_Count ++)
	{
		_regsw=0;
		_vbgren=1;							//VBG
		_pa7 = 1;	
		S_Delay_ms(10);				//change delay 1ms to 10ms
					
		_sadc0=0b00001111;
		_sadc1=0b00111101;
		R_BAT_ADVAL = (Read_ADC_OPAOUT(ADC_12BIT)&0xff00)>>8;
		_vbgren=0;
		_pa7 = 0;
		
		if(R_BAT_ADVAL >= C_BAT_LOW_SET)
		{
			R_BAT_STACNT++;
			if(R_BAT_STACNT>=C_BAT_LOW_CNTSET)
			{
				R_BAT_STACNT=C_BAT_LOW_CNTSET;
				//F_BAT_ISLOW=1;
				if(F_BAT_ISLOW == 0)
				{
					if(FLAG_LOW_BAT_HUSH == 0)
					{
						F_BAT_ISLOW = 1;
						F_LOW_BAT_M_INI = 0;
						//F_CO_ALARM_MEMEORY = 0;
						F_BUZZER_RESET = 1;
					}
				} 
				
				#if 1
				//save day
				S_WR_EE(LOW_BAT_EVENT,DAY&0x00ff);
				S_WR_EE(LOW_BAT_EVENT+0x01,(DAY&0xff00)>>8);	
				
				u8 temp_low_bat = 0;
				temp_low_bat = S_RD_EE(LOW_BAT_EVENT+0x02);
				if(temp_low_bat >= 255) temp_low_bat = 0;
				temp_low_bat = temp_low_bat + 1;
				S_WR_EE(LOW_BAT_EVENT+0x02,temp_low_bat);
				//S_WR_EE(LOW_BAT_EVENT,S_RD_EE(LOW_BAT_EVENT) >= 255 ? 0:S_RD_EE(LOW_BAT_EVENT)+1);
				#endif
			}
		}
		else
		{
			if(R_BAT_STACNT>0)
			{
				R_BAT_STACNT--;
				if(R_BAT_STACNT==0)
				{
					F_BAT_ISLOW=0;
				}
			}
		}
	}

	
	if( (F_DEBUG_OUTPUT == 1) )//&& (R_UART_WAIT_CNT == 0) 
	{
		if(F_BAT_ISLOW == 1)
		{
			S_UART_SEND_Str("LB:");
		}
		
		//当前电池AD
		S_UART_SEND_Str(">B:\0");
		//_itoa(R_BAT_ADVAL,10);
		S_UART_SEND_ASCII_Dec(0,R_BAT_ADVAL);
		
		//低压阈值
		S_UART_SEND_Str(",L:\0");
		//_itoa(C_BAT_LOW_SET,10);
		S_UART_SEND_ASCII_Dec(0,C_BAT_LOW_SET);
		S_UART_SEND_BYTE('\r');
	}
//---------------------------------------------	
//	电池需要做内阻检测？如果需要，那么电流怎样提供？
// sounder alarm not check low bat ,sample
//---------------------------------------------		
}



#if 1

void Battery_test(void)
{
	static unsigned short battery_test_count = 0;
	static unsigned short _12_min_battery_test = 0;
	
	if((!F_CO_ALARM) && (F_CO_BD_EN == 0))		// if alarm and calibration, do't check bat
	{
		//1s cycle
		battery_test_count ++;
		if(!FLAG_PWR_12_Min)
		{
			if(battery_test_count >= 60)		//60s
			{
				S_BAT_LOWCHECK();
				//if((F_BAT_ISLOW == 1) && (R_MODE_FG != 7))F_LOW_BAT_M_INI = 0;				
				battery_test_count = 0;	
				_12_min_battery_test ++;
				if(_12_min_battery_test >= 12)		//12min
				{
					FLAG_PWR_12_Min = 1;
					DAY = (u16)(S_RD_EE(Life_Day_MSB) << 8) | S_RD_EE(Life_Day_LSB);	//Correction time，if power on the voltage
					//is unstable  , read day is error, after voltage stable will check the day
					FLAG_HISTORY_UPDATA = 1;
				}
			}	
		}
		else
		{
			if(battery_test_count >= (unsigned short)43200)	//12hour
			{
				battery_test_count = 0;
				//BAT_VDD_Check(1);
				if(F_CO_ALARM == 0)
				{
					S_BAT_LOWCHECK();
					//if((F_BAT_ISLOW == 1) && (R_MODE_FG != 7))F_LOW_BAT_M_INI = 0;	
				}
			}	
		}	
	}
}

#endif

/**************************************
*@neam:S_UART_PROCESS
*@brief:UART数据处理
*@param[in]:
*@retval: 
*@notes:
**************************************/
void S_UART_PROCESS(void)
{
	//接收长度超过
	if(R_UART_BUFFSIZE > 1)
	{
		//to test delete
		unsigned char R_UART_CMD = 0;
		if(S_UART_FindByte('\r') >= 0)
		{
			if( (R_UART_BUFF[0] == 'A') && (R_UART_BUFF[1] == '0') )		R_UART_CMD = 1;		//horn off
			if( (R_UART_BUFF[0] == 'A') && (R_UART_BUFF[1] == '1') )		R_UART_CMD = 2;		//horn of
			if( (R_UART_BUFF[0] == 'B') && (R_UART_BUFF[1] == 'A') )		R_UART_CMD = 3;		//enable serial output
			if( (R_UART_BUFF[0] == 'B') && (R_UART_BUFF[1] == 'Z') )		R_UART_CMD = 4;		//disable serial output
			if( (R_UART_BUFF[0] == 'b'))									R_UART_CMD = 5;		//battery test
			if( (R_UART_BUFF[0] == 'C'))									R_UART_CMD = 6;		//set eeprom
			if( (R_UART_BUFF[0] == 'F'))									R_UART_CMD = 7;		//set alarm
			if( (R_UART_BUFF[0] == 'G'))									R_UART_CMD = 8;		//eeprom data output
			if( (R_UART_BUFF[0] == 'L'))									R_UART_CMD = 9;		//set eol data
			// CMD B
			F_UART_RX_COMTE = 1;
			
			switch(R_UART_CMD)
			{
				//===================================================
				//Horn off
				case 1:
					if(R_UART_BUFF[2] == '\r')
					{
						//Horn_OFF();
							_pa5 = 1;
							_pa3 = 0;
					}
				break;
				//===================================================
				//hron on
				case 2:
					if(R_UART_BUFF[2] == '\r')
					{	
						#if F_PWM
							_pa5 = 0;
							S_Delay_ms(5);
							_pton = 1;
						#else
							_pa5 = 0;
//							S_BUZZER_INIT();
//							_pton = 1;
//							S_Delay_ms(5);
							_pa3 = 1;
//							_pa5 = 0;
//							_pa3 = 1;
//							S_Delay_ms(10);
//							_pa5 = 1;
//							_pa3 = 0;
/*							_pton = 0;*/
						#endif
					}
				break;
				//===================================================
				//enable serial output			
				case 3:
					if(R_UART_BUFF[2] == '\r')
					{	
						F_DEBUG_OUTPUT = 1;
					}
				break;
				//===================================================
				//disable serial output
				case 4:
					if(R_UART_BUFF[2] == '\r')
					{	
						F_DEBUG_OUTPUT = 0;
					}
				break;
				//===================================================
				//battery test
				case 5:
					if(R_UART_BUFF[1] == '\r')
					{
						//当前电池AD
						S_UART_SEND_Str(">B:\0");
						//_itoa(R_BAT_ADVAL,10);
						S_UART_SEND_ASCII_Dec(0,R_BAT_ADVAL);
						
						//低压阈值
						S_UART_SEND_Str(",L:\0");
						//_itoa(C_BAT_LOW_SET,10);
						S_UART_SEND_ASCII_Dec(0,C_BAT_LOW_SET);
						S_UART_SEND_BYTE('\r');
					}	
				break;
				//===================================================
				//set eeprom data
				case 6:
					if(R_UART_BUFF[1] == '1')
					{
						if((R_UART_BUFF[2] == 32) && (R_UART_BUFF[5] == 32))
						{
							Uart_Cmd_Add = S_UART_ASCII_TO_HEX(R_UART_BUFF[3],R_UART_BUFF[4]);
							Uart_Recive_Data = S_UART_ASCII_TO_HEX(R_UART_BUFF[6],R_UART_BUFF[7]);
						}
						else
						{
							S_UART_SEND_Str("Uart CMD ERROR");
							S_UART_SEND_BYTE('\r');
							S_UART_SEND_BYTE('\n');	
						}
						#if 0
						u8 i = 0;
						for(i = 1; i < 9; i ++)
						{
							S_UART_SEND_ASCII_Dec((i&0xff00)>>8,i&0x00ff);
							S_UART_SEND_Str("R_UART_BUFF:");
							S_UART_SEND_ASCII_Dec((R_UART_BUFF[i]&0xff00)>>8,R_UART_BUFF[i]&0x00ff);
							S_UART_SEND_BYTE('\r');
							S_UART_SEND_BYTE('\n');
						}
						#endif
					}
					if(R_UART_BUFF[1] == '2')
					{
						if((Uart_Cmd_Add <= 0xFF) && (Uart_Recive_Data <= 0xFF))
						{
							S_WR_EE(Uart_Cmd_Add,Uart_Recive_Data);	
							Uart_Cmd_Add = 0;
							Uart_Recive_Data = 0xFF;
						}
						else
						{
							S_UART_SEND_Str("Uart CMD ERROR");
							S_UART_SEND_BYTE('\r');
							S_UART_SEND_BYTE('\n');	
						}
					}
				break;
				//===================================================
				//set alarm
				case 7:
					if(R_UART_BUFF[1] == '\r')
					{
						F_CO_ALARM = 1;
						R_MODE_FG = 1;
					}
				break;
				case 8:
					if(R_UART_BUFF[1] == '\r')
					{
						Read_EEPROM();
					}
					if(R_UART_BUFF[1] == '1')
					{
						
					}
					if(R_UART_BUFF[1] == '2')
					{
						
					}
					if(R_UART_BUFF[1] == '3')
					{
						Recovery_State ++;
						if(Recovery_State>=5)Recovery_State = 0;
						S_UART_SEND_BYTE('\n');
						S_UART_SEND_Str("HPPM State Add:");
						S_UART_SEND_BYTE('\r');	
					}
					if(R_UART_BUFF[1] == '4')
					{

					}
					if(R_UART_BUFF[1] == '5')
					{
						S_UART_SEND_BYTE('\n');
						S_UART_SEND_Str("R_MODE_FG:");
						S_UART_SEND_ASCII_Dec((R_MODE_FG&0xff00)>>8,R_MODE_FG&0x00ff);
						//S_UART_SEND_ASCII_Hex(R_MODE_FG);
						S_UART_SEND_BYTE('\r');	
					}
				break;
				case 9:
					//reset time of life to 0,and speed to 0
					if(R_UART_BUFF[1] == '0')
					{
						DAY = 0;
						FLAG_HISTORY_UPDATA = 1;
						//save day
						S_WR_EE(Life_Day_LSB,DAY&0x00ff);
						S_WR_EE(Life_Day_MSB,(DAY&0xff00)>>8);	
					}
					//set time of life to 3748 day,and counter to 2
					if(R_UART_BUFF[1] == '1')
					{
						DAY = 3750;
						FLAG_HISTORY_UPDATA = 1;
						//save day
						S_WR_EE(Life_Day_LSB,DAY&0x00ff);
						S_WR_EE(Life_Day_MSB,(DAY&0xff00)>>8);	
					}
					//set year conter to value
					if(R_UART_BUFF[1] == '2')
					{
						DAY = 3757;
						FLAG_HISTORY_UPDATA = 1;
						//save day
						S_WR_EE(Life_Day_LSB,DAY&0x00ff);
						S_WR_EE(Life_Day_MSB,(DAY&0xff00)>>8);	
					}
					//set year conter to value
					if(R_UART_BUFF[1] == '3')
					{
						R_Clock_sec = 0;
						R_Clock_min = 0;
						R_Clock_hour = 0;
					}
					if(R_UART_BUFF[1] == '4')
					{
						//1MIN TO 1 HOUR
						Time_Rate = 60;
						#if 1
						S_UART_SEND_BYTE('\n');
						S_UART_SEND_Str("1MIN TO 1HOUR:");
						S_UART_SEND_BYTE('\r');	
						#endif
					}
					if(R_UART_BUFF[1] == '5')
					{
						//1MIN TO 1 DAY
						Time_Rate = 1440;
						#if 1
						S_UART_SEND_BYTE('\n');
						S_UART_SEND_Str("1MIN TO 1DAY:");
						S_UART_SEND_BYTE('\r');	
						#endif	
					}
				break;
				//===================================================
				//
				default:
				break;
			}
		}
		else
		{
			//长度超过复位
			if(R_UART_BUFFSIZE > C_UART_RX_BUFFSIZE-1)
			{
				F_UART_RX_COMTE = 1;
			}
			else
			{
				//over time reset
				if(R_UART_WAIT_CNT <= 0)
				{
					F_UART_RX_COMTE = 1;
				}
			}
		}
		//===================================================	
		//uart reset
		if(F_UART_RX_COMTE == 1)
		{
			F_UART_RX_COMTE = 0;
			S_UART_PTRESET();
		}
	}
		
}
/**************************************
*@neam:S_UART_DATA_Output
*@brief:串口数据输出
*@param[in]:
*@retval: 
*@notes:1s一次
**************************************/
void S_UART_DATA_Output(void)
{
	if((F_DEBUG_OUTPUT == 1))  //&& (R_UART_WAIT_CNT == 0)
		{
			//===================================================
			//#if 1
			S_UART_SEND_BYTE('\n');
			S_UART_SEND_Str("V:");
			//_itoa(average_counts,10);
			S_UART_SEND_ASCII_Dec((average_counts&0xff00)>>8,average_counts&0x00ff);
			//S_UART_SEND_BYTE('\r');
			//#endif
			
			//浓度
			S_UART_SEND_Str(">C:\0");
			
			//正常模式 或 报警模式输出PPM值
			//if((F_CO_ALARM == 1) || (F_NORMAL_M_INI == 1))
			{
				//最大输出 999 PPM  是否需要做此限制？
		//		if(wPPM >= 999)
		//		{
		//			S_UART_SEND_ASCII_Dec((999&0xff00)>>8,999&0x00ff);
		//		}
		//		else
		//		{
					//S_UART_SEND_ASCII_Dec((wPPM&0xff00)>>8,wPPM&0x00ff);
/*					_itoa(wPPM,10);*/
		//		}
			}
			//其他模式下则输出0
//			else
//			{
//				//S_UART_SEND_ASCII_Dec(0,0);
//				_itoa(0,10);
//			}
			
			//if(F_CO_BD_EN == 1)
			if((Sys_Cal_State == 0)  || (Sys_Cal_State == 1))
			{
				//_itoa(0,10);
				S_UART_SEND_ASCII_Dec(0,0);	
			}
			else
			{
				//_itoa(wPPM,10);
				S_UART_SEND_ASCII_Dec((wPPM&0xff00)>>8,wPPM&0x00ff);
			}
			
			if(F_CO_ALARM == 1)
			{
				S_UART_SEND_Str("*");	
			}
			
//			S_UART_DATA_CO_Ooutput();
			//所有数据输出
//			if( F_UART_CMD_Output_ALL == 1)
//			{				
//				//OPA电压 & CO浓度值
//				S_UART_DATA_CO_Ooutput();	
//				//当前电池AD
//				S_UART_SEND_Str(ORDER_B);
//				S_UART_SEND_ASCII_Dec(0,R_BAT_ADVAL);
//				//低压阈值
//				S_UART_SEND_Str(ORDER_L);
//				S_UART_SEND_ASCII_Dec(0,C_BAT_LOW_SET);
//				S_UART_SEND_BYTE('\r');
//				
//			}
//			//===================================================
//			//CO数据输出
//			else if( F_UART_CMD_Output_CO == 1)
//			{
//				S_UART_DATA_CO_Ooutput();
//			}	

			#if 0
			S_UART_SEND_BYTE('\n');
			S_UART_SEND_Str("R_MODE_FG:");
			//_itoa(u16_co_count,10);
			S_UART_SEND_ASCII_Dec((R_MODE_FG&0xff00)>>8,R_MODE_FG&0x00ff);
			S_UART_SEND_BYTE('\r');	
			
			
			S_UART_SEND_BYTE('\n');
			S_UART_SEND_Str("FLAG_EOL_HUSH:");
			//_itoa(u16_co_count,10);
			S_UART_SEND_ASCII_Dec((FLAG_EOL_HUSH&0xff00)>>8,FLAG_EOL_HUSH&0x00ff);
			S_UART_SEND_BYTE('\r');	
			
			#endif	
			
			#if 0
			//防止中途拔掉串口产生异常
			if(F_DEBUG_OUTPUT)
			{	
				if((_pa0 == 0) && (_urxif != 1) )
				{
					_wdtc = 0;
					while(1);
				}
			}
			#endif	
		}	
}
/**************************************
*@neam:S_UART_SEND_HEAR
*@brief:上电输出
*@param[in]:
*@retval: 
*@notes:
**************************************/
//void S_UART_SEND_HEAR(void)
//{
//	//xxx
//	//S_UART_SEND_Str("Rst-14\r\0");		
//	//物料号
//	//S_UART_SEND_Str(">Model 2030-DCR\r\0");		
//	//版本号
//	S_UART_SEND_Str(">2030-DCR Ver 2.1.0\r\0");				
//	//ID
//	//S_UART_SEND_Str(">ID:0\r\0");	
//}
/**************************************
*@neam:S_UART_DATA_CO_Ooutput
*@brief:串口CO数据输出
*@param[in]:
*@retval: 
*@notes:1s一次
**************************************/
//void S_UART_DATA_CO_Ooutput(void)
//{
//	//OPA电压
////	S_UART_SEND_Str(ORDER_V);
////	S_UART_SEND_ASCII_Dec(R_CO_AD_DATA[1],R_CO_AD_DATA[0]);
////	S_UART_SEND_BYTE('\r');
//	
//	#if 1
//	S_UART_SEND_BYTE('\n');
//	S_UART_SEND_Str("V:");
//	S_UART_SEND_ASCII_Dec((average_counts&0xff00)>>8,average_counts&0x00ff);
//	//S_UART_SEND_BYTE('\r');
//	#endif
//	
//	//浓度
//	S_UART_SEND_Str(">C:\0");
//	
//	//正常模式 或 报警模式输出PPM值
//	if( (F_CO_ALARM == 1) || (F_NORMAL_M_INI == 1) )
//	{
//		//最大输出 999 PPM  是否需要做此限制？
////		if(wPPM >= 999)
////		{
////			S_UART_SEND_ASCII_Dec((999&0xff00)>>8,999&0x00ff);
////		}
////		else
//		{
//			S_UART_SEND_ASCII_Dec((wPPM&0xff00)>>8,wPPM&0x00ff);
//		}
//	}
//	//其他模式下则输出0
//	else
//	{
//		S_UART_SEND_ASCII_Dec(0,0);
//	}
//	if(F_CO_ALARM == 1)
//	{
//		S_UART_SEND_Str("*");	
//	}
//	//S_UART_SEND_BYTE('\r');		
//}


/**************************************
*@neam:Read_EEPROM
*@brief:串口命令读取EEPROM数据
*@param[in]:
*@retval: 
*@notes:
**************************************/
void Read_EEPROM(void)
{
	u16 i;

	S_UART_SEND_BYTE('\n');
	S_UART_SEND_Str("H:UU");
	for(i=0; i < 256; i++){
		if((i % 16) == 0){
			S_UART_SEND_BYTE('\n');
			S_UART_SEND_BYTE('\r');
			//_itoa(i,16);
			S_UART_SEND_ASCII_Hex(i);
			S_UART_SEND_Str(": ");
		}
		//_itoa(S_RD_EE(i),16);
		S_UART_SEND_ASCII_Hex(S_RD_EE(i));
		S_UART_SEND_BYTE(' ');
	}
	//S_UART_SEND_BYTE('\r');	
}