/*========================================================
*@File name:key.h
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#ifndef	_KEY_H__
#define _KEY_H__
#include "BA45F6750.h"
#include "common.h"

#define New_Key			1
#define Old_Key			0

#define Amber_Blink_Width	350
#define	Amber_Close_Delay	100

#define	CO_FAULT_CODE		5
#define EOL_CODE			2


#define	P_TEST_KEY		_pa4	// _pa3
/*#define	P_BD		_pa1*/

extern  u16 Key_Count;

//void S_KEY_INIT(void);
#if Old_Key
void S_Key_Scan(void);
#endif
#if New_Key
void Button_Scan(void);
#endif



#endif

