/*========================================================
*@File name:led.c
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#include "led.h"
#include "buzzer.h"
#include "sys_init.h"

/**************************************
*@neam:S_LED_FLASH_CTRL
*@brief:LED驱动  
*@param[in]:
*@retval: 
*@notes:
**************************************/
#if Old_Led
void S_LED_FLASH_CTRL(void)
{	
	#if 1
	static	u16	R_LED_CNT;
	static	u8	R_LED_SERIES;
	
	//LED使能
	if(F_LED_FLASH_EN == 1)
	{
		u8 R_LED_MODE_FG = 0;
		if(R_LED_STATE == 0x01)	R_LED_MODE_FG = 3;
		if(R_LED_STATE == 0x02)	R_LED_MODE_FG = 2;
		if(R_LED_STATE == 0x04)	R_LED_MODE_FG = 1;
		
		switch(R_LED_MODE_FG)
		{
			//打开LED
			case 3:
				if(F_LED_G == 1) P_LED_G=0;
				if(F_LED_R == 1) P_LED_R=0;
				if(F_LED_Y == 1) P_LED_Y=0;
				R_LED_CNT++;
				if(R_LED_CNT >= R_LEDON_CNTSET)	F_LED_SHIFT=1;		
			break;
			//===================================================
			//关闭LED
			case 2:
				if(F_LED_G ==1) P_LED_G=1;
				if(F_LED_R ==1) P_LED_R=1;	
				if(F_LED_Y ==1) P_LED_Y=1;			
				R_LED_CNT++;				
				if(R_LED_CNT >= R_LEDOFF_CNTSET)F_LED_SHIFT=1;		
					
					
			break;
			//===================================================
			//周期循环处理
			case 1:
				//单次处理
				if(F_LED_ONCE)						
				{
					F_LED_RESET = 1;
					R_LED_SERIES++;
					if(R_LED_SERIES >= R_LED_SERIESSET)	//不能放到括号里面加
						{
							F_LED_RESET=0;
							F_LED_STOP=1;
						}
				}
				//循环处理
				else									
				{
					F_LED_RESET = 1;
					R_LED_SERIES++;
					if(R_LED_SERIES>=R_LED_SERIESSET)	//不能放到括号里面加
						{
							F_LED_RESET = 0;
							R_LED_SERIES = 0;
							F_LED_ONEPERIOD = 1;		//完成一个短周期
							F_LED_SHIFT = 1;
						}				
				}			
			break;
			//===================================================
			//等待处理
			case 0:
				if(F_LED_G ==1) P_LED_G=1;
				if(F_LED_R ==1) P_LED_R=1;	
				if(F_LED_Y ==1) P_LED_Y=1;	
				F_LED_RESET=0;
				R_LED_CNT++;
				if(R_LED_CNT>=R_LEDWAIT_CNTSET)		F_LED_RESET=1;

				
			break;
			//===================================================
			//
			default:
			break;
			
		}
//===================================================		
		//复位处理		
		if(F_LED_RESET)							
		{
			F_LED_RESET = 0;
			R_LED_CNT = 0;
			//R_LED_STATE = 0;
			R_LED_STATE = 0x01;
		}
		
		//状态移位处理
		if(F_LED_SHIFT)							
		{
			F_LED_SHIFT = 0;
			R_LED_CNT = 0;
			R_LED_STATE <<= 1;	
		}
		//led停止工作处理
		if(F_LED_STOP)							
		{
			F_LED_STOP = 0;
			F_LED_EN = 0;
			R_LED_STATE = 0;
		}
	}
	else
	{
		R_LED_CNT = 0;
		R_LED_SERIES = 0;
	}
	#endif
	
}
#endif
/**************************************
*@neam:S_LED_STOPFLASH
*@brief:LED停止闪烁  
*@param[in]:
*@retval: 
*@notes:
**************************************/
void LED_STOPFLASH(void)
{
	P_LED_R = 1;
	P_LED_G = 1;
	P_LED_Y = 1;
	R_LED_FG.byte = 0;
}

/**************************************
*@neam:S_LED_FLASH_FUN
*@brief:闪灯设置  
*@param[in]:R_LED_ON;R_LED_OFF
*@retval: 
*@notes:
**************************************/
#if 0
void S_LED_FLASH_FUN(u8 R_LED_ON,u8 R_LED_OFF)
{
	F_LED_FLASH_EN = 1;
	R_RED_SET_ON = R_LED_ON;
	R_RED_SET_OFF = R_LED_OFF;
}
#endif

#if 0
void S_LED_FLASH_FUN(u8 R_LED_ON,u8 R_LED_OFF)
{
	static R_LED_FLASH_CNT;
	R_RED_SET_ON = R_LED_ON;
	R_RED_SET_OFF = R_LED_OFF;
    if (R_LED_FLASH_CNT = 0||(R_LED_FLASH_CNT > R_RED_SET_ON)||(R_LED_FLASH_CNT > R_RED_SET_OFF))  
	{
		if(F_LED_G ==1) ~P_LED_G;
	    if(F_LED_R ==1) ~P_LED_R;	
		if(F_LED_Y ==1) ~P_LED_Y;
        if (R_LED_FLASH_CNT > R_RED_SET_OFF)
        {
            R_LED_FLASH_CNT = 0;
        }
    }
    R_LED_FLASH_CNT++;
}
#endif

/**************************************
*@neam:S_YLED_FLASH
*@brief:黄灯闪烁，蜂鸣器鸣叫  
*@param[in]:cnt 黄灯闪烁、蜂鸣器鸣叫次数
*@retval: 
*@notes:
**************************************/
//void S_YLED_FLASH(u8 cnt)
//{
//	u8 i;
//	for(i=0;i<cnt;i++)
//	{
//		P_LED_Y = 0;
//		//_pton = 1;
//		_pa3 = 1;
//		GCC_DELAY(200000);
//		P_LED_Y = 1;
//		//_pton = 0;
//		_pa3 = 0;
//		GCC_DELAY(200000);
//	}
//}


/**************************************
*@neam:S_LED_ON
*@brief:控制LED按指定参数闪 
*@param[in]:V1，ON时长
	      	V2，OFF时长
			V3，一个周闪的次数
			V4，WAIT时长
			V5,	=1 闪一次，=0循环闪
*@retval: 
*@notes: 假设S_LED_CTRL调用周期为8ms，V1=10，V2=15，V3=2，V4=50
		 ON=80ms，OFF=120ms，WAIT=400ms
		 |ON|OFF|ON|OFF|WAIT|ON|OFF|ON|OFF|WAIT|
**************************************/

#if 1
void S_LED_ON(u8 R_LED_ON, u8 R_LED_OFF, u8 R_LED_CNT,u16 R_LED_WAIT,u8 R_LED_ONCE)
{
	if(F_TEST == 1)
	{
		S_Delay_ms(5);
	}
	R_LEDON_CNTSET  = R_LED_ON;
	R_LEDOFF_CNTSET = R_LED_OFF;
	R_LED_SERIESSET = R_LED_CNT;
	if(R_LED_ONCE == 0)
	{
		R_LEDWAIT_CNTSET = R_LED_WAIT;
		F_LED_ONCE = 0;
	}
	else
	{
		F_LED_ONCE = 1;
	}
	F_LED_FLASH_EN = 1;
	R_LED_STATE = 0X01;
}
#endif
/**************************************
*@neam:Blink_Led
*@brief: system 8ms time
*@param[in]:
*@retval: 
*@notes: v1 循环时间 V2 哪个灯亮  V3 亮几次，2为1次， v4 每次亮的时间间隔
**************************************/
#if New_Led
void Blink_Led(u16 Delay_Count,u8 Led_Set,u16 Blink_Count, u16 Blink_Wiat)
{
	static u16 	Blink_Time = 0;
	static u16  Cycle_Count = 0;
	static u16  Cycle_Wait = 0;
	static u8  R_LED_RG = 0;
	Blink_Time ++;
	if(Blink_Time >= Delay_Count)		//循环时间
	{
		#if 0
		Cycle_Wait++;
		if(Cycle_Wait >= Blink_Wiat)
		{
			Cycle_Wait = 0;
			if(Led_Set == 0)		//Green
			{
				_pa6 = ~_pa6;
			}else if(Led_Set == 1)	//Red
			{
				_pb2 = ~_pb2;
			}else if(Led_Set == 2)	//Amber
			{
				_pb3 = ~_pb3;
			}else if(Led_Set == 3)	//Green & Amber
			{
				_pa6 = ~_pa6;
				_pb3 = ~_pb3;
			}
			Cycle_Count ++;
			
			if(Cycle_Count >= Blink_Count)		//亮灯次数
			{
				Blink_Time = 0;
				Cycle_Count = 0;
			}
		}
	#endif

	#if 1
		switch (R_LED_RG)
		{
			case 0:
				if(Led_Set == 0)		//Green
				{
					_pa6 = 0;
				}else if(Led_Set == 1)	//Red
				{
					_pb2 = 0;
				}else if(Led_Set == 2)	//Amber
				{
					_pb3 = 0;
				}else if(Led_Set == 3)	//Green & Amber
				{
					_pa6 = 0;
					_pb3 =	0;
				}
				Cycle_Wait++;
				if(Cycle_Wait >= 12)		//Blink is 100ms long
				{
					Cycle_Wait = 0;
					R_LED_RG = 1;
				}
			break;	
			
			case 1:
				if(Led_Set == 0)		//Green
				{
					_pa6 = 1;
				}else if(Led_Set == 1)	//Red
				{
					_pb2 = 1;
				}else if(Led_Set == 2)	//Amber
				{
					_pb3 = 1;
				}else if(Led_Set == 3)	//Green & Amber
				{
					_pa6 = 1;
					_pb3 =1;
				}
				Cycle_Wait++;
				if(Cycle_Wait >= 37)		//Spacing between blinks is 300ms
				{
					Cycle_Wait = 0;
					Cycle_Count ++;
					if(Cycle_Count >= Blink_Count)		//亮灯次数
					{
						Blink_Time = 0;
						Cycle_Count = 0;
						R_LED_RG = 2;
						_pa6 = 1;
						_pb3 = 1;
						_pb2 = 1;
					}
					else
					{
						R_LED_RG = 0;	
					}
				}
			break;
			
			default:
			break;
		}
		#endif
	}
	
}

#endif



/**************************************
*@neam:LED_Buzzer_ON
*@brief:控制LED buzzer按指定参数闪 
*@param[in]:V1，ON时长
	      	V2，OFF时长
			V3，一个周闪的次数
			V4，WAIT时长
			V5,	=1 闪，响一次，=0循环闪,响
*@retval: 
*@notes: 假设S_LED_BUZZER_CTRL调用周期为8ms，V1=10，V2=15，V3=2，V4=50
		 ON=80ms，OFF=120ms，WAIT=400ms
		 |ON|OFF|ON|OFF|WAIT|ON|OFF|ON|OFF|WAIT|
		 set: 0 is LED , 1 is Buzzer
**************************************/
#if 0
void LED_BUZZER_ON(u8 Set,u8 R_ON, u8 R_OFF, u8 R_CNT,u16 R_WAIT,u8 R_ONCE)
{
	if(F_TEST == 1)
	{
			S_Delay_ms(5);
	}
	if(Set)
	{
		P_BUZZ_EN = 0;
		R_BUZZON_CNTSET  = R_ON;
		R_BUZZOFF_CNTSET = R_OFF;
		R_BUZZ_SERIESSET = R_CNT;
		if(R_ONCE == 0)
		{
			R_BUZZWAIT_CNTSET = R_WAIT;
			F_BUZZER_ONCE = 0;
		}
		else
		{
			F_BUZZER_ONCE = 1;
		}
		F_BUZZER_EN = 1;
		R_BUZZER_STATE = 0X01;
	}
	else
	{
		R_LEDON_CNTSET  = R_ON;
		R_LEDOFF_CNTSET = R_OFF;
		R_LED_SERIESSET = R_CNT;
		if(R_ONCE == 0)
		{
			R_LEDWAIT_CNTSET = R_WAIT;
			F_LED_ONCE = 0;
		}
		else
		{
			F_LED_ONCE = 1;
		}
		F_LED_FLASH_EN = 1;
		R_LED_STATE = 0X01;	
	}
}
#endif



/**************************************
*@neam:
*@brief:
*@param[in]:
*@retval: 
*@notes:
**************************************/
void Amber_Blink(u16 Delay_ms)
{
	_pb3 = 0;
	S_Delay_ms(Delay_ms);
	_pb3 = 1;	
}