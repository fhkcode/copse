/*========================================================
*@File name:timer.c
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#include "timer.h"
#include "sys_init.h"
#include "eeprom.h"
#include "sys_function.h"

unsigned char R_Clock_count;
unsigned char R_Clock_Differ;
unsigned char R_Clock_valsum;
unsigned char R_Clock_sec;
unsigned char R_Clock_min;
unsigned char R_Clock_hour;

unsigned int  R_Clock_HIRC_cout;
unsigned char R_Clock_HIRC_sec;
unsigned char R_Clock_HIRC_min;
unsigned char R_Clock_HIRC_hour;

/**************************************
*@neam:S_TB1_INIT
*@brief: TB1 初始化
*@param[in]:
*@retval: 
*@notes:
**************************************/
//void S_TB1_INIT(void)
//{
//	//假设校准时钟为32760，则为125ms 周期
//	_tb1c = 0b00000100;		// 4096/fpcs
//	_tb1e = 1;
//}

/**************************************
*@neam:S_STM_INIT
*@brief: STM初始化
*@param[in]:
*@retval: 
*@notes:
**************************************/
//void S_STM_INIT(void)
//{
//	_stmc0 = 0b00100000;	//fH/16
//	_stmc1 = 0b11000001;	//定时/计数模式，A匹配
//	
//	//(1/fstm)*x
//	//fstm = 8000000/16=500000
//	//1/500000*500= 1ms
//	_stmal = 500 & 0x0ff;
//	_stmah = 500 >> 8;
//	
//	//STM中断使能
//	_stmae = 1;
//}

/**********************************************************
*Function:使能LIRC校准
*Description: 一个周期后自动关闭	
*Return: 	None
***********************************************************/
void S_Clock_CALC_Enable(void)
{
	//S_TB1_INIT();
	//假设校准时钟为32760，则为125ms 周期
	_tb1c = 0b00000100;		// 4096/fpcs
	_tb1e = 1;
	R_Clock_CALC_FG.byte = 0;
	F_Clock_CALC_FG = 1;
	R_Clock_count = 0;
	_tb1on = 1;	
}

/**********************************************************
*Function:S_Clock_CALC
*Description: S_Clock_CALC	
*Return: 	None
***********************************************************/
void S_Clock_CALC(void)
{
	R_Clock_valsum = R_Clock_valsum + R_Clock_Differ;
	
	if(R_Clock_valsum >= 125)
	{
		R_Clock_valsum = R_Clock_valsum - 125;
		
		if(F_Clock_CALC_MAX == 1)
		{
			R_Clock_sec ++;
		}
		else if (F_Clock_CALC_MIN == 1)
		{
			R_Clock_sec --;
		}
	} 
	//clock count
	S_Clock_Count();
}	

/**********************************************************
*Function:S_Clock_Count
*Description: S_Clock_Count	
*Return: 	None
***********************************************************/
void S_Clock_Count(void)
{
	R_Clock_sec++;
	if(R_Clock_sec >= 60)
	{
		R_Clock_sec = R_Clock_sec - 60;
		R_Clock_min ++ ;
		F_CAL_Onemin = 1;
		
		if( R_Clock_min >= 60)  //60  1440
		{
			R_Clock_min = 0;
			F_CAL_OneHour = 1;
			
			R_Clock_hour ++;
			if( R_Clock_hour >= 24)
			{
				R_Clock_hour = 0;
				DAY++;
				FLAG_HISTORY_UPDATA = 1;
			}	
		}
	}
}

/**********************************************************
*Function:S_TB1_INTERRUPT
*Description: S_TB1_INTERRUPT	
*Return: 	None
***********************************************************/
void S_TB1_INTERRUPT(void)
{
	if(F_Clock_CALC_FG == 1)
	{
		if(_ston == 1)
		{
			_ston = 0;
			_tb1on = 0;
			F_Clock_CALC_EN = 1;
			if(R_Clock_count == 125)
			{
				F_Clock_CALC_EQU = 1;
				R_Clock_Differ = 0;
			}
			else if(R_Clock_count > 125)
			{
				F_Clock_CALC_MAX = 1;
				R_Clock_Differ = R_Clock_count - 125;
			}
			else
			{
				F_Clock_CALC_MIN = 1;
				R_Clock_Differ = 125 - R_Clock_count;
			}
			
			F_Clock_CALC_FG = 0;
		}
		else
		{
			_ston = 1;	
		}
	}
}

/**********************************************************
*Function:S_STM_INTERRUPT
*Description: S_STM_INTERRUPT	
*Return: 	None
***********************************************************/
//void S_STM_INTERRUPT(void)
//{
//	if( F_Clock_CALC_FG == 1)	
//	{
//		R_Clock_count ++;
//	}	
	//使用STM计数，仅测试使用			
//	else
//	{
//		//1ms * 1000 = 1s
//		//R_Clock_HIRC_cout ++;
//		if(R_Clock_HIRC_cout++ >= 1000)
//		{
//			R_Clock_HIRC_cout = 0;
//			//R_Clock_HIRC_sec ++;
//			if( R_Clock_HIRC_sec++ >= 60)
//			{
//				R_Clock_HIRC_sec = 0;
//				//R_Clock_HIRC_min ++;
//				if(R_Clock_HIRC_min++ >= 60)
//				{
//					R_Clock_HIRC_min = 0;
//					//R_Clock_HIRC_hour ++;
//					if(R_Clock_HIRC_hour++ >= 24)
//					{
//						R_Clock_HIRC_hour = 0;	
//					}	
//				}
//			}	
//		}
//	}
//}







