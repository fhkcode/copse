/*========================================================
*@File name:uart.h
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#ifndef	_UART_H__
#define _UART_H__

#include "BA45F6750.h"
#include "common.h"

//UART接收缓存大小   接收处设置为32  如果大小不一样 有什么问题？
#define	C_UART_RX_BUFFSIZE 10	


void S_UART_INIT(void);
void S_UART_SEND_BYTE(unsigned char data);
void S_UART_INTERRUPT(void);
void S_UART_PTRESET(void);
signed char S_UART_FindByte(unsigned char dat);
void S_UART_SEND_Str(const char *data);
void S_UART_SEND_ASCII_Dec(unsigned char data_H,unsigned char data_L);
void S_UART_SEND_ASCII_Hex(unsigned char data);
unsigned char S_UART_ASCII_TO_HEX(unsigned char data1,unsigned char data2);
//void _itoa(unsigned int number, unsigned char radix);
//unsigned char Uart_Ascii_To_Hex(unsigned char data1,unsigned char data2);

extern unsigned char R_UART_BUFF[C_UART_RX_BUFFSIZE];
extern unsigned char R_UART_BUFFSIZE;
extern unsigned char R_UART_WAIT_CNT;
#endif