/*========================================================
*@File name:eerpom.c
*@Function:
*@Author:
*@Edition:
*@Date:
*@Remarks:
========================================================*/
#include "eeprom.h"

/**************************************
*@neam:S_RD_EE
*@brief:read EE
*@param[in]:RD_EE_addr
*@retval: EE_DATA
*@notes:
**************************************/
unsigned char S_RD_EE(unsigned char RD_EE_addr)
{
	//if(_lvrf == 0)//(_lvdo == 0) && 
	{
		u8  i;
		_emi=0;
		_eea=RD_EE_addr;
		_mp1l=0x40;
		_mp1h=0x01;
		_iar1=_iar1|0x03;
	//	_iar1.1=1;
	//	_iar1.0=1;
		while(_iar1&0x01);
		_iar1=0x00;
		_mp1h=0x00;
		i=_eed;
		_emi=1;
		return i;
	}	
}

/**************************************
*@neam:S_WR_EE
*@brief:write EE
*@param[in]:WR_EE_addr
*@retval: WR_EE_data
*@notes:
**************************************/
void S_WR_EE(unsigned char WR_EE_addr,unsigned char WR_EE_data)
{
	//if(_lvrf == 0)//(_lvdo == 0) && 
	{
		_emi=0;
		_eea=WR_EE_addr;
		_eed=WR_EE_data;
		_mp1l=0x40;
		_mp1h=0x01;
		_iar1=_iar1|0b00001000;
		_iar1=_iar1|0b00000100;
		while(_iar1&0b00000100) ;
		_iar1=0x00;
		_mp1h=0x00;
		_emi=1;
	}
}

/**************************************
*@neam:save check sum
*@brief:write check sum to EE
*@param[in]:
*@retval: 
*@notes:
**************************************/
void Save_CheckSum(u16 sum)
{
	S_WR_EE(EEP_Checksum_Address,sum);
	S_WR_EE(EEP_Checksum_Address+0x80,sum);		
}