; Generated by holtek-gcc v3.19, Thu Feb 10 14:53:43 2022
; 
; Configuration:
;       with long instruction
;       Single ROM, Multi-RAM
;       bits_per_rom_unit:16
;       with mp1
;       with tbhp, address(0x9)
;          Use tabrd-const
;       
; SFR address of long-instruction arch:
;    mp0 = -1,1,0
;    mp1 = 4,3,2
;    mp2 = 14,13,12
;    tbp = 9,7,8
;    acc = 5
;    pcl = 6
;    status = 10
;    bp = 11
;    intc = 73
;       
;       
; use 'tabrdc' instead of 'tabrd'
;       

#pragma translator "holtek-gcc 4.6.4" "3.19" "build 20130711"
; Rebuild 718

ir equ [2]
mp equ [3]
sbp equ [4]
acc equ [5]
bp equ [11]
tblp equ [7]
tbhp equ [9]
status equ [10]
c equ [10].0
ac equ [10].1
z equ [10].2
ov equ [10].3
cz equ [10].6
sc equ [10].7
intc equ [73]

extern ra:byte
extern rb:byte
extern rc:byte
extern rd:byte
extern re:byte
extern rf:byte
extern rg:byte
extern rh:byte
extern _Crom2Prom:near
extern _Crom2PromNext:near

RAMBANK 0 @BITDATASEC, @BITDATASEC1
@HCCINIT	.section 'data'
@HCCINIT0	.section 'data'
@BITDATASEC	 .section 'data'
@BITDATASEC1	 .section 'data'

#pragma debug scope 1 1
	extern __DELAY3:near
	extern __DELAYX3:near
	extern __DELAYX6:near
	extern __DELAYY5:near
	extern __DELAYY3:near
	extern _builtin_holtek_delay_2:byte
___pa equ [20]
___opsw0 equ [102]
___wdtc equ [61]
@crom	.section 'crom'
_LC0:
	db 04dh,065h,06dh,06fh,072h,079h,020h,046h,061h,075h
	db 06ch,074h,00h
@crom	.section 'crom'
_LC1:
	db 045h,04fh,04ch,020h,048h,055h,053h,048h,00h
@crom	.section 'crom'
_LC2:
	db 04ch,042h,020h,046h,041h,054h,041h,04ch,00h
@crom	.section 'crom'
_LC3:
	db 04ch,042h,020h,048h,055h,053h,048h,00h
@crom	.section 'crom'
_LC4:
	db 045h,058h,049h,054h,020h,041h,06ch,061h,072h,06dh
	db 020h,04dh,065h,06dh,06fh,072h,079h,00h
@crom	.section 'crom'
_LC5:
	db 045h,058h,049h,054h,020h,04ch,042h,020h,048h,055h
	db 053h,048h,00h
@crom	.section 'crom'
_LC6:
	db 045h,058h,049h,054h,020h,045h,04fh,04ch,020h,048h
	db 055h,053h,048h,00h
public _Button_Scan
#pragma debug scope 2 1
#line 122 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
_Button_Scan .section 'code'
_Button_Scan proc
    local _Button_Scan_2 db 4 dup(?)	; 0,4
#line 129 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	sz [20].4
	jmp _L2
#line 131 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	sz _R_SYS_FG2[0].5
	jmp _L3
	sz _R_SYS_FG3[0].2
	jmp _L3
	sz _R_SYS_FG[0].2
	jmp _L3
	sz _F_RUN_FLAG[0].5
	jmp _L3
#line 132 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_Sys_Cal_State[0]
	sub a,5
	snz z
	jmp _L3
#line 134 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	inc _Key_Count[0]
	sz z
	inc _Key_Count[1]
_L3:
#line 138 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_Key_Count[0]
	mov rb,a
	mov a,_Key_Count[1]
	mov rc,a
	mov a,231
	sub a,rb
	mov a,3
	sbc a,rc
	sz c
	jmp _L5
#line 140 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	set _F_RUN_FLAG[0].7
#line 141 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,rb
	sub a,232
	mov a,rc
	sbc a,3
	snz cz
	jmp _L5
#line 144 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_INI_FG[0].0
#line 145 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_SYS_FG[0].2
#line 146 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_SYS_FG2[0].5
	jmp _L5
_L2:
#line 152 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,11
	sub a,_Key_Count[0]
	mov a,0
	sbc a,_Key_Count[1]
	sz c
	jmp _L7
#line 154 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _R_CO_RUNFLAG[0].7
	jmp _L8
#line 156 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr [102].2
#line 157 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr [102].1
#line 158 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,-72
	mov _S_Delay_ms_2[0],a
	mov a,11
	mov _S_Delay_ms_2[1],a
	call _S_Delay_ms
#line 159 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr [61]
_L9:
	jmp _L9
_L8:
#line 162 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _R_SYS_FG2[0].4
	jmp _L10
#line 164 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr [102].2
#line 165 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr [102].1
#line 166 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,-72
	mov _S_Delay_ms_2[0],a
	mov a,11
	mov _S_Delay_ms_2[1],a
	call _S_Delay_ms
#line 167 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr [61]
_L11:
	jmp _L11
_L10:
#line 170 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	sz _F_RUN_FLAG[0].5
	jmp _L12
	snz _F_MEMORY_FLAG[0].0
	jmp _L13
_L12:
#line 173 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,10
	call _S_UART_SEND_BYTE
#line 174 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,low offset _LC0
	mov _S_UART_SEND_Str_2[0],a
	mov a,high offset _LC0
	mov _S_UART_SEND_Str_2[1],a
	jmp _L57
_L13:
#line 178 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _R_CO_RUNFLAG[0].6
	jmp _L14
	mov a,5
	mov _Button_Scan_2[0],a
_L16:
#pragma debug scope 3 2
;begin block, line: 178.-1
#line 183 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,94
	mov _Amber_Blink_2[0],a
	mov a,1
	mov _Amber_Blink_2[1],a
	call _Amber_Blink
#line 184 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,100
	mov _S_Delay_ms_2[0],a
	clr _S_Delay_ms_2[1]
	call _S_Delay_ms
#line 185 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
;# 185 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c" 1
	clr wdt
	sdz _Button_Scan_2[0]
	jmp _L16
#line 191 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,-24
	mov _S_Delay_ms_2[0],a
	mov a,3
	mov _S_Delay_ms_2[1],a
	call _S_Delay_ms
#line 192 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr [61]
_L17:
	jmp _L17
_L14:
#pragma debug scope 2 1
#line 195 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _F_RUN_FLAG[0].7
	jmp _L18
#line 197 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_SYS_FG2[0].5
#line 198 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _F_RUN_FLAG[0].7
#line 199 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_BUZZ_FG[0].3
	jmp _L7
_L18:
#line 201 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _R_SYS_FG[0].7
	jmp _L19
	sz _R_SYS_FG2[0].3
	jmp _L19
#pragma debug scope 4 2
;begin block, line: 201.-1
#line 203 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	set _R_SYS_FG2[0].3
#line 204 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	set _R_INI_FG[0].7
#line 205 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_SYS_FG[0].7
#line 209 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,94
	mov _Amber_Blink_2[0],a
	mov a,1
	mov _Amber_Blink_2[1],a
	call _Amber_Blink
#line 210 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,100
	mov _S_Delay_ms_2[0],a
	clr _S_Delay_ms_2[1]
	call _S_Delay_ms
#line 211 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
;# 211 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c" 1
	clr wdt
#line 209 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,94
	mov _Amber_Blink_2[0],a
	mov a,1
	mov _Amber_Blink_2[1],a
	call _Amber_Blink
#line 210 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,100
	mov _S_Delay_ms_2[0],a
	clr _S_Delay_ms_2[1]
	call _S_Delay_ms
#line 211 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
;# 211 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c" 1
	clr wdt
#line 215 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,10
	call _S_UART_SEND_BYTE
#line 216 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,low offset _LC1
	mov _S_UART_SEND_Str_2[0],a
	mov a,high offset _LC1
	mov _S_UART_SEND_Str_2[1],a
	call _S_UART_SEND_Str
#line 217 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,13
	call _S_UART_SEND_BYTE
#line 220 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,-72
	mov _S_Delay_ms_2[0],a
	mov a,11
	mov _S_Delay_ms_2[1],a
	call _S_Delay_ms
#line 221 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_INI_FG1[0].5
#pragma debug scope 2 1
#line 202 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	jmp _L7
_L19:
#line 224 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _F_RUN_FLAG[0].3
	jmp _L20
#line 226 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,94
	mov _Amber_Blink_2[0],a
	mov a,1
	mov _Amber_Blink_2[1],a
	call _Amber_Blink
#line 229 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,10
	call _S_UART_SEND_BYTE
#line 230 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,low offset _LC2
	mov _S_UART_SEND_Str_2[0],a
	mov a,high offset _LC2
	mov _S_UART_SEND_Str_2[1],a
_L57:
	call _S_UART_SEND_Str
#line 231 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,13
	call _S_UART_SEND_BYTE
	jmp _L7
_L20:
#line 234 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _F_BAT_ISLOW
	jmp _L21
	sz _R_SYS_FG2[0].7
	jmp _L21
#line 236 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,94
	mov _Amber_Blink_2[0],a
	mov a,1
	mov _Amber_Blink_2[1],a
	call _Amber_Blink
#line 237 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	set _R_SYS_FG2[0].7
#line 238 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_INI_FG1[0].6
#line 239 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _F_BAT_ISLOW
#line 242 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,10
	call _S_UART_SEND_BYTE
#line 243 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,low offset _LC3
	mov _S_UART_SEND_Str_2[0],a
	mov a,high offset _LC3
	mov _S_UART_SEND_Str_2[1],a
	jmp _L57
_L21:
#line 247 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _F_RUN_FLAG[0].0
	jmp _L22
#pragma debug scope 5 2
;begin block, line: 247.-1
#line 249 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _F_RUN_FLAG[0].0
#line 250 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_SYS_FG[0].2
#line 251 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_SYS_FG2[0].5
#line 252 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_INI_FG[0]
#line 253 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_INI_FG1[0]
#line 254 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_BUZZ_FG[0].3
#line 258 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_DAY[0]
	mov _S_WR_EE_2[0],a
	mov a,58
	call _S_WR_EE
#line 259 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_DAY[1]
	mov _S_WR_EE_2[0],a
	mov a,59
	call _S_WR_EE
#line 262 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,60
	call _S_RD_EE
	mov ra,a
#line 263 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	siza ra
	jmp _L23
	clr ra
_L23:
#line 265 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	inca ra
	mov _S_WR_EE_2[0],a
	mov a,60
	call _S_WR_EE
#line 270 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,10
	call _S_UART_SEND_BYTE
#line 271 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,low offset _LC4
	mov _S_UART_SEND_Str_2[0],a
	mov a,high offset _LC4
	mov _S_UART_SEND_Str_2[1],a
	jmp _L57
_L22:
#pragma debug scope 2 1
#line 275 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _R_SYS_FG2[0].7
	jmp _L24
#line 277 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_BUZZ_FG[0].3
#line 278 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_SYS_FG2[0].7
#line 279 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	set _R_SYS_FG2[0].5
#line 280 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_INI_FG[0].0
#line 283 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,10
	call _S_UART_SEND_BYTE
#line 284 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,low offset _LC5
	mov _S_UART_SEND_Str_2[0],a
	mov a,high offset _LC5
	mov _S_UART_SEND_Str_2[1],a
	jmp _L57
_L24:
#line 288 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _R_SYS_FG2[0].3
	jmp _L25
#pragma debug scope 6 2
;begin block, line: 288.-1
#line 290 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_SYS_FG2[0].3
#line 294 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,94
	mov _Amber_Blink_2[0],a
	mov a,1
	mov _Amber_Blink_2[1],a
	call _Amber_Blink
#line 295 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,100
	mov _S_Delay_ms_2[0],a
	clr _S_Delay_ms_2[1]
	call _S_Delay_ms
#line 296 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
;# 296 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c" 1
	clr wdt
#line 294 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,94
	mov _Amber_Blink_2[0],a
	mov a,1
	mov _Amber_Blink_2[1],a
	call _Amber_Blink
#line 295 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,100
	mov _S_Delay_ms_2[0],a
	clr _S_Delay_ms_2[1]
	call _S_Delay_ms
#line 296 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
;# 296 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c" 1
	clr wdt
#line 300 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,10
	call _S_UART_SEND_BYTE
#line 301 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,low offset _LC6
	mov _S_UART_SEND_Str_2[0],a
	mov a,high offset _LC6
	mov _S_UART_SEND_Str_2[1],a
	call _S_UART_SEND_Str
#line 302 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,13
	call _S_UART_SEND_BYTE
#line 305 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,-72
	mov _S_Delay_ms_2[0],a
	mov a,11
	mov _S_Delay_ms_2[1],a
	call _S_Delay_ms
#line 306 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	set _R_SYS_FG[0].7
#line 307 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_INI_FG[0].7
#pragma debug scope 2 1
	jmp _L7
_L25:
#line 312 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	sz _R_SYS_FG2[0].5
	jmp _L7
#line 314 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	set _R_SYS_FG2[0].5
#line 316 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_BUZZON_CNTSET[0]
	clr _R_BUZZON_CNTSET[1]
#line 317 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_BUZZOFF_CNTSET[0]
	clr _R_BUZZOFF_CNTSET[1]
#line 318 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_BUZZ_SERIESSET[0]
	clr _R_BUZZ_SERIESSET[1]
#line 319 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_BUZZWAIT_CNTSET[0]
	clr _R_BUZZWAIT_CNTSET[1]
#line 320 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_BUZZ_FG[0].3
#line 321 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr ___stopcnt_2406[0]
#line 324 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	set [102].2
#line 326 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,5
	mov _S_Delay_ms_2[0],a
	clr _S_Delay_ms_2[1]
	call _S_Delay_ms
#line 328 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	call _Opamp_Calibration
#line 329 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr [102].2
_L7:
#line 333 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _Key_Count[0]
	clr _Key_Count[1]
#line 334 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _F_RUN_FLAG[0].7
_L5:
#line 338 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _R_SYS_FG2[0].5
	jmp _L26
#pragma debug scope 7 2
;begin block, line: 338.-1
#pragma debug variable 12 7 _Button_Scan_2+2 "key_adc"
#line 340 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_SYS_FG2[0].5
	mov a,-12
	mov _Button_Scan_2[0],a
	mov a,1
	mov _Button_Scan_2[1],a
_L29:
#line 381 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
;# 381 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c" 1
	clr wdt
#line 382 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	call _Read_CO_Value
	mov a,ra
	mov _Button_Scan_2[2],a
	mov a,rb
	mov _Button_Scan_2[3],a
#line 383 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,24
	sub a,ra
	mov a,0
	sbc a,rb
	snz c
	jmp _L27
#line 387 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	call _MEAS_Send_Charge_Pulse
_L27:
	mov a,255
	addm a,_Button_Scan_2[0]
	mov a,255
	adcm a,_Button_Scan_2[1]
#line 378 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_Button_Scan_2[0]
	or a,_Button_Scan_2[1]
	snz z
	jmp _L29
#line 391 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,20
	mov ___PTT_Count_2408[0],a
#line 412 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,24
	sub a,_Button_Scan_2[2]
	mov a,0
	sbc a,_Button_Scan_2[3]
	snz c
	jmp _L30
	snz _R_SYS_FG3[0].6
	jmp _L31
_L30:
#line 415 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	set _R_SYS_FG3[0].2
#line 416 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _F_RUN_FLAG[0].6
#line 417 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr ___PTT_Wait_Count_2407[0]
	clr ___PTT_Wait_Count_2407[1]
	jmp _L26
_L31:
#line 421 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,19
	mov ___PTT_Count_2408[0],a
#line 432 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	call _MEAS_Send_Charge_Pulse
_L26:
#pragma debug scope 2 1
#line 437 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _R_SYS_FG3[0].2
	jmp _L33
#line 439 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,1
	add a,___PTT_Wait_Count_2407[0]
	mov rb,a
	mov a,0
	adc a,___PTT_Wait_Count_2407[1]
	mov rc,a
	mov a,rb
	mov ___PTT_Wait_Count_2407[0],a
	mov a,rc
	mov ___PTT_Wait_Count_2407[1],a
#line 440 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,3
	sub a,rb
	mov a,0
	sbc a,rc
	snz c
	jmp _L34
#line 442 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr [20].5
	jmp _L33
_L34:
#line 459 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,249
	sub a,rb
	mov a,0
	sbc a,rc
	sz c
	jmp _L33
#line 461 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr ___PTT_Wait_Count_2407[0]
	clr ___PTT_Wait_Count_2407[1]
#line 462 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_SYS_FG3[0].2
#line 463 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	set _R_SYS_FG[0].2
_L33:
#line 467 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _R_SYS_FG[0].2
	jmp _L1
#line 469 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _R_BUZZ_FG[0].3
	jmp _L1
#line 471 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_BUZZ_FG[0].3
#line 473 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	inca ___stopcnt_2406[0]
	mov ra,a
	mov a,ra
	mov ___stopcnt_2406[0],a
#line 474 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,1
	sub a,ra
	sz c
	jmp _L1
#pragma debug scope 8 2
;begin block, line: 474.-1
#line 477 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr _R_SYS_FG[0].2
#line 478 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr ___stopcnt_2406[0]
#line 481 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_DAY[0]
	mov _S_WR_EE_2[0],a
	mov a,45
	call _S_WR_EE
#line 482 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_DAY[1]
	mov _S_WR_EE_2[0],a
	mov a,46
	call _S_WR_EE
#line 485 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,47
	call _S_RD_EE
	mov ra,a
#line 486 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	siza ra
	jmp _L37
	clr ra
_L37:
#line 488 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	inca ra
	mov _S_WR_EE_2[0],a
	mov a,47
	call _S_WR_EE
#line 490 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	snz _R_SYS_FG2[0].0
	jmp _L38
#line 492 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,1
	mov _S_WR_EE_2[0],a
	mov a,21
	call _S_WR_EE
#line 493 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_w1AlarmPastSum[0]
	mov _S_WR_EE_2[0],a
	mov a,22
	call _S_WR_EE
#line 494 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_w1AlarmPastSum[1]
	mov _S_WR_EE_2[0],a
	mov a,24
	call _S_WR_EE
#line 495 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_w1AlarmInterimSum[0]
	mov _S_WR_EE_2[0],a
	mov a,24
	call _S_WR_EE
#line 496 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_w1AlarmInterimSum[1]
	mov _S_WR_EE_2[0],a
	mov a,26
	call _S_WR_EE
#line 497 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_wAlarmSum[0]
	mov _S_WR_EE_2[0],a
	mov a,26
	call _S_WR_EE
#line 498 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,_wAlarmSum[1]
	mov _S_WR_EE_2[0],a
	mov a,28
	call _S_WR_EE
_L38:
#line 506 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	mov a,-48
	mov _S_Delay_ms_2[0],a
	mov a,7
	mov _S_Delay_ms_2[1],a
	call _S_Delay_ms
#line 507 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
	clr [61]
_L39:
	jmp _L39
_L1:
#pragma debug scope 2 1
	ret
_Button_Scan endp
#line 507 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
#pragma debug scope 1 1
@HCCINIT	.section 'data'
#line 124 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
@HCCINIT___PTT_Wait_Count_2407 .section 'data'
___PTT_Wait_Count_2407 label byte
#pragma debug variable 12 2 ___PTT_Wait_Count_2407 "PTT_Wait_Count"
	db 0
@ROMDATA_BASE .section inpage 'code'
	db 0
@HCCINIT___PTT_Wait_Count_2407 .section 'data'
	db 0
@ROMDATA_BASE .section inpage 'code'
	db 0
@HCCINIT___PTT_Wait_Count_2407 .section 'data'
@HCCINIT0___PTT_Count_2408 .section 'data'
___PTT_Count_2408	db 0
#pragma debug variable 8 2 ___PTT_Count_2408 "PTT_Count"

@HCCINIT .section 'data'
#line 123 "D:\Porject\CO\Code\Copse Plan A\Procject_2030_6750\C\key.c"
@HCCINIT___stopcnt_2406 .section 'data'
___stopcnt_2406 label byte
#pragma debug variable 8 2 ___stopcnt_2406 "stopcnt"
	db 0
@ROMDATA_BASE .section inpage 'code'
	db 0
@HCCINIT___stopcnt_2406 .section 'data'
#pragma debug struct_begin 42 "__pa_bits"
#pragma debug field 21 8 0 1 "__pa0" ;0,1
#pragma debug field 21 8 0 1 "__pa1" ;1,1
#pragma debug field 21 8 0 1 "__pa2" ;2,1
#pragma debug field 21 8 0 1 "__pa3" ;3,1
#pragma debug field 21 8 0 1 "__pa4" ;4,1
#pragma debug field 21 8 0 1 "__pa5" ;5,1
#pragma debug field 21 8 0 1 "__pa6" ;6,1
#pragma debug field 21 8 0 1 "__pa7" ;7,1
#pragma debug struct_end
#pragma debug union_begin 43 ""
#pragma debug field 2 42 "bits"
#pragma debug field 0 8 "byte"
#pragma debug union_end
#pragma debug variable 43 1 ___pa "__pa" 1
#pragma debug struct_begin 44 "__wdtc_bits"
#pragma debug field 21 8 0 1 "__ws0" ;0,1
#pragma debug field 21 8 0 1 "__ws1" ;1,1
#pragma debug field 21 8 0 1 "__ws2" ;2,1
#pragma debug field 21 8 0 1 "__we0" ;3,1
#pragma debug field 21 8 0 1 "__we1" ;4,1
#pragma debug field 21 8 0 1 "__we2" ;5,1
#pragma debug field 21 8 0 1 "__we3" ;6,1
#pragma debug field 21 8 0 1 "__we4" ;7,1
#pragma debug struct_end
#pragma debug union_begin 45 ""
#pragma debug field 2 44 "bits"
#pragma debug field 0 8 "byte"
#pragma debug union_end
#pragma debug variable 45 1 ___wdtc "__wdtc" 1
#pragma debug struct_begin 46 "__opsw0_bits"
#pragma debug field 21 8 0 1 "__ops0" ;0,1
#pragma debug field 21 8 0 1 "__ops1" ;1,1
#pragma debug field 21 8 0 1 "__ops2" ;2,1
#pragma debug field 21 8 0 1 "__ops3" ;3,1
#pragma debug field 21 8 0 1 "__ops4" ;4,1
#pragma debug field 21 8 0 1 "__ops5" ;5,1
#pragma debug field 21 8 0 1 "__ops6" ;6,1
#pragma debug field 21 8 0 1 "__ops7" ;7,1
#pragma debug struct_end
#pragma debug union_begin 47 ""
#pragma debug field 2 46 "bits"
#pragma debug field 0 8 "byte"
#pragma debug union_end
#pragma debug variable 47 1 ___opsw0 "__opsw0" 1

; output external variables
extern _wAlarmSum:byte
extern _w1AlarmInterimSum:byte
extern _w1AlarmPastSum:byte
extern _R_BUZZWAIT_CNTSET:byte
extern _R_BUZZ_SERIESSET:byte
extern _R_BUZZOFF_CNTSET:byte
extern _R_BUZZON_CNTSET:byte
extern _DAY:byte
extern _F_BAT_ISLOW:bit
extern _R_INI_FG1:byte
extern _R_BUZZ_FG:byte
extern _F_MEMORY_FLAG:byte
extern _R_CO_RUNFLAG:byte
extern _R_INI_FG:byte
extern _Key_Count:byte
extern _Sys_Cal_State:byte
extern _F_RUN_FLAG:byte
extern _R_SYS_FG:byte
extern _R_SYS_FG3:byte
extern _R_SYS_FG2:byte
extern _S_Delay_ms_2:byte
extern _S_Delay_ms:near
extern _S_UART_SEND_BYTE:near
extern _S_UART_SEND_Str_2:byte
extern _S_UART_SEND_Str:near
extern _Amber_Blink_2:byte
extern _Amber_Blink:near
extern _S_WR_EE_2:byte
extern _S_WR_EE:near
extern _S_RD_EE:near
extern _Opamp_Calibration:near
extern _Read_CO_Value:near
extern _MEAS_Send_Charge_Pulse:near

; 
; Generated by holtek-gcc v3.19, Thu Feb 10 14:53:43 2022
; Rebuild 718
; end of file
